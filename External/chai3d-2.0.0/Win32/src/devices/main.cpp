////===========================================================================
///*
//    This file is part of the Reach Laboratory of Kettering University.
//    Copyright (C) 2014. All rights reserved.
//
//    \author    Hadi Rahmat-Khah
//    \version   2.0.0 $Rev: 451 $
//*/
////===========================================================================
//
//#include "GRT/GRT.h"
//using namespace GRT;
//
//
//int main(int argc, const char * argv[]){
//
//	//Load the training data
//	LabelledTimeSeriesClassificationData trainingData;
//
//	if (!trainingData.loadDatasetFromFile("HMMTrainingData.txt")){
//		cout << "ERROR: Failed to load training data!\n";
//		return false;
//	}
//
//	//Remove 20% of the training data to use as test data
//	LabelledTimeSeriesClassificationData testData = trainingData.partition(80);
//
//	//The input to the HMM must be a quantized discrete value
//	//We therefore use a KMeansQuantizer to covert the N-dimensional continuous data into 1-dimensional discrete data
//	const UINT NUM_SYMBOLS = 10;
//	KMeansQuantizer quantizer(trainingData.getNumDimensions());//, NUM_SYMBOLS);
//
//	//Train the quantizer using the training data
//	if (!quantizer.train(trainingData)){
//		cout << "ERROR: Failed to train quantizer!\n";
//		return false;
//	}
//
//	//Quantize the training data
//	LabelledTimeSeriesClassificationData quantizedTrainingData(1);
//
//	for (UINT i = 0; i<trainingData.getNumSamples(); i++){
//
//		UINT classLabel = trainingData[i].getClassLabel();
//		MatrixDouble quantizedSample;
//
//		for (UINT j = 0; j<trainingData[i].getLength(); j++){
//			quantizer.quantize(trainingData[i].getData().getRowVector(j));
//
//			quantizedSample.push_back(quantizer.getFeatureVector());
//		}
//
//		if (!quantizedTrainingData.addSample(classLabel, quantizedSample)){
//			cout << "ERROR: Failed to quantize training data!\n";
//			return false;
//		}
//
//	}
//
//	//Create a new HMM instance
//	HMM hmm;
//
//	//Set the number of states in each model
//	hmm.setNumStates(4);
//
//	//Set the number of symbols in each model, this must match the number of symbols in the quantizer
//	hmm.setNumSymbols(NUM_SYMBOLS);
//
//	//Set the HMM model type to LEFTRIGHT with a delta of 1
//	hmm.setModelType(HiddenMarkovModel::LEFTRIGHT);
//	hmm.setDelta(1);
//
//	//Set the training parameters
//	hmm.setMinImprovement(1.0e-5);
//	hmm.setMaxNumIterations(100);
//	hmm.setNumRandomTrainingIterations(20);
//
//	////Train the HMM model
//	//if (!hmm.train(quantizedTrainingData)){
//	//	cout << "ERROR: Failed to train the HMM model!\n";
//	//	return false;
//	//}
//
//	////Save the HMM model to a file
//	//if (!hmm.saveModelToFile("HMMModel.txt")){
//	//	cout << "ERROR: Failed to save the model to a file!\n";
//	//	return false;
//	//}
//
//	//Load the HMM model from a file
//	if (!hmm.loadModelFromFile("HMMModel.txt")){
//		cout << "ERROR: Failed to load the model from a file!\n";
//		return false;
//	}
//
//	//Quantize the test data
//	LabelledTimeSeriesClassificationData quantizedTestData(1);
//
//	for (UINT i = 0; i<testData.getNumSamples(); i++){
//
//		UINT classLabel = testData[i].getClassLabel();
//		MatrixDouble quantizedSample;
//
//		for (UINT j = 0; j<testData[i].getLength(); j++){
//			quantizer.quantize(testData[i].getData().getRowVector(j));
//
//			quantizedSample.push_back(quantizer.getFeatureVector());
//		}
//
//		if (!quantizedTestData.addSample(classLabel, quantizedSample)){
//			cout << "ERROR: Failed to quantize training data!\n";
//			return false;
//		}
//	}
//
//	//Compute the accuracy of the HMM models using the test data
//	double numCorrect = 0;
//	double numTests = 0;
//	for (UINT i = 0; i<quantizedTestData.getNumSamples(); i++){
//
//		UINT classLabel = quantizedTestData[i].getClassLabel();
//		hmm.predict(quantizedTestData[i].getData());
//
//		if (classLabel == hmm.getPredictedClassLabel()) numCorrect++;
//		numTests++;
//
//		VectorDouble classLikelihoods = hmm.getClassLikelihoods();
//		VectorDouble classDistances = hmm.getClassDistances();
//
//		cout << "ClassLabel: " << classLabel;
//		cout << " PredictedClassLabel: " << hmm.getPredictedClassLabel();
//		cout << " MaxLikelihood: " << hmm.getMaximumLikelihood();
//
//		cout << "  ClassLikelihoods: ";
//		for (UINT k = 0; k<classLikelihoods.size(); k++){
//			cout << classLikelihoods[k] << "\t";
//		}
//
//		cout << "ClassDistances: ";
//		for (UINT k = 0; k<classDistances.size(); k++){
//			cout << classDistances[k] << "\t";
//		}
//		cout << endl;
//	}
//
//	cout << "Test Accuracy: " << numCorrect / numTests*100.0 << endl;
//
//	return true;
//}


#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include <stdexcept>
#include <deque>

using namespace std;

template <class T>
class SpatialInformation {
private:
	deque<T> timeDeque;
	deque<T> positionDeque;
	deque<T> velocityDeque;
	deque<T> accelerationDeque;

	int maxSize;
	int windowSize;

public:
	SpatialInformation(int maxSize, int windowSize);
	T operator [](int i);
	void push(T const&, T const&);  // push element 
	void pop();               // pop element 
	T top() const;            // return top element 
	bool empty() const{       // return true if empty.
		return positionDeque.empty();
	}
	T getPosition();
	T getVelocity();
	T getAcceleration();
};

template <class T>
SpatialInformation<T>::SpatialInformation(int initialMaxSize, int initialWindowSize)
{
	maxSize = initialMaxSize;
	windowSize = initialWindowSize;
}

template <class T>
T SpatialInformation<T>::operator [](int i)
{

	return positionDeque[i];
}

template <class T>
void SpatialInformation<T>::push(T const& pos, T const& recordedTime)
{
	// remove the last element
	if (positionDeque.size() >= maxSize)
	{
		positionDeque.pop_back();
		timeDeque.pop_back();
	}
	if (velocityDeque.size() >= maxSize)
	{
		velocityDeque.pop_back();
	}
	if (accelerationDeque.size() >= maxSize)
	{
		accelerationDeque.pop_back();
	}
	// append copy of passed element 
	positionDeque.push_front(pos);
	timeDeque.push_front(recordedTime);

	if (positionDeque.size() >= 2)
	{
		velocityDeque.push_front(positionDeque[0] - positionDeque[1]);
	}

	if (velocityDeque.size() >= 2)
	{
		accelerationDeque.push_front(velocityDeque[0] - velocityDeque[1]);
	}
}

template <class T>
void SpatialInformation<T>::pop()
{
	if (positionDeque.empty()) {
		throw out_of_range("Stack<>::pop(): empty stack");
	}
	// remove last element 
	positionDeque.pop_back();
}

template <class T>
T SpatialInformation<T>::top() const
{
	if (positionDeque.empty()) {
		throw out_of_range("Stack<>::top(): empty stack");
	}
	// return copy of last element 
	return positionDeque.back();
}

template <class T>
T SpatialInformation<T>::getPosition()
{
	if (positionDeque.empty()) {
		throw out_of_range("Stack<>::top(): empty stack");
	}
	// return copy of last element 
	return positionDeque[0];
}

template <class T>
T SpatialInformation<T>::getVelocity()
{
	T velocityAverage = 0.0;
	if (velocityDeque.size() > windowSize)
	{
		for (int i = 0; i < windowSize; i++)
			velocityAverage += velocityDeque[i];

		return velocityAverage / (timeDeque[0] - timeDeque[windowSize - 1]);
	}
	return 0.0;
}

template <class T>
T SpatialInformation<T>::getAcceleration()
{
	T accelerationAverage = 0.0;
	if (accelerationDeque.size() > windowSize)
	{
		for (int i = 0; i < windowSize; i++)
			accelerationAverage += accelerationDeque[i];

		return accelerationAverage / (timeDeque[0] - timeDeque[windowSize - 1]);
	}
	return 0.0;
}

#define SERIAL_INTERFACE
//#define MOTION_CAPTURE
#define HAPTIC_DEVICE
//#define TIXML_USE_STL
#define DATA_STORAGE
//#define SAVE_CONFIGURATION_CONFIRMATION
//#define VIDEO_CAPTURE

#include "../stdafx.h"
//---------------------------------------------------------------------------
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//---------------------------------------------------------------------------
#include "chai3d.h"
#include "CODE.h"
//---------------------------------------------------------------------------
using namespace std;


//---------------------------------------------------------------------------
// File Storage
//---------------------------------------------------------------------------
#ifdef DATA_STORAGE

#include "timer.h"
#include <time.h>

//---------------------------------------------------------------------------
// DECLARED CONSTANTS
//---------------------------------------------------------------------------

const int MAX_DATE = 12;

// Number of output file and an id for each oh them
const int NUMBER_OF_OUTFILE = 4;
enum OutputFileFormat
{
	OUTPUT_FILE_HAPTIC,
	OUTPUT_FILE_3D_VECTORS,
	OUTPUT_FILE_6D_VECTORS,
	OUTPUT_FILE_ANALOG_DATA
};

//---------------------------------------------------------------------------
// DECLARED VARIABLES
//---------------------------------------------------------------------------

// Create one output file for each
FILE *OutputFile[NUMBER_OF_OUTFILE] = { NULL, NULL, NULL, NULL };

// Sampling frequencies
CTimer timer;
float  motionCapture3DSamplingCurrentTimestamp, motionCapture3DSamplingPreviousTimestamp;
float  motionCapture6DSamplingCurrentTimestamp, motionCapture6DSamplingPreviousTimestamp;
float  motionCaptureAnalogSamplingCurrentTimestamp, motionCaptureAnalogSamplingPreviousTimestamp;
float  hapticSamplingCurrentTimestamp, hapticSamplingPreviousTimestamp;
float  hapticSamplingTime, motionCaptureSamplingTime;

//---------------------------------------------------------------------------
// DECLARED MACROS
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// DECLARED FUNCTIONS
//---------------------------------------------------------------------------


#else // DATA_STORAGE

#endif // DATA_STORAGE

//---------------------------------------------------------------------------
// TinyXML
//---------------------------------------------------------------------------
#ifdef TIXML_USE_STL

#include <iostream>
#include <sstream>
#if defined( WIN32 ) && defined( TUNE )
#include <crtdbg.h>
_CrtMemState startMemState;
_CrtMemState endMemState;
#endif

#include "tinyxml.h"

//---------------------------------------------------------------------------
// DECLARED CONSTANTS
//---------------------------------------------------------------------------

static int gPass = 0;
static int gFail = 0;

//---------------------------------------------------------------------------
// DECLARED VARIABLES
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// DECLARED MACROS
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// DECLARED FUNCTIONS
//---------------------------------------------------------------------------

#else // TIXML_USE_STL

#endif // TIXML_USE_STL

//---------------------------------------------------------------------------
// Seiral Interface
//---------------------------------------------------------------------------
#ifdef SERIAL_INTERFACE

#include "Serial.h"
#include "RTC3Dclient.h"

//---------------------------------------------------------------------------
// DECLARED CONSTANTS
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// DECLARED VARIABLES
//---------------------------------------------------------------------------

CSerial serial;

string serialPortName = "COM1";

char lastRead = NULL;
bool serialIsConnected = false;
bool isButtonPressed = false;

bool serialInterfaceThreadFinished = false;

//---------------------------------------------------------------------------
// DECLARED MACROS
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// DECLARED FUNCTIONS
//---------------------------------------------------------------------------

void SerialInterfaceUpdateFucntion(void);

#else // SERIAL_INTERFACE

#endif // SERIAL_INTERFACE

//---------------------------------------------------------------------------
// Motion Capture - NDI 3D Investigator
//---------------------------------------------------------------------------
#ifdef MOTION_CAPTURE

#include "Serial.h"
#include "RTC3Dclient.h"

//---------------------------------------------------------------------------
// DECLARED CONSTANTS
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// DECLARED VARIABLES
//---------------------------------------------------------------------------

int MotionCapture_None		= 0x00;
int MotionCapture_3D_Vector = 0x01;
int MotionCapture_6D_Vector = 0x02;
int MotionCapture_Analog	= 0x04;
int MotionCapture_All		= 0x07;

int MotionCapture_ActiveOutputs = MotionCapture_None;

int active3DMarkersCount;
int* active3DMarkerIDs;
string* active3DMarkerNames;

// initial size (width/height) in pixels of the display window
int motionCaptureWindowWidth;
int motionCaptureWindowHeight;

int motionCaptureWindowReference;

// has exited haptics simulation thread
bool motionCaptureSimulationFinished = false;

Client m_Client;
CString m_sHostname;
DWORD m_dwIP;
char szHost[100];
bool rtc3dClientIsConnected = false;
vector<QuatErrorTransformation> m_v6d;
vector<Position3d> m_v3d;
vector<float> m_vAnalog;
unsigned int m_uFrame3d;
unsigned int m_uFrame6d;
unsigned int m_uFrameAnalog;

int motionCaptureWindowPosX;
int motionCaptureWindowPosY;

//---------------------------------------------------------------------------
// DECLARED MACROS
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// DECLARED FUNCTIONS
//---------------------------------------------------------------------------

// callback when the window display is resized
void MotionCaptureWindowResizeFunc(int w, int h);

// main motion capture graphics callback
void MotionCaptureWindowUpdateGraphics(void);

// main motion capture loop
void MotionCaptureWindowUpdateFucntion(void);

// Updates marker items in the list control
void MotionCaptureUpdate3dList(vector<Position3d> & v3d);

// Updates analog values in list control
void MotionCaptureUpdateAnalogList(vector<float> & vAnalog);

// Updates tool items in the control list
void MotionCaptureUpdate6dList(vector<QuatErrorTransformation> & v6d);

// transform rads to deg
void ConvertQuaterionToEuler(float qw, float qx, float qy, float qz, float& ex, float& ey, float& ez);

#else // MOTION_CAPTURE

#endif // MOTION_CAPTURE

//---------------------------------------------------------------------------
// Video Capture based on OpenCV
//---------------------------------------------------------------------------
#ifdef VIDEO_CAPTURE

#ifndef GL_BGR
#define GL_BGR 0x80E0
#endif

#include "opencv2/highgui/highgui.hpp"
#include <iostream>

using namespace cv;
using namespace std;

//---------------------------------------------------------------------------
// DECLARED CONSTANTS
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// DECLARED VARIABLES
//---------------------------------------------------------------------------

string videoStreamAddress;

// initial size (width/height) in pixels of the display window
int videoCaptureWindowWidth;
int videoCaptureWindowHeight;

int videoCaptureWindowReference;

// has exited haptics simulation thread
bool videoCaptureSimulationFinished = false;

int videoCaptureWindowPosX;
int videoCaptureWindowPosY;

VideoWriter oVideoWriter;
VideoWriter oVirtualSceneWriter;

Size frameSize;

bool isVideoWriterOpen = false;

bool displayVideoOnScreen = false;

// Texturing.
GLuint tex1;
GLfloat tex1AR;

VideoCapture cap;
cv::Mat frame;

//---------------------------------------------------------------------------
// DECLARED MACROS
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// DECLARED FUNCTIONS
//---------------------------------------------------------------------------

// callback when the window display is resized
//void VideoCaptureWindowResizeFunc(int w, int h);

// main motion capture loop
void VideoCaptureWindowUpdateFucntion(void);

#else // VIDEO_CAPTURE

#endif // VIDEO_CAPTURE

//---------------------------------------------------------------------------
// Haptic Device - Phantom Omni
//---------------------------------------------------------------------------
#ifdef HAPTIC_DEVICE

//#define LEFTTOOL
//#include "chai3d_extention\tools\CLaparoscopicTool.h"

//---------------------------------------------------------------------------
// DECLARED CONSTANTS
//---------------------------------------------------------------------------

// maximum number of haptic devices supported in this demo
const int MAX_DEVICES = 2;
enum LaparoscopicTool { RightTool, LeftTool };

const int toolIsReversed[MAX_DEVICES] = { 0, 1 };

cVector3d toolOffset[MAX_DEVICES] = { cVector3d(0.2, 0.0, 0.0) , cVector3d(0.0, 0.0, 0.0) };

//---------------------------------------------------------------------------
// DECLARED VARIABLES
//---------------------------------------------------------------------------

// a table containing pointers to label which display the position of
// each haptic device
cLabel* labels[MAX_DEVICES];
cGenericObject* rootLabels;

// a haptic device handler
cHapticDeviceHandler* handler;

// a virtual tool representing the haptic device in the scene
cGeneric3dofPointer* tool[MAX_DEVICES];
//cLaparoscopicTool* tool[MAX_DEVICES];

// a pointer the ODE object grasped by the tool
cODEGenericBody* graspObject[MAX_DEVICES];

// the last grasp position in global coordinates
cVector3d lastGraspPosition[MAX_DEVICES];
cVector3d lastToolPosition[MAX_DEVICES];
cMatrix3d lastToolRotation[MAX_DEVICES];
cVector3d lastPegPosition[MAX_DEVICES];
cMatrix3d lastPegRotation[MAX_DEVICES];
cVector3d lastAppliedPosition[MAX_DEVICES];
cMatrix3d lastAppliedRotation[MAX_DEVICES];

// grasp position is respect to object
cVector3d graspPosition[MAX_DEVICES];

// is grasp currently active?
bool graspActive[MAX_DEVICES] = { false, false };

// a small line used to display a grasp
cShapeLine* graspLine[MAX_DEVICES];

bool userSwitch[MAX_DEVICES];

// number of haptic devices detected
int numHapticDevices = 1;

// a table containing pointers to all haptic devices detected on this computer
cGenericHapticDevice* hapticDevices[MAX_DEVICES];

// a vibrating spherical objects
cShapeSphere* vibratingObject;
cODEGenericBody* ODEVibratingObject;

double maximumForce[MAX_DEVICES];

//---------------------------------------------------------------------------
// DECLARED MACROS
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// DECLARED FUNCTIONS
//---------------------------------------------------------------------------

// main haptics loop
void HapticWindowUpdateFunction(void);

#else  // HAPTIC_DEVICE
#endif // HAPTIC_DEVICE

//---------------------------------------------------------------------------
// Common Part
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// DECLARED CONSTANTS
//---------------------------------------------------------------------------

const int WINDOW_SIZE_W = 600;
const int WINDOW_SIZE_H = 600;

// mouse menu options (right button)
const int OPTION_FULLSCREEN = 1;
const int OPTION_WINDOWDISPLAY = 2;

const int NUMBER_OF_GROUND_PLANE = 6;

//---------------------------------------------------------------------------
// DECLARED VARIABLES
//---------------------------------------------------------------------------

int mainWindowReference;

// initial size (width/height) in pixels of the display window
int mainWindowWidth;
int mainWindowHeight;

// a world that contains all objects of the virtual environment
cWorld* world;

// a camera that renders the world in a window display
cCamera* camera;

// camera position (eye)
cVector3d cameraPostion;

// lookat position (target)
cVector3d cameraLookAt;

// direction of the "up" vector
cVector3d cameraUpVector;

// a light source to illuminate the objects in the virtual scene
cLight *light;

// a little "chai3d" bitmap logo at the bottom of the screen
cBitmap* logo;

// width and height of the current window display
int displayW = 0;
int displayH = 0;

// retrieve the resolution of the computer display and estimate the position
// of the GLUT window so that it is located at the center of the screen
int screenW = glutGet(GLUT_SCREEN_WIDTH);
int screenH = glutGet(GLUT_SCREEN_HEIGHT);

int mainWindowPosX;
int mainWindowPosY;

// status of the main simulation haptics loop
bool simulationRunning = false;

// has exited haptics simulation thread
bool simulationFinished = false;

// ODE world
cODEWorld* ODEWorld;

bool isODEEnabled = true;

// number of pegs and axis
int numberOfPegs;
int numberOfAxises;

// define postion and roation variables for peg object
cVector3d* pegPosition;
cMatrix3d* pegRotation;

// define postion and roation variables for axis object
cVector3d* axisPosition;

// create a virtual mesh  that will be used for the geometry
// representation of the dynamic body
cMesh** peg;
cMesh** axis;

// ODE object
cODEGenericBody** ODEPeg;
cODEGenericBody** ODEAxis;

cODEGenericBody* ODEGPlane[NUMBER_OF_GROUND_PLANE];

// clock
cPrecisionClock startHapticsClock;

// radius of the tool proxy
double proxyRadius;

// maximum stiffness to be used with virtual objects in scene
double stiffnessMax;

// root resource path
string resourceRoot;

//// a virtual peg mesh
//cMesh* peg;

// transparency level
double transparencyLevel = 0.3;

// virtual drill mesh
cMesh* drill;

//---------------------------------------------------------------------------
// DECLARED MACROS
//---------------------------------------------------------------------------

// convert to resource path
#define RESOURCE_PATH(p)    (char*)((resourceRoot+string(p)).c_str())

//---------------------------------------------------------------------------
// DECLARED FUNCTIONS
//---------------------------------------------------------------------------

bool CtrlHandler(DWORD fdwCtrlType);

// callback when the window display is resized
void HapticWindowResizeFunc(int w, int h);

// callback when a keyboard key is pressed
void KeySelect(unsigned char key, int x, int y);

// callback when the right mouse button is pressed to select a menu item
void MenuSelect(int value);

// function called before exiting the application
void Close(void);

// main graphics callback
void HapticWindowUpdateGraphics(void);

// GLUT idle function
void IdleFunction(void);

void ConfigurationLoad(const char *configFilename);

void ConfigurationSave(const char *configFilename);

void ConfigurationCreateTemplateXML();

string GetDateTime(void);

//===========================================================================
/*
    APP:    main.cpp

    This demonstration shows how to attach a 3D mesh file to a virtual
    tool. A second mesh (peg) is loaded in the scene. By pressing the user
    switch of the haptic device it is possible to translate or oriente
    the peg object accordingly.

    In the main haptics loop function  "updateHaptics()" , the position
    of the haptic device is retrieved at each simulation iteration.
    The interaction forces are then computed and sent to the device.
*/
//===========================================================================

#include <windows.h>
#include <GL/glut.h>
#include <stdio.h> 
int w, h;
const int font = (int)GLUT_BITMAP_9_BY_15;
char s[30];
double t;
static void resize(int width, int height)
{
	const float ar = (float)width / (float)height;
	w = width;
	h = height;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-ar, ar, -1.0, 1.0, 2.0, 100.0);     glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
void setOrthographicProjection() {
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, displayW, 0, displayH);
	glScalef(1, -1, 1);
	glTranslatef(0, -displayH, 0);
	glMatrixMode(GL_MODELVIEW);
}
void resetPerspectiveProjection() {
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}
void renderBitmapString(float x, float y, void *font, const char *string){
	const char *c;
	glRasterPos2f(x, y);
	for (c = string; *c != '\0'; c++) {
		glutBitmapCharacter(font, *c);
	}
}
static void display(void){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3d(1.0, 0.0, 0.0);
	setOrthographicProjection();
	glPushMatrix();
	glLoadIdentity();
	renderBitmapString(200, 200, (void *)font, "Font Rendering - Programming Techniques");
	renderBitmapString(300, 220, (void*)font, s);
	renderBitmapString(300, 240, (void *)font, "Esc - Quit");
	glPopMatrix();
	resetPerspectiveProjection();
	glutSwapBuffers();
}
void update(int value){
	t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
	int time = (int)t;
	sprintf(s, "TIME : %2d Sec", time);
	glutTimerFunc(1000, update, 0);
	glutPostRedisplay();
}
int main1(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(10, 10);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);     glutCreateWindow("Font Rendering Using Bitmap Font - Programming Techniques0");     glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutTimerFunc(25, update, 0);     glutMainLoop();
	return EXIT_SUCCESS;
}

int main(int argc, char* argv[])
{
	if (SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, true))
	{
		// Program can successfully manage the ctrl key.
	}
	else
	{
		exit(1);
	}
    //-----------------------------------------------------------------------
    // INITIALIZATION
    //-----------------------------------------------------------------------

	cout << endl;
	cout << "Surgery Simulator - Peg Transfer" << endl;
	cout << "Reach Lab" << endl;
	cout << "Kettering University" << endl;
	cout << "Electrical and Computer Engineering" << endl;
	cout << "Copyright 2014" << endl;
	cout << endl;
	cout << endl;
	cout << "Instruction:" << endl;
	cout << "Use laparoscopic device connected to a haptic device" << endl;
	cout << "to move a peg in the virtual environment." << endl;
	cout << endl;
	cout << endl;
	cout << "Keyboard Options:" << endl;
	cout << "[x] - [Esc] - Exit application" << endl;
	cout << endl;
	cout << endl;

    // parse first arg to try and locate resources
    resourceRoot = string(argv[0]).substr(0,string(argv[0]).find_last_of("/\\")+1);

	bool fileload;

	ConfigurationLoad(RESOURCE_PATH("resources/configuration.xml"));

	//ConfigurationCreateTemplateXML();
	//return 0;

#ifdef MOTION_CAPTURE
	//-----------------------------------------------------------------------
	// Connect to NDI First Principles using network socket 
	//-----------------------------------------------------------------------

	gethostname(szHost, 100);
	m_sHostname = szHost;
	m_dwIP = ntohl(m_Client.ulGetIPAddress(szHost));

	m_Client.bConnect(m_dwIP);
	if (m_Client.bIsConnected())
	{
		rtc3dClientIsConnected = true;
		m_Client.nSendCommand("StreamFrames AllFrames");
	}
#endif // MOTION_CAPTURE

	//-----------------------------------------------------------------------
	// 3D - SCENEGRAPH
	//-----------------------------------------------------------------------

	// create a new world.
	world = new cWorld();

	// set the background color of the environment
	// the color is defined by its (R,G,B) components.
	world->setBackgroundColor(0.0, 0.0, 0.0);

	// create a camera and insert it into the virtual world
	camera = new cCamera(world);
	world->addChild(camera);

	// position and oriente the camera
	camera->set(
		cameraLookAt,	// camera position (eye)
		cameraPostion,	// lookat position (target)
		cameraUpVector);	// direction of the "up" vector

	// set the near and far clipping planes of the camera
	// anything in front/behind these clipping planes will not be rendered
	camera->setClippingPlanes(0.01, 10.0);

	// create a light source and attach it to the camera
	light = new cLight(world);
	camera->addChild(light);                   // attach light to camera
	light->setEnabled(true);                   // enable light source
	light->setPos(cVector3d(2.0, 0.5, 1.0));  // position the light source
	light->setDir(cVector3d(-2.0, 0.5, 1.0));  // define the direction of the light beam
	light->m_ambient.set(0.6, 0.6, 0.6);
	light->m_diffuse.set(0.8, 0.8, 0.8);
	light->m_specular.set(0.8, 0.8, 0.8);


	// create a node on which we will attach small labels that display the
	// position of each haptic device
	rootLabels = new cGenericObject();
	camera->m_front_2Dscene.addChild(rootLabels);

	// create a small label as title
	cLabel* titleLabel = new cLabel();
	rootLabels->addChild(titleLabel);


	//-----------------------------------------------------------------------
	// 2D - WIDGETS
	//-----------------------------------------------------------------------

//	// create a 2D bitmap logo
//	logo = new cBitmap();
//
//	// add logo to the front plane
//	camera->m_front_2Dscene.addChild(logo);
//
//	// load a "chai3d" bitmap image file
//	fileload = logo->m_image.loadFromFile(RESOURCE_PATH("resources/images/chai3d.bmp"));
//	if (!fileload)
//	{
//#if defined(_MSVC)
//		fileload = logo->m_image.loadFromFile("../../../bin/resources/images/chai3d.bmp");
//#endif
//	}
//
//	// position the logo at the bottom left of the screen (pixel coordinates)
//	logo->setPos(10, 10, 0);
//
//	// scale the logo along its horizontal and vertical axis
//	logo->setZoomHV(0.25, 0.25);
//
//	// here we replace all black pixels (0,0,0) of the logo bitmap
//	// with transparent black pixels (0, 0, 0, 0). This allows us to make
//	// the background of the logo look transparent.
//	logo->m_image.replace(
//		cColorb(0, 0, 0),      // original RGB color
//		cColorb(0, 0, 0, 0)    // new RGBA color
//		);
//
//	// enable transparency
//	logo->enableTransparency(true);

#ifdef HAPTIC_DEVICE
	//-----------------------------------------------------------------------
	// HAPTIC DEVICES / TOOLS
	//-----------------------------------------------------------------------

	// create a haptic device handler
	handler = new cHapticDeviceHandler();

	// read the number of haptic devices currently connected to the computer
	numHapticDevices = handler->getNumDevices();

	// limit the number of devices to MAX_DEVICES
	numHapticDevices = cMin(numHapticDevices, MAX_DEVICES);

	// for each available haptic device, create a 3D cursor
	// and a small line to show velocity
	int i = 0;
	for (i = 0; i < numHapticDevices; i++)
	{
		// get a handle to the next haptic device
		cGenericHapticDevice* newHapticDevice;
		handler->getDevice(newHapticDevice, i);

		// open connection to haptic device
		newHapticDevice->open();

		// initialize haptic device
		newHapticDevice->initialize();

		// store the handle in the haptic device table
		hapticDevices[i] = newHapticDevice;

		// retrieve information about the current haptic device
		cHapticDeviceInfo info = newHapticDevice->getSpecifications();

		// create a 3D tool and add it to the world
		tool[i] = new cGeneric3dofPointer(world);
		//tool[i] = new cLaparoscopicTool(world, toolIsReversed[i], toolOffset[i]);
		world->addChild(tool[i]);

		// connect the haptic device to the tool
		tool[i]->setHapticDevice(hapticDevices[i]);

		// initialize tool by connecting to haptic device
		tool[i]->start();

		// map the physical workspace of the haptic device to a larger virtual workspace.
		tool[i]->setWorkspaceRadius(1.4);

		// define a radius for the tool (graphical display)
		tool[i]->setRadius(0.03);

		// hide the device sphere. only show proxy.
		tool[i]->m_deviceSphere->setShowEnabled(false);

		// set the physical readius of the proxy.
		proxyRadius = 0.0;
		tool[i]->m_proxyPointForceModel->setProxyRadius(proxyRadius);
		tool[i]->m_proxyPointForceModel->m_collisionSettings.m_checkBothSidesOfTriangles = false;

		// enable if objects in the scene are going to rotate of translate
		// or possibly collide against the tool. If the environment
		// is entirely static, you can set this parameter to "false"
		tool[i]->m_proxyPointForceModel->m_useDynamicProxy = true;

		// adjust the color of the tool
		tool[i]->m_materialProxy = tool[i]->m_materialProxyButtonPressed;

		// read the scale factor between the physical workspace of the haptic
		// device and the virtual workspace defined for the tool
		double workspaceScaleFactor = tool[i]->getWorkspaceScaleFactor();

		// define a maximum stiffness that can be handled by the current
		// haptic device. The value is scaled to take into account the
		// workspace scale factor
		stiffnessMax = info.m_maxForceStiffness / workspaceScaleFactor;

		maximumForce[i] = info.m_maxForce;

		// crate a small label to indicate the position of the device
		cLabel* newPosLabel = new cLabel();
		rootLabels->addChild(newPosLabel);
		newPosLabel->setPos(0, -20 * i, 0);
		newPosLabel->m_fontColor.set(0.6, 0.6, 0.6);
		labels[i] = newPosLabel;
	}
	// create a small white line that will be enabled every time the operator
	// grasps an object. The line indicated the connection between the
	// position of the tool and the grasp position on the object
	for (i = 0; i < MAX_DEVICES; i++)
	{
		graspLine[i] = new cShapeLine(cVector3d(0, 0, 0), cVector3d(0, 0, 0));
		world->addChild(graspLine[i]);
		graspLine[i]->m_ColorPointA.set(1.0, 1.0, 1.0);
		graspLine[i]->m_ColorPointB.set(1.0, 1.0, 1.0);
		graspLine[i]->setShowEnabled(false);
	}

#endif // HAPTIC_DEVICE

	//-----------------------------------------------------------------------
	// COMPOSE THE VIRTUAL SCENE
	//-----------------------------------------------------------------------

	// create an ODE world to simulate dynamic bodies
	ODEWorld = new cODEWorld(world);

	// add ODE world as a node inside world
	world->addChild(ODEWorld);

	// set some gravity
	ODEWorld->setGravity(cVector3d(0.0, 0.0, -0.5));

	// define some material properties for peg objects
	cMaterial pegMaterial;
	pegMaterial.m_ambient.set(0.7, 0.1, 0.2);
	pegMaterial.m_diffuse.set(0.8, 0.15, 0.2);
	pegMaterial.m_specular.set(1.0, 0.2, 0.2);
	pegMaterial.setStiffness(0.1 * stiffnessMax);
	pegMaterial.setDynamicFriction(0.4);
	pegMaterial.setStaticFriction(0.9);

	// initialize and load model into the peg variable
	for (int i = 0; i < numberOfPegs; i++)
	{
		peg[i] = new cMesh(world);
		fileload = peg[i]->loadFromFile(RESOURCE_PATH("resources/models/peg_transfer/peg.3ds"));
		if (!fileload)
		{
#if defined(_MSVC)
			fileload = peg[i]->loadFromFile("../../../bin/resources/models/peg_transfer/peg.3ds");
#endif
		}
		peg[i]->scale(0.1);
		peg[i]->createAABBCollisionDetector(proxyRadius, true, false);

		// set material properties
		peg[i]->setMaterial(pegMaterial, true);

		// create a new ODE object that is automatically added to the ODE world
		ODEPeg[i] = new cODEGenericBody(ODEWorld);

		// define image models
		ODEPeg[i]->setImageModel(peg[i]);

		// create a dynamic model of the ODE object. Here we decide to use a box just like
		// the object mesh we just defined
		ODEPeg[i]->createDynamicMesh();

		// define some mass properties for each gear
		ODEPeg[i]->setMass(0.6);

		// set postion and orientation
		ODEPeg[i]->setPosition(pegPosition[i]);
		ODEPeg[i]->setRotation(pegRotation[i]);

		if (!isODEEnabled)
		{
			ODEPeg[i]->disableDynamics();
		}
	}

	// define some material properties for axix objects
	cMaterial axisMaterial;
	axisMaterial.m_ambient.set(0.2, 0.2, 0.2);
	axisMaterial.m_diffuse.set(0.8, 0.8, 0.8);
	axisMaterial.m_specular.set(1.0, 1.0, 1.0);
	axisMaterial.setStiffness(0.5 * stiffnessMax);
	axisMaterial.setDynamicFriction(0.4);
	axisMaterial.setStaticFriction(0.9);

	// initialize and load model into the axis variable
	for (int i = 0; i < numberOfAxises; i++)
	{
		axis[i] = new cMesh(world);
		// load static object 1 - a shaft
		fileload = axis[i]->loadFromFile(RESOURCE_PATH("resources/models/peg_transfer/axis.3ds"));
		if (!fileload)
		{
#if defined(_MSVC)
			fileload = axis[i]->loadFromFile("../../../bin/resources/models/peg_transfer/axis.3ds");
#endif
		}
		axis[i]->scale(0.0015);
		axis[i]->createAABBCollisionDetector(proxyRadius, true, false);

		// set material properties
		axis[i]->setMaterial(axisMaterial, true);

		// create a new ODE object that is automatically added to the ODE world
		ODEAxis[i] = new cODEGenericBody(ODEWorld);

		// define image models
		ODEAxis[i]->setImageModel(axis[i]);

		// create a dynamic model of the ODE object. Here we decide to use a box just like
		// the object mesh we just defined
		ODEAxis[i]->createDynamicCapsule(0.050, 0.47, true);  // true = make object static

		// set position
		ODEAxis[i]->setPosition(axisPosition[i]);
	}
	
	// we now create 6 static walls to bound the workspace of our simulation
	for (int i = 0; i < NUMBER_OF_GROUND_PLANE; i++)
	{
		ODEGPlane[i] = new cODEGenericBody(ODEWorld);
	}

	ODEGPlane[0]->createStaticPlane(cVector3d(0.0, 0.0, 2.0),	cVector3d(0.0, 0.0, -1.0));	// Upper plane
	ODEGPlane[1]->createStaticPlane(cVector3d(0.0, 0.0, -0.4),	cVector3d(0.0, 0.0, 1.0));	// Lower plane
	ODEGPlane[2]->createStaticPlane(cVector3d(0.0, 1.5, 0.0),	cVector3d(0.0, -1.0, 0.0));	// Right plane
	ODEGPlane[3]->createStaticPlane(cVector3d(0.0, -1.5, 0.0),	cVector3d(0.0, 1.0, 0.0));	// Left plane
	ODEGPlane[4]->createStaticPlane(cVector3d(1.1, 0.0, 0.0),	cVector3d(-1.0, 0.0, 0.0));	// Front plane
	ODEGPlane[5]->createStaticPlane(cVector3d(-1.1, 0.0, 0.0),	cVector3d(1.0, 0.0, 0.0));	// Back plane

	/////////////////////////////////////////////////////////////////////////
	// "VIBRATIONS"
	////////////////////////////////////////////////////////////////////////

	// create a sphere and define its radius
	vibratingObject = new cShapeSphere(0.015);

	// add object to world
	//world->addChild(vibratingObject);

	// set the position of the object at the center of the world
	vibratingObject->setPos(-0.5, -0.9, -0.1);
	
	// set haptic properties
	vibratingObject->m_material.setVibrationFrequency(40);
	vibratingObject->m_material.setVibrationAmplitude(0.3 * maximumForce[LeftTool]);
	vibratingObject->m_material.setStiffness(0.1 * stiffnessMax);

	// temp variable
	cGenericEffect* newEffect;

	// create a haptic viscous effect
	newEffect = new cEffectVibration(vibratingObject);
	vibratingObject->addEffect(newEffect);

	newEffect = new cEffectSurface(vibratingObject);
	vibratingObject->addEffect(newEffect);
	
	//////////////////////////////////////////////////////////////////////////
	// Create some reflexion
	//////////////////////////////////////////////////////////////////////////

//	// we create an intermediate node to which we will attach
//	// a copy of the object located inside the world
//	cGenericObject* reflexion = new cGenericObject();
//
//	// set this object as a ghost node so that no haptic interactions or
//	// collision detecting take place within the child nodes added to the
//	// reflexion node.
//	reflexion->setAsGhost(true);
//
//	// add reflexion node to world
//	world->addChild(reflexion);
//
//	// turn off culling on each object (objects now get rendered on both sides)
//	for (int i = 0; i < NUMBER_OF_PEGS; i++)
//	{
//		peg[i]->setUseCulling(false, true);
//	}
//	for (int i = 0; i < NUMBER_OF_AXISES; i++)
//	{
//		axis[i]->setUseCulling(false, true);
//	}
//
//	// create a symmetry rotation matrix (z-plane)
//	cMatrix3d rotRefexion;
//	rotRefexion.set(1.0, 0.0, 0.0,
//		0.0, 1.0, 0.0,
//		0.0, 0.0, -1.0);
//	reflexion->setRot(rotRefexion);
//	reflexion->setPos(0.0, 0.0, -2.005);
//
//	// add objects to the world
//	reflexion->addChild(ODEWorld);
//#ifdef HAPTIC_DEVICE
//	reflexion->addChild(tool);
//#endif // HAPTIC_DEVICE

	//////////////////////////////////////////////////////////////////////////
	// Create a Ground
	//////////////////////////////////////////////////////////////////////////

	// create mesh to model ground surface
	cMesh* ground = new cMesh(world);
	world->addChild(ground);

	// create 4 vertices (one at each corner)
	double groundSize = 2.0;
	int vertices0 = ground->newVertex(-groundSize, -groundSize, 0.0);
	int vertices1 = ground->newVertex(groundSize, -groundSize, 0.0);
	int vertices2 = ground->newVertex(groundSize, groundSize, 0.0);
	int vertices3 = ground->newVertex(-groundSize, groundSize, 0.0);

	// compose surface with 2 triangles
	ground->newTriangle(vertices0, vertices1, vertices2);
	ground->newTriangle(vertices0, vertices2, vertices3);

	// compute surface normals
	ground->computeAllNormals();

	// position ground at the right level
	ground->setPos(0.0, 0.0, -1.0);

	// define some material properties and apply to mesh
	cMaterial matGround;
	matGround.setStiffness(stiffnessMax);
	matGround.setDynamicFriction(0.7);
	matGround.setStaticFriction(1.0);
	matGround.m_ambient.set(0.0, 0.0, 0.0);
	matGround.m_diffuse.set(0.0, 0.0, 0.0);
	matGround.m_specular.set(0.0, 0.0, 0.0);
	ground->setMaterial(matGround);

	// enable and set transparency level of ground
	ground->setTransparencyLevel(0.75);
	ground->setUseTransparency(true);

	//-----------------------------------------------------------------------
    // OPEN GL - WINDOW DISPLAY
    //-----------------------------------------------------------------------

    // initialize GLUT
    glutInit(&argc, argv);

    // initialize the OpenGL GLUT window
    glutInitWindowPosition(mainWindowPosX, mainWindowPosY);
    glutInitWindowSize(mainWindowWidth, mainWindowHeight);
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	mainWindowReference = glutCreateWindow(argv[0]);
	glutDisplayFunc(HapticWindowUpdateGraphics);
    glutKeyboardFunc(KeySelect);
	glutReshapeFunc(HapticWindowResizeFunc);
    glutSetWindowTitle("Surgery Simulator - Peg transfer");

    //// create a mouse menu (right button)
    //glutCreateMenu(menuSelect);
    //glutAddMenuEntry("full screen", OPTION_FULLSCREEN);
    //glutAddMenuEntry("window display", OPTION_WINDOWDISPLAY);
    //glutAttachMenu(GLUT_RIGHT_BUTTON);

#ifdef MOTION_CAPTURE
	// initialize the OpenGL GLUT window
	glutInitWindowPosition(motionCaptureWindowPosX, motionCaptureWindowPosY);
	glutInitWindowSize(motionCaptureWindowWidth, motionCaptureWindowHeight);
	motionCaptureWindowReference = glutCreateWindow("MotionCapture");
	glutDisplayFunc(MotionCaptureWindowUpdateGraphics);
	glutKeyboardFunc(KeySelect);
	glutReshapeFunc(MotionCaptureWindowResizeFunc);
#endif // MOTION_CAPTURE

	glutIdleFunc(IdleFunction);

	glutSetWindow(mainWindowReference);
    //-----------------------------------------------------------------------
    // START SIMULATION
    //-----------------------------------------------------------------------

    // simulation in now running
    simulationRunning = true;

#ifdef HAPTIC_DEVICE
    // create a thread which starts the main haptics rendering loop
    cThread* mainWindowHapticsThread = new cThread();
	mainWindowHapticsThread->set(HapticWindowUpdateFunction, CHAI_THREAD_PRIORITY_HAPTICS);
#endif // HAPTIC_DEVICE

#ifdef MOTION_CAPTURE
	// create a thread which starts the main haptics rendering loop
	cThread* motionCaptureThread = new cThread();
	motionCaptureThread->set(MotionCaptureWindowUpdateFucntion, CHAI_THREAD_PRIORITY_DATA_CAPTURE);
#endif // MOTION_CAPTURE

#ifdef VIDEO_CAPTURE
	// create a thread which starts the main haptics rendering loop
	cThread* videoCaptureThread = new cThread();
	videoCaptureThread->set(VideoCaptureWindowUpdateFucntion, CHAI_THREAD_PRIORITY_DATA_CAPTURE);
#endif // VIDEO_CAPTURE

#ifdef SERIAL_INTERFACE
	// create a thread which starts the main haptics rendering loop
	cThread* serialInterfaceThread = new cThread();
	serialInterfaceThread->set(SerialInterfaceUpdateFucntion, CHAI_THREAD_PRIORITY_DATA_CAPTURE);
#endif // SERIAL_INTERFACE

	atexit(Close);

    // start the main graphics rendering loop
    glutMainLoop();

	// close everything
    Close();

	// exit
    return (0);
}

//---------------------------------------------------------------------------

void HapticWindowResizeFunc(int w, int h)
{
    // update the size of the viewport
    displayW = w;
    displayH = h;
    glViewport(0, 0, displayW, displayH);

	// update position of labels
	rootLabels->setPos(10, displayH - 70, 0);
}

//---------------------------------------------------------------------------

void KeySelect(unsigned char key, int x, int y)
{
    // escape key
    if ((key == 27) || (key == 'x'))
    {
        // close everything
        Close();
    }

    //// option 1:
    //if (key == '1')
    //{
    //    // only enable/disable wireframe of one part of the peg model
    //    bool useWireMode = ((cMesh*)(peg->getChild(1)))->getWireMode();
    //    ((cMesh*)(peg->getChild(1)))->setWireMode(!useWireMode);
    //}

    //// option 2:
    //if (key == '2')
    //{
    //    bool showNormals = peg->getShowNormals();
    //    peg->setShowNormals(!showNormals, true);
    //    peg->setNormalsProperties(0.05, cColorf(1.0, 0.0, 0.0), true);
    //}
    //
    //// option -:
    //if (key == '-')
    //{
    //    // decrease transparency level
    //    transparencyLevel = transparencyLevel - 0.1;
    //    if (transparencyLevel < 0.0) { transparencyLevel = 0.0; }

    //    // apply changes to peg
    //    ((cMesh*)(peg->getChild(1)))->setTransparencyLevel(transparencyLevel);
    //    ((cMesh*)(peg->getChild(1)))->setUseTransparency(true);

    //    // if object is almost transparent, make it invisible
    //    if (transparencyLevel < 0.1)
    //    {
    //        ((cMesh*)(peg->getChild(1)))->setShowEnabled(false, true);
    //    }
    //}

    //// option +:
    //if (key == '+')
    //{
    //    // increase transparency level
    //    transparencyLevel = transparencyLevel + 0.1;
    //    if (transparencyLevel > 1.0) { transparencyLevel = 1.0; }

    //    // apply changes to peg
    //    ((cMesh*)(peg->getChild(1)))->setTransparencyLevel(transparencyLevel);

    //    // make object visible
    //    if (transparencyLevel >= 0.1)
    //    {
    //        ((cMesh*)(peg->getChild(1)))->setShowEnabled(true, true);
    //    }

    //    // disable transparency is transparency level is set to 1.0
    //    if (transparencyLevel == 1.0)
    //    {
    //        ((cMesh*)(peg->getChild(1)))->setUseTransparency(false);
    //    }
    //}
}

//---------------------------------------------------------------------------

void MenuSelect(int value)
{
    switch (value)
    {
        // enable full screen display
        case OPTION_FULLSCREEN:
            glutFullScreen();
            break;

        // reshape window to original size
        case OPTION_WINDOWDISPLAY:
            glutReshapeWindow(WINDOW_SIZE_W, WINDOW_SIZE_H);
            break;
    }
}

//---------------------------------------------------------------------------

bool PromptForChar(const char* prompt, char& readch)
{
	std::string tmp;
	std::cout << prompt << std::endl;
	if (std::getline(std::cin, tmp))
	{
		// Only accept single character input
		if (tmp.length() == 1)
		{
			readch = tmp[0];
		}
		else
		{
			// For most input, char zero is an appropriate sentinel 
			readch = '\0';
		}
		return true;
	}
	return false;
}

//---------------------------------------------------------------------------

bool CtrlHandler(DWORD fdwCtrlType)
{
	switch (fdwCtrlType)
	{
		// Handle the CTRL-C signal.
	case CTRL_C_EVENT:
		// and the close button
	case CTRL_CLOSE_EVENT:
		Close();
		return true;

		// Pass other signals to the next handler. 
	case CTRL_BREAK_EVENT:
		return false;

		// delete the pointer anyway
	case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT:
	default:
		Close();
		return false;
	}
}

//---------------------------------------------------------------------------

void Close(void)
{
	if (!simulationRunning)
		return;

	// stop the simulation
    simulationRunning = false;

#ifdef SERIAL_INTERFACE
	// wait for graphics and haptics loops to terminate
	while (!serialInterfaceThreadFinished) { cSleepMs(100); }
#endif // SERIAL_INTERFACE

#ifdef HAPTIC_DEVICE
    // wait for graphics and haptics loops to terminate
    while (!simulationFinished) { cSleepMs(100); }
	
	for (int i = 0; i < numHapticDevices; i++)
	{
		// close haptic device
		tool[i]->stop();
	}
#endif // HAPTIC_DEVICE

#ifdef MOTION_CAPTURE
	// wait for graphics and haptics loops to terminate
	while (!motionCaptureSimulationFinished) { cSleepMs(100); }
#endif // MOTION_CAPTURE

#ifdef SAVE_CONFIGURATION_CONFIRMATION
	char type = '\0';
	while (PromptForChar("Do you want to save the configuration? [y/n]", type))
	{
		if (type == 'y' || type == 'Y')
		{
			// Process response
			saveConfiguration();
			break;
		}
		else if (type == 'n' || type == 'N')
		{
			break;
		}
	}
#else // SAVE_CONFIGURATION_CONFIRMATION
	ConfigurationSave(RESOURCE_PATH("resources/configuration.xml"));
#endif // SAVE_CONFIGURATION_CONFIRMATION
	// exit application
	exit(0);
}

//---------------------------------------------------------------------------

void IdleFunction(void)
{

}

//---------------------------------------------------------------------------

SpatialInformation<float> theta4Movements(20, 15);
SpatialInformation<float> theta5Movements(20, 15);
SpatialInformation<float> theta6Movements(20, 15);

void HapticWindowUpdateGraphics(void)
{
	// TODO: Add a feature to store opengl window as a movie beside the actual webcam input for later review.
	glutSetWindow(mainWindowReference);

	// update content of position label
	for (int i = 0; i<numHapticDevices; i++)
	{
		// read position of device an convert into millimeters
		cVector3d pos;
		cVector3d jointAngles;
		cVector3d gimbalAngles;
		hapticDevices[i]->getPosition(pos);
		hapticDevices[i]->getJointAngles(jointAngles, gimbalAngles);
		pos.mul(1000);
		
		float timeNow = timer.Now();
		theta4Movements.push(gimbalAngles[0], timeNow);
		theta5Movements.push(gimbalAngles[1], timeNow);
		theta6Movements.push(gimbalAngles[2], timer.Now());

		// create a string that concatenates the device number and its position.
		string strID;
		cStr(strID, i);
		string strLabel;
		
		//strLabel = "#" + strID + "  x: ";
		//cStr(strLabel, pos.x, 2);
		//strLabel = strLabel + "   y: ";
		//cStr(strLabel, pos.y, 2);
		//strLabel = strLabel + "  z: ";
		//cStr(strLabel, pos.z, 2);

		//strLabel = strLabel + "  1: ";
		//cStr(strLabel, jointAngles[0], 2);
		//strLabel = strLabel + "  2: ";
		//cStr(strLabel, jointAngles[1], 2);
		//strLabel = strLabel + "  3: ";
		//cStr(strLabel, jointAngles[2], 2);
		//strLabel = strLabel + "  4: ";
		//cStr(strLabel, gimbalAngles[0], 2);
		//strLabel = strLabel + "  5: ";
		//cStr(strLabel, gimbalAngles[1], 2);
		strLabel = strLabel + "  Theta4: ";
		cStr(strLabel, theta4Movements.getPosition(), 2);
		strLabel = strLabel + ", ";
		cStr(strLabel, theta4Movements.getVelocity(), 2);
		strLabel = strLabel + ", ";
		cStr(strLabel, theta4Movements.getAcceleration(), 2);
		strLabel = strLabel + "  Theta5: ";
		cStr(strLabel, theta5Movements.getPosition(), 2);
		strLabel = strLabel + ", ";
		cStr(strLabel, theta5Movements.getVelocity(), 2);
		strLabel = strLabel + ", ";
		cStr(strLabel, theta5Movements.getAcceleration(), 2);
		strLabel = strLabel + "  Theta6: ";
		cStr(strLabel, theta6Movements.getPosition(), 2);
		strLabel = strLabel + ", ";
		cStr(strLabel, theta6Movements.getVelocity(), 2);
		strLabel = strLabel + ", ";
		cStr(strLabel, theta6Movements.getAcceleration(), 2);

		//int th1 = jointAngles.x, th2 = jointAngles.y, th3 = jointAngles.z,
		//	th4 = gimbalAngles.x, th5 = gimbalAngles.y, th6 = gimbalAngles.z;
		//double L1 = 133.35, L2 = 133.35;
		//strLabel = strLabel + "  X: ";
		//cStr(strLabel, 0.027412*L2*sin(0.017453*th1 + 0.017453*th2) + L1*cos(0.017453*th3)*cos(0.017453*th1 + 0.017453*th2) - 0.99962*L1*sin(0.017453*th1 + 0.017453*th2)*sin(0.017453*th3), 2);
		//strLabel = strLabel + "  Y: ";
		//cStr(strLabel, L1*sin(0.017453*th1 + 0.017453*th2)*cos(0.017453*th3) - 0.027412*L2*cos(0.017453*th1 + 0.017453*th2) + 0.99962*L1*sin(0.017453*th3)*cos(0.017453*th1 + 0.017453*th2), 2);
		//strLabel = strLabel + "  Z: ";
		//cStr(strLabel, 0.99962*L2 + 0.027412*L1*sin(0.017453*th3), 2);
		
			
			


		labels[i]->m_string = strLabel;
	}

	// render world
	camera->renderView(displayW, displayH);

	//if (isVideoWriterOpen)
	//{
	//	int height = 240, width = 320;
	//	cv::Mat frame(height, width, CV_8UC3), fframe(height, width, CV_8UC3);
	//	//use fast 4-byte alignment (default anyway) if possible
	//	//glPixelStorei(GL_PACK_ALIGNMENT, (frame.step & 3) ? 1 : 4);

	//	//set length of one complete row in destination data (doesn't need to equal img.cols)
	//	glPixelStorei(GL_PACK_ROW_LENGTH, frame.step / frame.elemSize());
	//	glReadPixels(0, 0, frame.cols, frame.rows, GL_BGR, GL_UNSIGNED_BYTE, frame.data);
	//	cv::flip(frame, fframe, 0);
	//	oVirtualSceneWriter.write(fframe); // write the frame into the file
	//}

	// Swap buffers
	glutSwapBuffers();

	// TODO: glut returns "out of memory" after few minutes!
	// check for any OpenGL errors
	GLenum err;
	err = glGetError();
	if (err != GL_NO_ERROR) printf("Error: HapticWindowUpdateGraphics():: %s\n", gluErrorString(err));

	// inform the GLUT window to call updateGraphics again (next frame)
	if (simulationRunning)
	{
		glutPostRedisplay();
	}
}

//---------------------------------------------------------------------------

#ifdef HAPTIC_DEVICE
void HapticWindowUpdateFunction(void)
{
	// toggle and positions are used to calculate the distance between two points.
	int toggle = 0, runItOnce = 1;
	cVector3d positions[2];

	// simulation clock
	cPrecisionClock simClock;
	simClock.start(true);

	// reset haptics activation clock
	startHapticsClock.reset();
	startHapticsClock.start();
	bool hapticsReady = false;

	cVector3d toolPosition[MAX_DEVICES];
	cMatrix3d toolRotation[MAX_DEVICES];

	// main haptic simulation loop
	while (simulationRunning)
	{
		// wait for some time before enabling haptics
		if (!hapticsReady)
		{
			if (startHapticsClock.getCurrentTimeSeconds() > 3.0)
			{
				hapticsReady = true;
			}
		}

		// compute global reference frames for each object
		world->computeGlobalPositions(true);

		// update position and orientation of tool
		tool[RightTool]->updatePose();
		// compute interaction forces
		tool[RightTool]->computeInteractionForces();

#ifdef LEFTTOOL
		// update position and orientation of tool
		tool[LeftTool]->updatePose();
		// compute interaction forces
		tool[LeftTool]->computeInteractionForces();
#endif

		// check if the tool is touching an object
		cGenericObject* object = tool[RightTool]->m_proxyPointForceModel->m_contactPoint0->m_object;

		userSwitch[RightTool] = tool[RightTool]->getUserSwitch(1);

		// TODO: there might be a memory leak here!!!
		toolPosition[RightTool] = tool[RightTool]->m_proxyPointForceModel->getProxyGlobalPosition();
		toolRotation[RightTool] = tool[RightTool]->m_deviceGlobalRot;

		// if the tool is currently grasping an object we simply update the interaction grasp force
		// between the tool and the object (modeled by a virtual spring)
		if (graspActive[RightTool] && userSwitch[RightTool])
		{
			cMatrix3d rotDevice01 = cMul(cTrans(lastToolRotation[RightTool]), toolRotation[RightTool]);
			cMatrix3d newRot = cMul(toolRotation[RightTool], lastAppliedRotation[RightTool]);
			cVector3d newPos = cAdd(toolPosition[RightTool], cMul(toolRotation[RightTool], lastAppliedPosition[RightTool]));
			graspObject[RightTool]->setPosition(newPos);
			graspObject[RightTool]->setRotation(newRot);
		}

		// the user is not or no longer currently grasping the object
		else
		{
			// was the user grasping the object at the previous simulation loop
			if (graspActive[RightTool])
			{
				// we disable grasping
				graspActive[RightTool] = false;

				// we hide the virtual line between the tool and the grasp point
				graspLine[RightTool]->setShowEnabled(false);

				// we enable haptics interaction between the tool and the previously grasped object
				if (graspObject[RightTool] != NULL)
				{
					graspObject[RightTool]->m_imageModel->setHapticEnabled(true, true);
				}

				if (isODEEnabled)
				{
					graspObject[RightTool]->enableDynamics();
				}
			}

			// the user is touching an object
			if (object != NULL)
			{
				// check if object is attached to an external ODE parent
				cGenericType* externalParent = object->getExternalParent();
				cODEGenericBody* ODEobject = dynamic_cast<cODEGenericBody*>(externalParent);
				if (ODEobject != NULL)
				{
					// get position of tool
					cVector3d pos = tool[RightTool]->m_proxyPointForceModel->m_contactPoint0->m_globalPos;

					// check if user has enabled the user switch to gras the object
					if (userSwitch[RightTool] && !ODEobject->isStatic())
					{
						// a new object is being grasped
						graspObject[RightTool] = ODEobject;

						// grasp in now active!
						graspActive[RightTool] = true;

						// disable haptic interaction between the tool and the grasped device.
						// this is performed for stability reasons.
						graspObject[RightTool]->m_imageModel->setHapticEnabled(false, true);

						if (isODEEnabled)
						{
							graspObject[RightTool]->disableDynamics();
						}

						lastPegPosition[RightTool] = ODEobject->getPos();
						lastPegRotation[RightTool] = ODEobject->getRot();
						lastAppliedPosition[RightTool] = cTrans(lastToolRotation[RightTool]) * ((lastPegPosition[RightTool] - lastToolPosition[RightTool]) + 0.01*cNormalize(lastPegPosition[RightTool] - lastToolPosition[RightTool]));
						lastAppliedRotation[RightTool] = cMul(cTrans(lastToolRotation[RightTool]), lastPegRotation[RightTool]);
					}

					// retrieve the haptic interaction force being applied to the tool
					cVector3d force = tool[RightTool]->m_lastComputedGlobalForce;

					// apply haptic force to ODE object
					cVector3d tmpfrc = cNegate(force);
					if (hapticsReady)
					{
						ODEobject->addGlobalForceAtGlobalPos(tmpfrc, pos);
					}
				}
			}
		}

		lastToolPosition[RightTool] = toolPosition[RightTool];
		lastToolRotation[RightTool] = toolRotation[RightTool];

#ifdef LEFTTOOL
		// TODO: there might be a memory leak here!!!
		toolPosition[LeftTool] = tool[LeftTool]->m_proxyPointForceModel->getProxyGlobalPosition();
		toolRotation[LeftTool] = tool[LeftTool]->m_deviceGlobalRot;
#endif

		//if (graspActive[RightTool])
		//{
		//	vibratingObject->m_material.setVibrationFrequency(30);
		//}
		//else
		//{
		//	vibratingObject->m_material.setVibrationFrequency(1);
		//}
#ifdef LEFTTOOL
		//vibratingObject->setPos(toolPosition[LeftTool]);
		//vibratingObject->setRot(toolRotation[LeftTool]);
		
		// send forces to device
		tool[LeftTool]->applyForces();
#endif

		// send forces to device
		tool[RightTool]->applyForces();

		cVector3d pos;
		cVector3d jointAngles;
		cVector3d gimbalAngles;
		hapticDevices[RightTool]->getPosition(pos);
		hapticDevices[RightTool]->getJointAngles(jointAngles, gimbalAngles);

#if defined(DATA_STORAGE) && defined(SERIAL_INTERFACE)
		if (isButtonPressed)
		{
			if (!OutputFile[OUTPUT_FILE_HAPTIC])
			{
				string filename;
				string datetime = GetDateTime();
				filename = "resources/output/" + datetime + "_data_haptic.csv";
				OutputFile[OUTPUT_FILE_HAPTIC] = fopen(RESOURCE_PATH(filename), "ab");
#ifdef LEFTTOOL
				fprintf(OutputFile[OUTPUT_FILE_HAPTIC], "time\tright_x\tright_y\tright_z\tleft_x\tleft_y\tleft_z\n");
#else
				fprintf(OutputFile[OUTPUT_FILE_HAPTIC], "time\tx\ty\tz\tth1\tth2\tth3\tth4\tth5\tth6\n");
#endif
			}
			hapticSamplingCurrentTimestamp = timer.GetSeconds();
			if (((hapticSamplingCurrentTimestamp - hapticSamplingPreviousTimestamp) >= hapticSamplingTime) && OutputFile[OUTPUT_FILE_HAPTIC])
			{
#ifdef LEFTTOOL
				fprintf(OutputFile[OUTPUT_FILE_HAPTIC], "%6.20f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\n",
					hapticSamplingCurrentTimestamp,
					toolPosition[RightTool].x, toolPosition[RightTool].y, toolPosition[RightTool].z,
					toolPosition[LeftTool].x, toolPosition[LeftTool].y, toolPosition[LeftTool].z
					);
#else
				fprintf(OutputFile[OUTPUT_FILE_HAPTIC], "%6.20f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\n",
					hapticSamplingCurrentTimestamp,
					pos.x, pos.y, pos.z, jointAngles.x, jointAngles.y, jointAngles.z, gimbalAngles.x, gimbalAngles.y, gimbalAngles.z
					);
#endif
				hapticSamplingPreviousTimestamp = hapticSamplingCurrentTimestamp;
			}
		}
		else
		{
//			if (OutputFile[OUTPUT_FILE_HAPTIC])
//			{
//				fclose(OutputFile[OUTPUT_FILE_HAPTIC]);
//				OutputFile[OUTPUT_FILE_HAPTIC] = NULL;
//			}
			runItOnce = 1;
		}
#endif // defined(DATA_STORAGE) && defined(SERIAL_INTERFACE)

		// retrieve simulation time and compute next interval
		double time = simClock.getCurrentTimeSeconds();
		double nextSimInterval = cClamp(time, 0.001, 0.004);

		// reset clock
		simClock.reset();
		simClock.start();
		// update simulation
		ODEWorld->updateDynamics(nextSimInterval);
	}

	// exit haptics thread
	simulationFinished = true;
}
#endif // HAPTIC_DEVICE

//---------------------------------------------------------------------------

#ifdef MOTION_CAPTURE

void MotionCaptureWindowUpdateGraphics(void)
{
	glutSetWindow(motionCaptureWindowReference);

	// render world
	camera->renderView(displayW, displayH);

	// Swap buffers
	glutSwapBuffers();

	// check for any OpenGL errors
	GLenum err;
	err = glGetError();
	if (err != GL_NO_ERROR) printf("Error:  %s\n", gluErrorString(err));

	// inform the GLUT window to call updateGraphics again (next frame)
	if (simulationRunning)
	{
		glutPostRedisplay();
	}
}

//---------------------------------------------------------------------------

void MotionCaptureWindowResizeFunc(int w, int h)
{
	// update the size of the viewport
	displayW = w;
	displayH = h;
	glViewport(0, 0, displayW, displayH);
}

//---------------------------------------------------------------------------

void MotionCaptureWindowUpdateFucntion(void)
{
	// main haptic simulation loop
	while (simulationRunning)
	{
		// body gesture
		if (rtc3dClientIsConnected)
		{
#if defined(DATA_STORAGE) && defined(SERIAL_INTERFACE)
			string filename;
			string datetime = GetDateTime();

			if (isButtonPressed)
			{
				if ((MotionCapture_ActiveOutputs & MotionCapture_3D_Vector) && !OutputFile[OUTPUT_FILE_3D_VECTORS])
				{
					filename = "resources/output/" + datetime + "_data_3d.csv";
					OutputFile[OUTPUT_FILE_3D_VECTORS] = fopen(RESOURCE_PATH(filename), "ab");
					fprintf(OutputFile[OUTPUT_FILE_3D_VECTORS], "time\t");
					// all markers count is in (int)m_v3d.size()
					for (int i = 0; i < active3DMarkersCount; i++)
					{
						fprintf(OutputFile[OUTPUT_FILE_3D_VECTORS], "%s_x\t%s_y\t%s_z\t", active3DMarkerNames[i].c_str(), active3DMarkerNames[i].c_str(), active3DMarkerNames[i].c_str());
					}
					fprintf(OutputFile[OUTPUT_FILE_3D_VECTORS], "\n");
				}

				if ((MotionCapture_ActiveOutputs & MotionCapture_6D_Vector) && !OutputFile[OUTPUT_FILE_6D_VECTORS])
				{
					filename = "resources/output/" + datetime + "_data_6d.csv";
					OutputFile[OUTPUT_FILE_6D_VECTORS] = fopen(RESOURCE_PATH(filename), "ab");
					fprintf(OutputFile[OUTPUT_FILE_6D_VECTORS], "time\t");
					for (int i = 0; i < (int)m_v6d.size(); i++)
					{
						fprintf(OutputFile[OUTPUT_FILE_6D_VECTORS], "ex\tey\tez\tx\ty\tz\terr\t");
					}
					fprintf(OutputFile[OUTPUT_FILE_6D_VECTORS], "\n");
				}

				if ((MotionCapture_ActiveOutputs & MotionCapture_Analog) && !OutputFile[OUTPUT_FILE_ANALOG_DATA])
				{
					filename = "resources/output/" + datetime + "_data_analog.csv";
					OutputFile[OUTPUT_FILE_ANALOG_DATA] = fopen(RESOURCE_PATH(filename), "ab");
					fprintf(OutputFile[OUTPUT_FILE_ANALOG_DATA], "time\t");
					for (int i = 0; i < (int)m_v6d.size(); i++)
					{
						fprintf(OutputFile[OUTPUT_FILE_ANALOG_DATA], "ex\tey\tez\tx\ty\tz\terr\t");
					}
					fprintf(OutputFile[OUTPUT_FILE_ANALOG_DATA], "\n");
				}
			}
			else
			{
				if ((MotionCapture_ActiveOutputs & MotionCapture_3D_Vector) && OutputFile[OUTPUT_FILE_3D_VECTORS])
				{
					fclose(OutputFile[OUTPUT_FILE_3D_VECTORS]);
					OutputFile[OUTPUT_FILE_3D_VECTORS] = NULL;
				}

				if ((MotionCapture_ActiveOutputs & MotionCapture_6D_Vector) && OutputFile[OUTPUT_FILE_6D_VECTORS])
				{
					fclose(OutputFile[OUTPUT_FILE_6D_VECTORS]);
					OutputFile[OUTPUT_FILE_6D_VECTORS] = NULL;
				}

				if ((MotionCapture_ActiveOutputs & MotionCapture_Analog) && OutputFile[OUTPUT_FILE_ANALOG_DATA])
				{
					fclose(OutputFile[OUTPUT_FILE_ANALOG_DATA]);
					OutputFile[OUTPUT_FILE_ANALOG_DATA] = NULL;
				}
			}

			// update list controls with new data
			if ((MotionCapture_ActiveOutputs & MotionCapture_3D_Vector) && OutputFile[OUTPUT_FILE_3D_VECTORS])
			{
				m_v3d.resize(m_Client.nGetNum3d());
				// get lastest marker data from client
				m_uFrame3d = m_Client.GetLatest3d(m_v3d);

				MotionCaptureUpdate3dList(m_v3d);
			}

			if ((MotionCapture_ActiveOutputs & MotionCapture_6D_Vector) && OutputFile[OUTPUT_FILE_6D_VECTORS])
			{
				m_v6d.resize(m_Client.nGetNum6d());
				// get lastest tool data from client
				m_uFrame6d = m_Client.GetLatest6d(m_v6d);

				MotionCaptureUpdate6dList(m_v6d);
			}

			if ((MotionCapture_ActiveOutputs & MotionCapture_Analog) && OutputFile[OUTPUT_FILE_ANALOG_DATA])
			{
				m_vAnalog.resize(m_Client.nGetNumAnalog());
				// get latest analog data from client
				m_uFrameAnalog = m_Client.GetLatestAnalog(m_vAnalog);

				MotionCaptureUpdateAnalogList(m_vAnalog);
			}
#endif // defined(DATA_STORAGE) && defined(SERIAL_INTERFACE)
		}
	}

	// exit haptics thread
	motionCaptureSimulationFinished = true;
}

//---------------------------------------------------------------------------


/***************************************************************************************
Name: MotionCaptureUpdate3dList

Returns: void
Argument: vector<Position3d> & v3d

Description: Updates marker items in the list control
***************************************************************************************/
void MotionCaptureUpdate3dList(vector<Position3d> & v3d)
{
	int i;
	int nDataItems = (int)v3d.size();
	if (nDataItems <= 0)
		return;

#if defined(DATA_STORAGE) && defined(SERIAL_INTERFACE)
	motionCapture3DSamplingCurrentTimestamp = timer.GetSeconds();
	if (isButtonPressed && ((motionCapture3DSamplingCurrentTimestamp - motionCapture3DSamplingPreviousTimestamp) >= motionCaptureSamplingTime) && OutputFile[OUTPUT_FILE_3D_VECTORS])
	{
		fprintf(OutputFile[OUTPUT_FILE_3D_VECTORS], "%6.20f\t", motionCapture3DSamplingCurrentTimestamp);

		for (i = 0; i < active3DMarkersCount; i++)
		{
			fprintf(OutputFile[OUTPUT_FILE_3D_VECTORS], "%6.3f\t%6.3f\t%6.3f\t", v3d[active3DMarkerIDs[i]].x, v3d[active3DMarkerIDs[i]].y, v3d[active3DMarkerIDs[i]].z);
		}
		fprintf(OutputFile[OUTPUT_FILE_3D_VECTORS], "\n");
		motionCapture3DSamplingPreviousTimestamp = motionCapture3DSamplingCurrentTimestamp;
	}
#endif // defined(DATA_STORAGE) && defined(SERIAL_INTERFACE)
} // MotionCaptureUpdate3dList

//---------------------------------------------------------------------------

/***************************************************************************************
Name: MotionCaptureUpdate6dList

Returns: void
Argument: vector<ErrorTransformation> & v6d

Description: Updates tool items in the control list
***************************************************************************************/
void MotionCaptureUpdate6dList(vector<QuatErrorTransformation> & v6d)
{
	int i;
	float ex, ey, ez;
	int nDataItems = (int)v6d.size();
	if (nDataItems <= 0)
		return;

	QuatErrorTransformation TmpErrTrans;

#if defined(DATA_STORAGE) && defined(SERIAL_INTERFACE)
	motionCapture6DSamplingCurrentTimestamp = timer.GetSeconds();
	if (isButtonPressed && ((motionCapture6DSamplingCurrentTimestamp - motionCapture6DSamplingPreviousTimestamp) >= motionCaptureSamplingTime) && OutputFile[OUTPUT_FILE_6D_VECTORS])
	{
		fprintf(OutputFile[OUTPUT_FILE_6D_VECTORS], "%6.20f\t", motionCapture6DSamplingCurrentTimestamp);
		for (i = 0; i < nDataItems; i++)
		{
			TmpErrTrans = v6d[i];
			//// transform rads to deg
			ConvertQuaterionToEuler(TmpErrTrans.rotation.q0, TmpErrTrans.rotation.qx, TmpErrTrans.rotation.qy, TmpErrTrans.rotation.qz, ex, ey, ez);

			if (TmpErrTrans.rotation.q0 == BAD_FLOAT || TmpErrTrans.rotation.qx == BAD_FLOAT || TmpErrTrans.rotation.qy == BAD_FLOAT || TmpErrTrans.rotation.qz == BAD_FLOAT)
				ex = ey = ez = BAD_FLOAT;
			fprintf(OutputFile[OUTPUT_FILE_6D_VECTORS], "%6.3f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t", ex, ey, ez, TmpErrTrans.translation.x, TmpErrTrans.translation.y, TmpErrTrans.translation.z, TmpErrTrans.error);
		}
		fprintf(OutputFile[OUTPUT_FILE_6D_VECTORS], "\n");
		motionCapture6DSamplingPreviousTimestamp = motionCapture6DSamplingCurrentTimestamp;
	}
#endif // defined(DATA_STORAGE) && defined(SERIAL_INTERFACE)
} // MotionCaptureUpdate6dList

//---------------------------------------------------------------------------

/***************************************************************************************
Name: MotionCaptureUpdateAnalogList

Returns: void
Argument: vector<float> & vAnalog

Description: Updates analog values in list control
***************************************************************************************/
void MotionCaptureUpdateAnalogList(vector<float> & vAnalog)
{
	int i;
	int nDataItems = (int)vAnalog.size();
	if (nDataItems <= 0)
		return;

#if defined(DATA_STORAGE) && defined(SERIAL_INTERFACE)
	motionCaptureAnalogSamplingCurrentTimestamp = timer.GetSeconds();
	if (isButtonPressed && ((motionCaptureAnalogSamplingCurrentTimestamp - motionCaptureAnalogSamplingPreviousTimestamp) >= motionCaptureSamplingTime) && OutputFile[OUTPUT_FILE_ANALOG_DATA])
	{
		fprintf(OutputFile[OUTPUT_FILE_ANALOG_DATA], "%6.20f\t", motionCaptureAnalogSamplingCurrentTimestamp);

		for (i = 0; i < nDataItems; i++)
		{
			fprintf(OutputFile[OUTPUT_FILE_ANALOG_DATA], "%6.3f\t", vAnalog[i]);
		}
		fprintf(OutputFile[OUTPUT_FILE_ANALOG_DATA], "\n");
		motionCaptureAnalogSamplingPreviousTimestamp = motionCaptureAnalogSamplingCurrentTimestamp;
	}
#endif // defined(DATA_STORAGE) && defined(SERIAL_INTERFACE)
} // MotionCaptureUpdateAnalogList

//void ConvertQuaterionToEuler(float qw, float qx, float qy, float qz, float& ex, float& ey, float& ez)
//{
//	float qwsquared = qw * qw;
//	float qxsquared = qx * qx;
//	float qysquared = qy * qy;
//	float qzsquared = qz * qz;
//	ez = atan2(2.0f * (qx * qy + qz * qw), (qxsquared - qysquared - qzsquared + qwsquared)) * (180.0f / M_PI);
//	ex = atan2(2.0f * (qy * qz + qx * qw), (-qxsquared - qysquared + qzsquared + qwsquared)) * (180.0f / M_PI);
//	ey = asin(-2.0f * (qx * qz - qy * qw)) * (180.0f / M_PI);
//}

//---------------------------------------------------------------------------

void ConvertQuaterionToEuler(float qw, float qx, float qy, float qz, float& ex, float& ey, float& ez)
{
	float qwsquared = qw * qw;
	float qxsquared = qx * qx;
	float qysquared = qy * qy;
	float qzsquared = qz * qz;
	ez = atan2(2.0f * (qx * qy + qz * qw), (qxsquared - qysquared - qzsquared + qwsquared)) * (180.0f / M_PI);
	ex = atan2(2.0f * (qy * qz + qx * qw), (-qxsquared - qysquared + qzsquared + qwsquared)) * (180.0f / M_PI);
	ey = asin(-2.0f * (qx * qz - qy * qw)) * (180.0f / M_PI);
}


#endif // MOTION_CAPTURE

#ifdef VIDEO_CAPTURE

void VideoCaptureWindowUpdateFucntion(void)
{
	string filename;
	//const string videoStreamAddress = "http://192.168.0.10:554/axis-cgi/mjpg/video.cgi?resolution=640x480";
	// videoStreamAddress = "rtsp://camera/axis-media/media.amp?videocodec=h264&streamprofile=Quality";
	// it may be an address of an mjpeg stream,
	// e.g. "http://user:pass@cam_address:8081/cgi/mjpg/mjpg.cgi?.mjpg"

	//VideoCapture cap(videoStreamAddress); // open the video camera no. 0

	cap.open(videoStreamAddress);

	if (!cap.isOpened())  // if not success, exit program
	{
		cout << "ERROR: Cannot open the video file" << endl;
		return;
	}

	if (displayVideoOnScreen)
	{
		namedWindow("Video", CV_WINDOW_AUTOSIZE); //create a window called "MyVideo"
	}

	double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
	double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

	cout << "Frame Size = " << dWidth << "x" << dHeight << endl;

	frameSize.width = static_cast<int>(dWidth);
	frameSize.height = static_cast<int>(dHeight);

	// main video capture simulation loop
	while (simulationRunning)
	{
		if (isButtonPressed && !isVideoWriterOpen)
		{
			string datetime = GetDateTime();
			filename = "resources/output/" + datetime + "_video.avi";
			oVideoWriter.open(RESOURCE_PATH(filename), CV_FOURCC('D', 'I', 'V', 'X'), 20, frameSize, true); // initialize the VideoWriter object with DIVX Codec 
			// 			oVideoWriter.open(RESOURCE_PATH(filename), CV_FOURCC('M', 'J', 'P', 'G'), 20, frameSize, true); // initialize the VideoWriter object with MPEG Codec, Better quality, but need more space

			//filename = "resources/output/" + datetime + "_ve_video.avi";
			//oVirtualSceneWriter.open(RESOURCE_PATH(filename), CV_FOURCC('D', 'I', 'V', 'X'), 20, frameSize, true); // initialize the VideoWriter object with DIVX Codec 

			if (!oVideoWriter.isOpened())// || !oVirtualSceneWriter.isOpened())
			{
				//if not initialize the VideoWriter successfully
				cout << "ERROR: Failed to write the video" << endl;
			}
			else
			{
				isVideoWriterOpen = true;
			}
		}
		if (!isButtonPressed && isVideoWriterOpen)
		{
			oVideoWriter.release();
			//oVirtualSceneWriter.release();
			isVideoWriterOpen = false;
		}

		Mat frame;
		
		if (displayVideoOnScreen || isVideoWriterOpen)
		{
			bool bSuccess = cap.read(frame); // read a new frame from video
			if (!bSuccess) //if not success, break loop
			{
				cout << "ERROR: Cannot read a frame from video file" << endl;
			}
		}

#ifdef DATA_STORAGE
#ifdef SERIAL_INTERFACE
		if (isVideoWriterOpen)
		{
			oVideoWriter.write(frame); // writer the frame into the file
		}
#endif
#endif // DATA_STORAGE

		if (displayVideoOnScreen)
		{
			imshow("Video", frame); //show the frame in "MyVideo" window

			if (waitKey(10) == 'x') //wait for 'x' key press for 30ms. If 'x' key is pressed, break loop
			{
				break;
			}
		}
	}

	if (displayVideoOnScreen)
	{
		cvDestroyWindow("Video");
	}

	// exit haptics thread
	videoCaptureSimulationFinished = true;
}

#endif // VIDEO_CAPTURE

//---------------------------------------------------------------------------

void ConfigurationLoad(const char *configFilename)
{
#ifdef TIXML_USE_STL
	TiXmlDocument configDocument(configFilename);

	if (!configDocument.LoadFile())
	{
		printf("Could not load 'configuration.xml'. Error='%s'. Exiting.\n", configDocument.ErrorDesc());
#ifdef MOTION_CAPTURE
		MotionCapture_ActiveOutputs = MotionCapture_None;

		active3DMarkersCount = 1;
		active3DMarkerIDs = new int[active3DMarkersCount];
		active3DMarkerIDs[0] = 0;
		active3DMarkerNames = new string[active3DMarkersCount];
		active3DMarkerNames[0] = "Marker";

		mainWindowPosX = (screenW - WINDOW_SIZE_W) / 4;
		mainWindowPosY = (screenH - WINDOW_SIZE_H) / 4;
		motionCaptureWindowPosX = 3 * (screenW - WINDOW_SIZE_W) / 4;
		motionCaptureWindowPosY = (screenH - WINDOW_SIZE_H) / 4;
		mainWindowWidth = 600;
		mainWindowHeight = 600;
		motionCaptureWindowWidth = 600;
		motionCaptureWindowHeight = 600;
#else
		mainWindowPosX = (screenW - WINDOW_SIZE_W) / 2;
		mainWindowPosY = (screenH - WINDOW_SIZE_H) / 2;
		mainWindowWidth = 600;
		mainWindowHeight = 600;
#endif // MOTION_CAPTURE
		return;
	}

	TiXmlHandle configXMLHandle(&configDocument);
	TiXmlElement* configXMLElement;
	TiXmlHandle configRootHandle(0);

	// block: root
	{
		configXMLElement = configXMLHandle.FirstChildElement().Element();
		// should always have a valid root but handle gracefully if it does
		if (!configXMLElement) return;
		//m_name = pElem->Value();

		// save this for later
		configRootHandle = TiXmlHandle(configXMLElement);
	}

	// block: Window attributes
	{
		TiXmlElement* configWindowNode = configRootHandle.FirstChild("Windows").FirstChild().Element();
		for (configWindowNode; configWindowNode; configWindowNode = configWindowNode->NextSiblingElement())
		{
			const char *windowName = configWindowNode->Attribute("name");
			if (strcmp(windowName, "mainWindow") == 0)
			{
				configWindowNode->QueryIntAttribute("x", &mainWindowPosX);
				configWindowNode->QueryIntAttribute("y", &mainWindowPosY);
				configWindowNode->QueryIntAttribute("w", &mainWindowWidth);
				configWindowNode->QueryIntAttribute("h", &mainWindowHeight);
			}
#ifdef MOTION_CAPTURE
			else if (strcmp(windowName, "motionCaptureWindow") == 0)
			{
				configWindowNode->QueryIntAttribute("x", &motionCaptureWindowPosX);
				configWindowNode->QueryIntAttribute("y", &motionCaptureWindowPosY);
				configWindowNode->QueryIntAttribute("w", &motionCaptureWindowWidth);
				configWindowNode->QueryIntAttribute("h", &motionCaptureWindowHeight);
			}
#endif // MOTION_CAPTURE
		}
	}

#ifdef MOTION_CAPTURE
	// block: Points
	{
		configXMLElement = configRootHandle.FirstChild("Points").Element();
		if (configXMLElement)
		{
			active3DMarkersCount = atoi(configXMLElement->Attribute("count"));
			active3DMarkerIDs = new int[active3DMarkersCount];
			active3DMarkerNames = new string[active3DMarkersCount];

			int i = 0;
			for (configXMLElement = configRootHandle.FirstChild("Points").FirstChild().Element(); configXMLElement; configXMLElement = configXMLElement->NextSiblingElement())
			{
				const char *pointName = configXMLElement->Attribute("name");
				active3DMarkerNames[i].assign(pointName);
				configXMLElement->QueryIntAttribute("id", &active3DMarkerIDs[i]);
				active3DMarkerIDs[i]--;
				i++;
			}
			//pElem->QueryDoubleAttribute("timeout", &m_connection.timeout);
		}
	}
#endif // MOTION_CAPTURE

	// block: Peg Board Configuration
	{
		configXMLElement = configRootHandle.FirstChild("BoardConfigurations").Element();
		if (configXMLElement)
		{
			int activePegBoardConfiguration = atoi(configXMLElement->Attribute("active_id"));
			int id;

			for (TiXmlElement* configuration = configXMLElement->FirstChild("Configuration")->ToElement(); configuration; configuration = configuration->NextSiblingElement())
			{
				id = atoi(configuration->Attribute("id"));
				if (id == activePegBoardConfiguration)
				{
					numberOfPegs = atoi(configuration->Attribute("peg_count"));
					numberOfAxises = atoi(configuration->Attribute("axis_count"));
					
					configuration->QueryBoolAttribute("ODE_enabled", &isODEEnabled);

					// ODE object
					ODEPeg = new cODEGenericBody*[numberOfPegs];
					ODEAxis = new cODEGenericBody*[numberOfAxises];

					// define postion and roation variables for peg object
					pegPosition = new cVector3d[numberOfPegs];
					pegRotation = new cMatrix3d[numberOfPegs];

					// define postion and roation variables for axis object
					axisPosition = new cVector3d[numberOfAxises];

					// create a virtual mesh  that will be used for the geometry
					// representation of the dynamic body
					peg = new cMesh*[numberOfPegs];
					axis = new cMesh*[numberOfAxises];

					int peg_index = 0, axis_index = 0;
					for (TiXmlElement* configurationItem = configuration->FirstChild("Item")->ToElement(); configurationItem; configurationItem = configurationItem->NextSiblingElement())
					{
						const char *itemType = configurationItem->Attribute("type");
						if (strcmp(itemType, "Peg") == 0 && peg_index < numberOfPegs)
						{
							double x, y, z;
							int rotaion_vector_x, rotaion_vector_y, rotaion_vector_z, rotation_degree;

							// position and oriente ODEPeg
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							configurationItem->QueryIntAttribute("rotaion_vector_x", &rotaion_vector_x);
							configurationItem->QueryIntAttribute("rotaion_vector_y", &rotaion_vector_y);
							configurationItem->QueryIntAttribute("rotaion_vector_z", &rotaion_vector_z);
							configurationItem->QueryIntAttribute("rotation_degree", &rotation_degree);
							pegPosition[peg_index] = cVector3d(x, y, z);
							pegRotation[peg_index].identity();
							pegRotation[peg_index].rotate(cVector3d(rotaion_vector_x, rotaion_vector_y, rotaion_vector_z), cDegToRad(rotation_degree));
							peg_index++;
						}
						else if (strcmp(itemType, "Axis") == 0 && axis_index < numberOfAxises)
						{
							double x, y, z;
							int rotaion_vector_x, rotaion_vector_y, rotaion_vector_z, rotation_degree;

							// position ODEAxis
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							axisPosition[axis_index] = cVector3d(x, y, z);
							axis_index++;
						}
					}
				}
			}
		}
	}

	{ // block: Camera
		configXMLElement = configRootHandle.FirstChild("Camera").FirstChild().Element();
		for (configXMLElement; configXMLElement; configXMLElement = configXMLElement->NextSiblingElement())
		{
			const char *cameraPropertyName = configXMLElement->Attribute("name");
			if (strcmp(cameraPropertyName, "cameraLookAt") == 0)
			{
				double x, y, z;
				configXMLElement->QueryDoubleAttribute("x", &x);
				configXMLElement->QueryDoubleAttribute("y", &y);
				configXMLElement->QueryDoubleAttribute("z", &z);
				cameraLookAt = cVector3d(x, y, z);
			}
			else if (strcmp(cameraPropertyName, "cameraPostion") == 0)
			{
				double x, y, z;
				configXMLElement->QueryDoubleAttribute("x", &x);
				configXMLElement->QueryDoubleAttribute("y", &y);
				configXMLElement->QueryDoubleAttribute("z", &z);
				cameraPostion = cVector3d(x, y, z);
			}
			else if (strcmp(cameraPropertyName, "cameraUpVector") == 0)
			{
				double x, y, z;
				configXMLElement->QueryDoubleAttribute("x", &x);
				configXMLElement->QueryDoubleAttribute("y", &y);
				configXMLElement->QueryDoubleAttribute("z", &z);
				cameraUpVector = cVector3d(x, y, z);
			}
		}
	}

#ifdef MOTION_CAPTURE
	// block: Motion Capture
	{
		TiXmlElement* configOutputNode = configRootHandle.FirstChild("MotionCapture").FirstChild().Element();
		for (configOutputNode; configOutputNode; configOutputNode = configOutputNode->NextSiblingElement())
		{
			const char *outputType = configOutputNode->Attribute("type");
			bool isActive;
			configOutputNode->QueryBoolAttribute("enable", &isActive);
			if (strcmp(outputType, "3D Vector") == 0)
			{
				MotionCapture_ActiveOutputs |= isActive ? MotionCapture_3D_Vector : MotionCapture_None;
			}
			else if (strcmp(outputType, "6D Vector") == 0)
			{
				MotionCapture_ActiveOutputs |= isActive ? MotionCapture_6D_Vector : MotionCapture_None;
			}
			else if (strcmp(outputType, "Analog") == 0)
			{
				MotionCapture_ActiveOutputs |= isActive ? MotionCapture_Analog : MotionCapture_None;
			}
		}
	}
#endif // MOTION_CAPTURE

#ifdef SERIAL_INTERFACE
	// block: Serial
	{
		configXMLElement = configRootHandle.FirstChild("Serial").Element();
		if (configXMLElement)
		{
			serialPortName.assign(configXMLElement->Attribute("port"));
		}
	}
#endif // SERIAL_INTERFACE

#ifdef DATA_STORAGE
	// block: Serial
	{
		configXMLElement = configRootHandle.FirstChild("DataStorage").Element();
		if (configXMLElement)
		{
			float aTime;
			configXMLElement->QueryFloatAttribute("hapticSamplingTime_ms", &aTime);
			hapticSamplingTime = aTime / 1000.0;
			configXMLElement->QueryFloatAttribute("motionCaptureSamplingTime_ms", &aTime);
			motionCaptureSamplingTime = aTime / 1000.0;
		}
	}
#endif // DATA_STORAGE

#ifdef VIDEO_CAPTURE
	// block: Serial
	{
		configXMLElement = configRootHandle.FirstChild("Video").Element();
		if (configXMLElement)
		{
			videoStreamAddress.assign(configXMLElement->Attribute("streamAddress"));
			configXMLElement->QueryBoolAttribute("displayOnScreen", &displayVideoOnScreen);
		}
	}
#endif // VIDEO_CAPTURE

#else // TIXML_USE_STL

	{ // Load peg board configuration
		numberOfPegs = 1;
		numberOfAxises = 4;

		// ODE object
		ODEPeg = new cODEGenericBody*[numberOfPegs];
		ODEAxis = new cODEGenericBody*[numberOfAxises];

		// define postion and roation variables for peg object
		pegPosition = new cVector3d[numberOfPegs];
		pegRotation = new cMatrix3d[numberOfPegs];

		// define postion and roation variables for axis object
		axisPosition = new cVector3d[numberOfAxises];

		// default distance between both gears when demo starts
		double DISTANCE = 0.4;

		if (numberOfPegs == 1)
		{
			// position and oriente ODEPeg 1
			pegPosition[0] = cVector3d(0.2, -DISTANCE, -0.3);
			pegRotation[0].identity();
			pegRotation[0].rotate(cVector3d(0, 0, 1), cDegToRad(90));
		}
		else if (numberOfPegs == 2)
		{
			// position and oriente ODEPeg 1
			pegPosition[0] = cVector3d(0.2, -DISTANCE, -0.3);
			pegRotation[0].identity();
			pegRotation[0].rotate(cVector3d(0, 0, 1), cDegToRad(90));

			// position and oriente ODEPeg 2
			pegPosition[1] = cVector3d(-0.2, DISTANCE, -0.3);
			pegRotation[1].rotate(cVector3d(0, 0, 1), cDegToRad(14));
		}

		if (numberOfAxises == 1)
		{
			// position ODEAxis 1
			axisPosition[0] = cVector3d(0.0, -DISTANCE, -0.3);
		}
		else if (numberOfAxises == 2)
		{
			// position ODEAxis 1
			axisPosition[0] = cVector3d(0.0, -DISTANCE, -0.3);

			// position ODEAxis 2
			axisPosition[1] = cVector3d(0.0, DISTANCE, -0.3);
		}
		else if (numberOfAxises == 4)
		{
			// position ODEAxis 1
			axisPosition[0] = cVector3d(0.2, -DISTANCE, -0.1);

			// position ODEAxis 2
			axisPosition[1] = cVector3d(0.2, DISTANCE, -0.1);

			// position ODEAxis 3
			axisPosition[2] = cVector3d(-0.2, -DISTANCE, -0.1);

			// position ODEAxis 4
			axisPosition[3] = cVector3d(-0.2, DISTANCE, -0.1);
		}

		// create a virtual mesh  that will be used for the geometry
		// representation of the dynamic body
		peg = new cMesh*[numberOfPegs];
		axis = new cMesh*[numberOfAxises];
	}

	{ // Camera
		cameraLookAt = cVector3d(1.2, 0.0, 2.5);
		cameraPostion = cVector3d(0.0, 0.0, -0.5);
		cameraUpVector = cVector3d(0.0, 0.0, 1.0);
	}
#ifdef MOTION_CAPTURE
	MotionCapture_ActiveOutputs = MotionCapture_3D_Vector;

	active3DMarkersCount = 1;
	active3DMarkerIDs = new int[active3DMarkersCount];
	active3DMarkerIDs[0] = 0;
	active3DMarkerNames = new string[active3DMarkersCount];
	active3DMarkerNames[0] = "Marker";

	mainWindowPosX = (screenW - WINDOW_SIZE_W) / 4;
	mainWindowPosY = (screenH - WINDOW_SIZE_H) / 4;
	motionCaptureWindowPosX = 3 * (screenW - WINDOW_SIZE_W) / 4;
	motionCaptureWindowPosY = (screenH - WINDOW_SIZE_H) / 4;
	mainWindowWidth = 600;
	mainWindowHeight = 600;
	motionCaptureWindowWidth = 600;
	motionCaptureWindowHeight = 600;
#else
	mainWindowPosX = (screenW - WINDOW_SIZE_W) / 2;
	mainWindowPosY = (screenH - WINDOW_SIZE_H) / 2;
	mainWindowWidth = 600;
	mainWindowHeight = 600;
#endif // MOTION_CAPTURE
#ifdef SERIAL_INTERFACE
	serialPortName = "COM5";
#endif // SERIAL_INTERFACE
#ifdef VIDEO_CAPTURE
	videoStreamAddress = "rtsp://camera/axis-media/media.amp?videocodec=h264&streamprofile=Quality";
#endif // VIDEO_CAPTURE

#endif // TIXML_USE_STL
}

//---------------------------------------------------------------------------

void ConfigurationSave(const char *configFilename)
{
#ifdef TIXML_USE_STL
	TiXmlDocument configDocument(configFilename);

	if (!configDocument.LoadFile())
	{
		printf("Could not save 'configuration.xml'. Error='%s'. Exiting.\n", configDocument.ErrorDesc());
		return;
	}

	TiXmlHandle configXMLHandle(&configDocument);
	TiXmlElement* configXMLElement;
	TiXmlHandle configRootHandle(0);

	// block: root
	{
		configXMLElement = configXMLHandle.FirstChildElement().Element();
		// should always have a valid root but handle gracefully if it does
		if (!configXMLElement) return;
		//m_name = pElem->Value();

		// save this for later
		configRootHandle = TiXmlHandle(configXMLElement);
	}

	// block: Window attributes
	{
		TiXmlElement* configWindowNode = configRootHandle.FirstChild("Windows").FirstChild().Element();
		for (configWindowNode; configWindowNode; configWindowNode = configWindowNode->NextSiblingElement())
		{
			const char *windowName = configWindowNode->Attribute("name");
			if (strcmp(windowName, "mainWindow") == 0)
			{
				glutSetWindow(mainWindowReference);
				configWindowNode->SetAttribute("x", glutGet((GLenum)GLUT_WINDOW_X));
				configWindowNode->SetAttribute("y", glutGet((GLenum)GLUT_WINDOW_Y));
				configWindowNode->SetAttribute("w", glutGet((GLenum)GLUT_WINDOW_WIDTH));
				configWindowNode->SetAttribute("h", glutGet((GLenum)GLUT_WINDOW_HEIGHT));
			}
#ifdef MOTION_CAPTURE
			else if (strcmp(windowName, "motionCaptureWindow") == 0)
			{
				glutSetWindow(motionCaptureWindowReference);
				configWindowNode->SetAttribute("x", glutGet((GLenum)GLUT_WINDOW_X));
				configWindowNode->SetAttribute("y", glutGet((GLenum)GLUT_WINDOW_Y));
				configWindowNode->SetAttribute("w", glutGet((GLenum)GLUT_WINDOW_WIDTH));
				configWindowNode->SetAttribute("h", glutGet((GLenum)GLUT_WINDOW_HEIGHT));
			}
#endif // MOTION_CAPTURE
		}
	}
	configDocument.SaveFile();
#else // TIXML_USE_STL
// Do nothing!	
#endif // TIXML_USE_STL
}

//---------------------------------------------------------------------------

void ConfigurationCreateTemplateXML()
{
#ifdef TIXML_USE_STL
	TiXmlDocument doc;
	TiXmlElement* msg;
	TiXmlDeclaration* decl = new TiXmlDeclaration("1.0", "", "");
	doc.LinkEndChild(decl);

	TiXmlElement * root = new TiXmlElement("Configuration");
	doc.LinkEndChild(root);

	//TiXmlComment * comment = new TiXmlComment();
	//comment->SetValue(" Settings for MyApp ");
	//root->LinkEndChild(comment);

	TiXmlElement * windows = new TiXmlElement("Windows");
	root->LinkEndChild(windows);

	TiXmlElement * window;
	window = new TiXmlElement("Window");
	windows->LinkEndChild(window);
	window->SetAttribute("name", "mainWindow");
	window->SetAttribute("x", 330);
	window->SetAttribute("y", 120);
	window->SetAttribute("w", 600);
	window->SetAttribute("h", 600);

	window = new TiXmlElement("Window");
	windows->LinkEndChild(window);
	window->SetAttribute("name", "motionCaptureWindow");
	window->SetAttribute("x", 990);
	window->SetAttribute("y", 120);
	window->SetAttribute("w", 600);
	window->SetAttribute("h", 600);

	TiXmlElement * points = new TiXmlElement("Points");
	root->LinkEndChild(points);
	points->SetAttribute("count", "6");

	TiXmlElement * point;
	point = new TiXmlElement("Point");
	points->LinkEndChild(point);
	point->SetAttribute("name", "Torso");
	point->SetAttribute("id", 1);

	point = new TiXmlElement("Point");
	points->LinkEndChild(point);
	point->SetAttribute("name", "AC");
	point->SetAttribute("id", 2);

	point = new TiXmlElement("Point");
	points->LinkEndChild(point);
	point->SetAttribute("name", "EL");
	point->SetAttribute("id", 3);

	point = new TiXmlElement("Point");
	points->LinkEndChild(point);
	point->SetAttribute("name", "EM");
	point->SetAttribute("id", 4);

	point = new TiXmlElement("Point");
	points->LinkEndChild(point);
	point->SetAttribute("name", "RS");
	point->SetAttribute("id", 5);

	point = new TiXmlElement("Point");
	points->LinkEndChild(point);
	point->SetAttribute("name", "US");
	point->SetAttribute("id", 6);

	doc.SaveFile(RESOURCE_PATH("resources/configuration_template.xml"));
#endif // TIXML_USE_STL
}

//---------------------------------------------------------------------------

string GetDateTime(void)
{
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);

	// for more information about date/time format
	strftime(buf, sizeof(buf), "%Y%m%d_%H%M%S", &tstruct);

	return buf;
}

#ifdef SERIAL_INTERFACE
void SerialInterfaceUpdateFucntion(void)
{
	//-----------------------------------------------------------------------
	// Connect to laparoscopic forceps using serial port
	//-----------------------------------------------------------------------

	serial.Open(serialPortName.c_str());
	if (serial.IsOpen())
	{
		serial.Setup(CSerial::EBaud9600, CSerial::EData8, CSerial::EParNone, CSerial::EStop1);
		serial.SetupHandshaking(CSerial::EHandshakeHardware);
		serialIsConnected = true;
	}

	// main haptic simulation loop
	while (simulationRunning)
	{
		/**** BUG: if user presses and releases button fast enough, ****/
		/**** the tool doesn't open again.                          ****/
		if (serialIsConnected)
		{
			//check from arduino if button was pressed
			char* lpBuffer = new char[500];
			DWORD dwBytesRead = 0;
			do
			{
				serial.Read(lpBuffer, 500, &dwBytesRead);
			} while (dwBytesRead == sizeof(lpBuffer));
			
			//if button is pressed, close jaw
			//else, open jaw
			if (lastRead != lpBuffer[0])
			{
				if (lpBuffer[0] == '0') // Close
				{
					lastRead = '0';
					isButtonPressed = true;
					hapticSamplingPreviousTimestamp = -1 * hapticSamplingTime;
					motionCapture3DSamplingPreviousTimestamp = -1 * motionCaptureSamplingTime;
					motionCapture6DSamplingPreviousTimestamp = -1 * motionCaptureSamplingTime;
					motionCaptureAnalogSamplingPreviousTimestamp = -1 * motionCaptureSamplingTime;
					timer.Start();
				}
				if (lpBuffer[0] == '1') // Open
				{
					lastRead = '1';
					isButtonPressed = false;
				}
			}
			delete[] lpBuffer;
		}
	}

	// exit serial interface thread
	serialInterfaceThreadFinished = true;
}
#endif // SERIAL_INTERFACE
