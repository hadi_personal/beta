#include "threads.h"

#include "common.h"
#include "touchManipulation.h"
#include <iostream>
#include <fstream>
#include "matlabInterface.h"
#include "timer.h"
#include "common.h"


int subjectNumber, workingMode;
string workingModeLabels[] = { "", "DHMM1", "MHMM1", "MHMM2", "Maximum", "Medium", "Minimum", "HCRF" };

CTimer pedalTimer, taskTimer;

double taskPathLength[MAX_DEVICES];
double taskEconomyOfMotion[MAX_DEVICES];
double taskMotionSmoothness[MAX_DEVICES];
cVector3d previousTooltipPositions[MAX_DEVICES];

bool taskHasStarted = false;
float previousTime;

std::ofstream tooltipFile, mocapFile, kpValuesOutputFile;

int classID[MAX_DEVICES];
double similarity[MAX_DEVICES];
bool similaritiesAreValid[MAX_DEVICES];

TooltipSpatialDatatype *tooltipSpatialData;

void SetupPedal()
{
	serial.Open(settings.serialPortName.c_str());
	if (serial.IsOpen())
	{
		serial.Setup(CSerial::EBaud9600, CSerial::EData8, CSerial::EParNone, CSerial::EStop1);
		serial.SetupHandshaking(CSerial::EHandshakeHardware);
		serialIsConnected = true;
	}
};

void SetupNDI()
{

#ifdef MOTION_CAPTURE
	//-----------------------------------------------------------------------
	// Connect to NDI First Principles using network socket 
	//-----------------------------------------------------------------------
	char szPath[128] = "";
	gethostname(szPath, sizeof(szPath));
	ndiMotion->m_sHostname = ndiMotion->szHost;
	unsigned long tmp = ndiMotion->m_Client.ulGetIPAddress(ndiMotion->szHost);
	ndiMotion->m_dwIP = ntohl(tmp);

	ndiMotion->m_Client.bConnect(ndiMotion->m_dwIP);
	if (ndiMotion->m_Client.bIsConnected())
	{
		ndiMotion->rtc3dClientIsConnected = true;
		ndiMotion->m_Client.nSendCommand("StreamFrames AllFrames");
	}
#endif // MOTION_CAPTURE
}

void runningPedalThread(void)
{
	char lastRead = NULL;
	// main haptic simulation loop
	while (simulationRunning)
	{
		if (serialIsConnected)
		{
			//check from arduino if button was pressed
			char* lpBuffer = new char[20];
			DWORD dwBytesRead = 0;
			do
			{
				serial.Read(lpBuffer, sizeof(lpBuffer), &dwBytesRead);
			} while (dwBytesRead == sizeof(lpBuffer));

			if (lastRead != lpBuffer[0])
			{
				if (lpBuffer[0] == '0') // Open
				{
					lastRead = '0';

					string path = settings.outputDirectory;
					if (path.back() != '/')
						path.append("/");

					string hapticFile = path + currentDateTime() + "_data_haptic.csv";
					tooltipFile.open(hapticFile);
					if (!tooltipFile.is_open())
						perror("error");
#ifdef MOTION_CAPTURE
					string ndiFile = path + "NDI_record_" + currentDateTime() + ".txt";
					mocapFile.open(ndiFile);
					if (!mocapFile.is_open())
						perror("error");
#endif
					pedalTimer.Start();
					previousTime = pedalTimer.GetSeconds();
					isPedalPressed = true;
				}
				if (lpBuffer[0] == '1') // close
				{
					lastRead = '1';
					if (isPedalPressed)
					{
						isPedalPressed = false;
						tooltipFile.close();
#ifdef MOTION_CAPTURE
						mocapFile.close();
#endif
					}
				}
			}

			delete[] lpBuffer;
		}
		Sleep(300);
	}

	// exit serial interface thread
	serialInterfaceThreadFinished = true;
}

void dataRecorderThreadHandler(void)
{
	tooltipSpatialData = new TooltipSpatialDatatype[MAX_DEVICES]();
	InitMatlab();
	while (simulationRunning)
	{
		float currentTime = applicationTimer.GetSeconds();

		cVector3d jointAnglesL, jointAnglesR;
		cVector3d gimbalAnglesL, gimbalAnglesR;
		//genericHapticDevices[0]->getJointAngles(jointAnglesL, gimbalAnglesL);
		//genericHapticDevices[1]->getJointAngles(jointAnglesR, gimbalAnglesR);

		cVector3d tooltipPositions[] = { generic3dofPointers[0]->getProxyGlobalPos(), generic3dofPointers[1]->getProxyGlobalPos() };

		if (((currentTime - previousTime) >= settings.hapticSamplingTime))
		{
			if (isPedalPressed)
			{
				string tmp = std::to_string(pedalTimer.GetSeconds()) + "\t" +
					tooltipPositions[0].str(settings.precisionOfFloatingPoints) + "\t" +
					tooltipPositions[1].str(settings.precisionOfFloatingPoints) + "\t";// +
				//jointAnglesL.str(settings.precisionOfFloatingPoints) + "\t" + gimbalAnglesL.str(settings.precisionOfFloatingPoints) + "\t" +
				//jointAnglesR.str(settings.precisionOfFloatingPoints) + "\t" + gimbalAnglesR.str(settings.precisionOfFloatingPoints) + "\t";

				string tool0button0 = generic3dofPointers[0]->getUserSwitch(0) == false ? "0" : "1";
				string tool1button0 = generic3dofPointers[1]->getUserSwitch(0) == false ? "0" : "1";
				string tool0button1 = generic3dofPointers[0]->getUserSwitch(1) == false ? "0" : "1";
				string tool1button1 = generic3dofPointers[1]->getUserSwitch(1) == false ? "0" : "1";
				tmp += tool0button0 + "\t" + tool1button0 + "\t" + tool0button1 + "\t" + tool1button1;

				if (tooltipFile.is_open())
					tooltipFile << tmp << std::endl;
			}
#ifdef MOTION_CAPTURE
			if (ndiMotion->MotionCapture_ActiveOutputs & ndiMotion->MotionCapture_3D_Vector)
			{
				vector<Position3d> m_v3d;
				m_v3d.resize(ndiMotion->m_Client.nGetNum3d());
				// get lastest marker data from client
				ndiMotion->m_uFrame3d = ndiMotion->m_Client.GetLatest3d(ndiMotion->m_v3d);
				string tmp = std::to_string(currentTime);

				for (int i = 0; i < ndiMotion->active3DMarkersCount; i++)
				{
					char a[100];
					sprintf(a, "{%s: %6.3f %6.3f %6.3f\t}",
						ndiMotion->active3DMarkerNames[i].c_str(),
						m_v3d[ndiMotion->active3DMarkerIDs[i]].x,
						m_v3d[ndiMotion->active3DMarkerIDs[i]].y,
						m_v3d[ndiMotion->active3DMarkerIDs[i]].z);
				}
				mocapFile << tmp << std::endl;
			}
#endif
			for (int toolID = 0; toolID < numberOfHapticDevices; toolID++)
			{
				for (int i = 1; i < SPATIAL_DATA_WINDOW_SIZE; i++)
				{
					tooltipSpatialData[toolID].time[i - 1] = tooltipSpatialData[toolID].time[i];

					tooltipSpatialData[toolID].raw[Px][i - 1] = tooltipSpatialData[toolID].raw[Px][i];
					tooltipSpatialData[toolID].raw[Py][i - 1] = tooltipSpatialData[toolID].raw[Py][i];
					tooltipSpatialData[toolID].raw[Pz][i - 1] = tooltipSpatialData[toolID].raw[Pz][i];

					tooltipSpatialData[toolID].filtered[Px][i - 1] = tooltipSpatialData[toolID].filtered[Px][i];
					tooltipSpatialData[toolID].filtered[Py][i - 1] = tooltipSpatialData[toolID].filtered[Py][i];
					tooltipSpatialData[toolID].filtered[Pz][i - 1] = tooltipSpatialData[toolID].filtered[Pz][i];

					tooltipSpatialData[toolID].raw[Vx][i - 1] = tooltipSpatialData[toolID].raw[Vx][i];
					tooltipSpatialData[toolID].raw[Vy][i - 1] = tooltipSpatialData[toolID].raw[Vy][i];
					tooltipSpatialData[toolID].raw[Vz][i - 1] = tooltipSpatialData[toolID].raw[Vz][i];

					tooltipSpatialData[toolID].filtered[Vx][i - 1] = tooltipSpatialData[toolID].filtered[Vx][i];
					tooltipSpatialData[toolID].filtered[Vy][i - 1] = tooltipSpatialData[toolID].filtered[Vy][i];
					tooltipSpatialData[toolID].filtered[Vz][i - 1] = tooltipSpatialData[toolID].filtered[Vz][i];

					tooltipSpatialData[toolID].raw[Ax][i - 1] = tooltipSpatialData[toolID].raw[Ax][i];
					tooltipSpatialData[toolID].raw[Ay][i - 1] = tooltipSpatialData[toolID].raw[Ay][i];
					tooltipSpatialData[toolID].raw[Az][i - 1] = tooltipSpatialData[toolID].raw[Az][i];

					tooltipSpatialData[toolID].filtered[Ax][i - 1] = tooltipSpatialData[toolID].filtered[Ax][i];
					tooltipSpatialData[toolID].filtered[Ay][i - 1] = tooltipSpatialData[toolID].filtered[Ay][i];
					tooltipSpatialData[toolID].filtered[Az][i - 1] = tooltipSpatialData[toolID].filtered[Az][i];

					tooltipSpatialData[toolID].jerk[Jx][i - 1] = tooltipSpatialData[toolID].jerk[Jx][i];
					tooltipSpatialData[toolID].jerk[Jy][i - 1] = tooltipSpatialData[toolID].jerk[Jy][i];
					tooltipSpatialData[toolID].jerk[Jz][i - 1] = tooltipSpatialData[toolID].jerk[Jz][i];

					tooltipSpatialData[toolID].filteredJerk[Jx][i - 1] = tooltipSpatialData[toolID].filteredJerk[Jx][i];
					tooltipSpatialData[toolID].filteredJerk[Jy][i - 1] = tooltipSpatialData[toolID].filteredJerk[Jy][i];
					tooltipSpatialData[toolID].filteredJerk[Jz][i - 1] = tooltipSpatialData[toolID].filteredJerk[Jz][i];
				}

				tooltipSpatialData[toolID].time[END] = currentTime;

				tooltipSpatialData[toolID].raw[Px][END] = tooltipPositions[toolID].x;
				tooltipSpatialData[toolID].raw[Py][END] = tooltipPositions[toolID].y;
				tooltipSpatialData[toolID].raw[Pz][END] = tooltipPositions[toolID].z;

				tooltipSpatialData[toolID].filtered[Px][END] = tooltipPositions[toolID].x;
				tooltipSpatialData[toolID].filtered[Py][END] = tooltipPositions[toolID].y;
				tooltipSpatialData[toolID].filtered[Pz][END] = tooltipPositions[toolID].z;

				tooltipSpatialData[toolID].raw[Vx][END] = (tooltipSpatialData[toolID].raw[Px][END] - tooltipSpatialData[toolID].raw[Px][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].raw[Vy][END] = (tooltipSpatialData[toolID].raw[Py][END] - tooltipSpatialData[toolID].raw[Py][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].raw[Vz][END] = (tooltipSpatialData[toolID].raw[Pz][END] - tooltipSpatialData[toolID].raw[Pz][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);

				tooltipSpatialData[toolID].filtered[Vx][END] = (tooltipSpatialData[toolID].raw[Vx][END - 3] + tooltipSpatialData[toolID].raw[Vx][END - 2] + tooltipSpatialData[toolID].raw[Vx][END - 1] + tooltipSpatialData[toolID].raw[Vx][END]) / 4;
				tooltipSpatialData[toolID].filtered[Vy][END] = (tooltipSpatialData[toolID].raw[Vy][END - 3] + tooltipSpatialData[toolID].raw[Vy][END - 2] + tooltipSpatialData[toolID].raw[Vy][END - 1] + tooltipSpatialData[toolID].raw[Vy][END]) / 4;
				tooltipSpatialData[toolID].filtered[Vz][END] = (tooltipSpatialData[toolID].raw[Vz][END - 3] + tooltipSpatialData[toolID].raw[Vz][END - 2] + tooltipSpatialData[toolID].raw[Vz][END - 1] + tooltipSpatialData[toolID].raw[Vz][END]) / 4;

				tooltipSpatialData[toolID].raw[Ax][END] = (tooltipSpatialData[toolID].raw[Vx][END] - tooltipSpatialData[toolID].raw[Vx][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].raw[Ay][END] = (tooltipSpatialData[toolID].raw[Vy][END] - tooltipSpatialData[toolID].raw[Vy][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].raw[Az][END] = (tooltipSpatialData[toolID].raw[Vz][END] - tooltipSpatialData[toolID].raw[Vz][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);

				tooltipSpatialData[toolID].filtered[Ax][END] = (tooltipSpatialData[toolID].raw[Ax][END - 3] + tooltipSpatialData[toolID].raw[Ax][END - 2] + tooltipSpatialData[toolID].raw[Ax][END - 1] + tooltipSpatialData[toolID].raw[Ax][END]) / 4;
				tooltipSpatialData[toolID].filtered[Ay][END] = (tooltipSpatialData[toolID].raw[Ay][END - 3] + tooltipSpatialData[toolID].raw[Ay][END - 2] + tooltipSpatialData[toolID].raw[Ay][END - 1] + tooltipSpatialData[toolID].raw[Ay][END]) / 4;
				tooltipSpatialData[toolID].filtered[Az][END] = (tooltipSpatialData[toolID].raw[Az][END - 3] + tooltipSpatialData[toolID].raw[Az][END - 2] + tooltipSpatialData[toolID].raw[Az][END - 1] + tooltipSpatialData[toolID].raw[Az][END]) / 4;

				tooltipSpatialData[toolID].jerk[Jx][END] = (tooltipSpatialData[toolID].raw[Ax][END] - tooltipSpatialData[toolID].raw[Ax][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].jerk[Jy][END] = (tooltipSpatialData[toolID].raw[Ay][END] - tooltipSpatialData[toolID].raw[Ay][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].jerk[Jz][END] = (tooltipSpatialData[toolID].raw[Az][END] - tooltipSpatialData[toolID].raw[Az][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);

				tooltipSpatialData[toolID].filteredJerk[Jx][END] = (tooltipSpatialData[toolID].jerk[Jx][END - 3] + tooltipSpatialData[toolID].jerk[Jx][END - 2] + tooltipSpatialData[toolID].jerk[Jx][END - 1] + tooltipSpatialData[toolID].jerk[Jx][END]) / 4;
				tooltipSpatialData[toolID].filteredJerk[Jy][END] = (tooltipSpatialData[toolID].jerk[Jy][END - 3] + tooltipSpatialData[toolID].jerk[Jy][END - 2] + tooltipSpatialData[toolID].jerk[Jy][END - 1] + tooltipSpatialData[toolID].jerk[Jy][END]) / 4;
				tooltipSpatialData[toolID].filteredJerk[Jz][END] = (tooltipSpatialData[toolID].jerk[Jz][END - 3] + tooltipSpatialData[toolID].jerk[Jz][END - 2] + tooltipSpatialData[toolID].jerk[Jz][END - 1] + tooltipSpatialData[toolID].jerk[Jz][END]) / 4;

				// Find similarities with Matlab
				similaritiesAreValid[toolID] = FindSimilarities(tooltipSpatialData, toolID, classID[toolID], similarity[toolID]);
			}


			// reference path spatial data (toolID = 2)
			for (int toolID = 2; toolID < 2 + numberOfHapticDevices; toolID++)
			{
				for (int i = 1; i < SPATIAL_DATA_WINDOW_SIZE; i++)
				{
					tooltipSpatialData[toolID].time[i - 1] = tooltipSpatialData[toolID].time[i];

					tooltipSpatialData[toolID].raw[Px][i - 1] = tooltipSpatialData[toolID].raw[Px][i];
					tooltipSpatialData[toolID].raw[Py][i - 1] = tooltipSpatialData[toolID].raw[Py][i];
					tooltipSpatialData[toolID].raw[Pz][i - 1] = tooltipSpatialData[toolID].raw[Pz][i];

					tooltipSpatialData[toolID].filtered[Px][i - 1] = tooltipSpatialData[toolID].filtered[Px][i];
					tooltipSpatialData[toolID].filtered[Py][i - 1] = tooltipSpatialData[toolID].filtered[Py][i];
					tooltipSpatialData[toolID].filtered[Pz][i - 1] = tooltipSpatialData[toolID].filtered[Pz][i];

					tooltipSpatialData[toolID].raw[Vx][i - 1] = tooltipSpatialData[toolID].raw[Vx][i];
					tooltipSpatialData[toolID].raw[Vy][i - 1] = tooltipSpatialData[toolID].raw[Vy][i];
					tooltipSpatialData[toolID].raw[Vz][i - 1] = tooltipSpatialData[toolID].raw[Vz][i];

					tooltipSpatialData[toolID].filtered[Vx][i - 1] = tooltipSpatialData[toolID].filtered[Vx][i];
					tooltipSpatialData[toolID].filtered[Vy][i - 1] = tooltipSpatialData[toolID].filtered[Vy][i];
					tooltipSpatialData[toolID].filtered[Vz][i - 1] = tooltipSpatialData[toolID].filtered[Vz][i];

					tooltipSpatialData[toolID].raw[Ax][i - 1] = tooltipSpatialData[toolID].raw[Ax][i];
					tooltipSpatialData[toolID].raw[Ay][i - 1] = tooltipSpatialData[toolID].raw[Ay][i];
					tooltipSpatialData[toolID].raw[Az][i - 1] = tooltipSpatialData[toolID].raw[Az][i];

					tooltipSpatialData[toolID].filtered[Ax][i - 1] = tooltipSpatialData[toolID].filtered[Ax][i];
					tooltipSpatialData[toolID].filtered[Ay][i - 1] = tooltipSpatialData[toolID].filtered[Ay][i];
					tooltipSpatialData[toolID].filtered[Az][i - 1] = tooltipSpatialData[toolID].filtered[Az][i];

					tooltipSpatialData[toolID].jerk[Jx][i - 1] = tooltipSpatialData[toolID].jerk[Jx][i];
					tooltipSpatialData[toolID].jerk[Jy][i - 1] = tooltipSpatialData[toolID].jerk[Jy][i];
					tooltipSpatialData[toolID].jerk[Jz][i - 1] = tooltipSpatialData[toolID].jerk[Jz][i];

					tooltipSpatialData[toolID].filteredJerk[Jx][i - 1] = tooltipSpatialData[toolID].filteredJerk[Jx][i];
					tooltipSpatialData[toolID].filteredJerk[Jy][i - 1] = tooltipSpatialData[toolID].filteredJerk[Jy][i];
					tooltipSpatialData[toolID].filteredJerk[Jz][i - 1] = tooltipSpatialData[toolID].filteredJerk[Jz][i];
				}

				tooltipSpatialData[toolID].time[END] = currentTime;

				tooltipSpatialData[toolID].raw[Px][END] = settings.referencePoints[toolID - 2][theClosestPointIndices[toolID - 2]].x;
				tooltipSpatialData[toolID].raw[Py][END] = settings.referencePoints[toolID - 2][theClosestPointIndices[toolID - 2]].y;
				tooltipSpatialData[toolID].raw[Pz][END] = settings.referencePoints[toolID - 2][theClosestPointIndices[toolID - 2]].z;

				tooltipSpatialData[toolID].filtered[Px][END] = settings.referencePoints[toolID - 2][theClosestPointIndices[toolID - 2]].x;
				tooltipSpatialData[toolID].filtered[Py][END] = settings.referencePoints[toolID - 2][theClosestPointIndices[toolID - 2]].y;
				tooltipSpatialData[toolID].filtered[Pz][END] = settings.referencePoints[toolID - 2][theClosestPointIndices[toolID - 2]].z;

				tooltipSpatialData[toolID].raw[Vx][END] = (tooltipSpatialData[toolID].raw[Px][END] - tooltipSpatialData[toolID].raw[Px][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].raw[Vy][END] = (tooltipSpatialData[toolID].raw[Py][END] - tooltipSpatialData[toolID].raw[Py][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].raw[Vz][END] = (tooltipSpatialData[toolID].raw[Pz][END] - tooltipSpatialData[toolID].raw[Pz][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);

				tooltipSpatialData[toolID].filtered[Vx][END] = (tooltipSpatialData[toolID].raw[Vx][END - 3] + tooltipSpatialData[toolID].raw[Vx][END - 2] + tooltipSpatialData[toolID].raw[Vx][END - 1] + tooltipSpatialData[toolID].raw[Vx][END]) / 4;
				tooltipSpatialData[toolID].filtered[Vy][END] = (tooltipSpatialData[toolID].raw[Vy][END - 3] + tooltipSpatialData[toolID].raw[Vy][END - 2] + tooltipSpatialData[toolID].raw[Vy][END - 1] + tooltipSpatialData[toolID].raw[Vy][END]) / 4;
				tooltipSpatialData[toolID].filtered[Vz][END] = (tooltipSpatialData[toolID].raw[Vz][END - 3] + tooltipSpatialData[toolID].raw[Vz][END - 2] + tooltipSpatialData[toolID].raw[Vz][END - 1] + tooltipSpatialData[toolID].raw[Vz][END]) / 4;

				tooltipSpatialData[toolID].raw[Ax][END] = (tooltipSpatialData[toolID].raw[Vx][END] - tooltipSpatialData[toolID].raw[Vx][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].raw[Ay][END] = (tooltipSpatialData[toolID].raw[Vy][END] - tooltipSpatialData[toolID].raw[Vy][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].raw[Az][END] = (tooltipSpatialData[toolID].raw[Vz][END] - tooltipSpatialData[toolID].raw[Vz][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);

				tooltipSpatialData[toolID].filtered[Ax][END] = (tooltipSpatialData[toolID].raw[Ax][END - 3] + tooltipSpatialData[toolID].raw[Ax][END - 2] + tooltipSpatialData[toolID].raw[Ax][END - 1] + tooltipSpatialData[toolID].raw[Ax][END]) / 4;
				tooltipSpatialData[toolID].filtered[Ay][END] = (tooltipSpatialData[toolID].raw[Ay][END - 3] + tooltipSpatialData[toolID].raw[Ay][END - 2] + tooltipSpatialData[toolID].raw[Ay][END - 1] + tooltipSpatialData[toolID].raw[Ay][END]) / 4;
				tooltipSpatialData[toolID].filtered[Az][END] = (tooltipSpatialData[toolID].raw[Az][END - 3] + tooltipSpatialData[toolID].raw[Az][END - 2] + tooltipSpatialData[toolID].raw[Az][END - 1] + tooltipSpatialData[toolID].raw[Az][END]) / 4;

				tooltipSpatialData[toolID].jerk[Jx][END] = (tooltipSpatialData[toolID].raw[Ax][END] - tooltipSpatialData[toolID].raw[Ax][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].jerk[Jy][END] = (tooltipSpatialData[toolID].raw[Ay][END] - tooltipSpatialData[toolID].raw[Ay][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);
				tooltipSpatialData[toolID].jerk[Jz][END] = (tooltipSpatialData[toolID].raw[Az][END] - tooltipSpatialData[toolID].raw[Az][END - 1]) / (tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1]);

				tooltipSpatialData[toolID].filteredJerk[Jx][END] = (tooltipSpatialData[toolID].jerk[Jx][END - 3] + tooltipSpatialData[toolID].jerk[Jx][END - 2] + tooltipSpatialData[toolID].jerk[Jx][END - 1] + tooltipSpatialData[toolID].jerk[Jx][END]) / 4;
				tooltipSpatialData[toolID].filteredJerk[Jy][END] = (tooltipSpatialData[toolID].jerk[Jy][END - 3] + tooltipSpatialData[toolID].jerk[Jy][END - 2] + tooltipSpatialData[toolID].jerk[Jy][END - 1] + tooltipSpatialData[toolID].jerk[Jy][END]) / 4;
				tooltipSpatialData[toolID].filteredJerk[Jz][END] = (tooltipSpatialData[toolID].jerk[Jz][END - 3] + tooltipSpatialData[toolID].jerk[Jz][END - 2] + tooltipSpatialData[toolID].jerk[Jz][END - 1] + tooltipSpatialData[toolID].jerk[Jz][END]) / 4;

				// Find similarities with Matlab
				similaritiesAreValid[toolID] = FindSimilarities(tooltipSpatialData, toolID, classID[toolID], similarity[toolID]);
			}


			if (!taskHasStarted && (deviceInformation[0].hasTouchedTheFirstPoint || deviceInformation[1].hasTouchedTheFirstPoint))
			{
				taskTimer.Start();
				taskHasStarted = true;
				for (int toolID = 0; toolID < numberOfHapticDevices; toolID++)
				{
					taskPathLength[toolID] = 0.0;
					taskMotionSmoothness[toolID] = 0.0;
					taskEconomyOfMotion[toolID] = 0.0;
				}

				// open a file for storing Kp values.
				string path = settings.outputDirectory;
				if (path.back() != '/')
					path.append("/");

				string hapticFile = path + to_string(subjectNumber) + "/" + workingModeLabels[workingMode] + "/" + currentDateTime() + "_data_Kp.csv";
				string makingDirectoryCommand = RESOURCE_PATH("data\\" + to_string(subjectNumber) + "\\" + workingModeLabels[workingMode]);
				makingDirectoryCommand = "mkdir \"" + makingDirectoryCommand + "\"";
				system(makingDirectoryCommand.c_str());

				kpValuesOutputFile.open(hapticFile);
				if (!kpValuesOutputFile.is_open())
					perror("error");
			}
			if (taskHasStarted && (deviceInformation[0].hasReachedTheLastPoint || deviceInformation[1].hasReachedTheLastPoint))
			{
				taskHasStarted = false;
				// close the file for storing Kp values.
				kpValuesOutputFile.close();
			}

			if (taskHasStarted && kpValuesOutputFile.is_open())
			{
				double taskTime = taskTimer.GetSeconds();
				kpValuesOutputFile << taskTime << "\t";
				for (int toolID = 0; toolID < numberOfHapticDevices; toolID++)
				{
					double dt = tooltipSpatialData[toolID].time[END] - tooltipSpatialData[toolID].time[END - 1];

					double dJx = (tooltipSpatialData[toolID].filteredJerk[Jx][END] - tooltipSpatialData[toolID].filteredJerk[Jx][END - 1]) / dt;
					double dJy = (tooltipSpatialData[toolID].filteredJerk[Jy][END] - tooltipSpatialData[toolID].filteredJerk[Jy][END - 1]) / dt;
					double dJz = (tooltipSpatialData[toolID].filteredJerk[Jz][END] - tooltipSpatialData[toolID].filteredJerk[Jz][END - 1]) / dt;

					taskMotionSmoothness[toolID] += sqrt((dJx * dJx + dJy * dJy + dJz * dJz) / 2);
					taskPathLength[toolID] += tooltipPositions[toolID].distance(previousTooltipPositions[toolID]);
					taskEconomyOfMotion[toolID] = taskPathLength[toolID] / taskTime;
					kpValuesOutputFile << tooltipSpatialData[toolID].filtered[Px][END] << "\t" << tooltipSpatialData[toolID].filtered[Py][END] << "\t" << tooltipSpatialData[toolID].filtered[Pz][END] << "\t";
					kpValuesOutputFile << Kp[toolID] << "\t" << taskPathLength[toolID] << "\t" << taskMotionSmoothness[toolID] << "\t" << taskEconomyOfMotion[toolID] << "\t";
				}
				kpValuesOutputFile << std::endl;
			}

			for (int toolID = 0; toolID < numberOfHapticDevices; toolID++)
			{
				previousTooltipPositions[toolID].copyfrom(tooltipPositions[toolID]);
			}
			previousTime = currentTime;
		}
		Sleep(1);
	}
}

void interactiveCommandCenterThreadHandler(void)
{
	DWORD        mode;
	HANDLE       hstdin;
	INPUT_RECORD inrec;
	DWORD        count;
	char         key = '\0';

	cout << endl;
	cout << "\t s => Subject number" << endl;
	cout << "\t w => Working mode" << endl;
	cout << "\t x => Exit" << endl;

	/* Set the console mode to no-echo, raw input, */
	/* and no window or mouse events.              */
	hstdin = GetStdHandle(STD_INPUT_HANDLE);

	while (simulationRunning)
	{
		if (hstdin == INVALID_HANDLE_VALUE
			|| !GetConsoleMode(hstdin, &mode)
			|| !SetConsoleMode(hstdin, 0))
			return;
		FlushConsoleInputBuffer(hstdin);

		/* Wait for and get a single key PRESS */
		do ReadConsoleInput(hstdin, &inrec, 1, &count);
		while ((inrec.EventType != KEY_EVENT) || !inrec.Event.KeyEvent.bKeyDown);

		/* Remember which key the user pressed */
		key = inrec.Event.KeyEvent.uChar.AsciiChar;

		keySelect(key, 0, 0);

		if (hstdin == INVALID_HANDLE_VALUE
			|| !SetConsoleMode(hstdin, mode))
			return;
		FlushConsoleInputBuffer(hstdin);

		switch (key)
		{
		case 's':
			cout << "Enter the subject number: ";
			cin >> subjectNumber;
			cout << "\t Subject " << subjectNumber << endl;
			break;
		case 'w':
			cout << "Select the operating mode: " << endl;
			cout << "\t 1 => DHMM1" << endl;
			cout << "\t 2 => MHMM1" << endl;
			cout << "\t 3 => MHMM2" << endl;
			cout << "\t 4 => Maximum force" << endl;
			cout << "\t 5 => Medium force" << endl;
			cout << "\t 6 => Minimum force" << endl;
			cout << "\t 7 => HCRF" << endl;
			cout << endl;
			cout << "Mode: ";
			cin >> workingMode;
			switch (workingMode)
			{
			case 1:
				SetMatlabEngine(MatlabEngine::DHMM);
				controlledMode = ControlledModes::DHMM1;
				cout << "Controlled mode: DHMM1" << endl;
				break;
			case 2:
				SetMatlabEngine(MatlabEngine::MHMM);
				controlledMode = ControlledModes::MHMM1;
				cout << "Controlled mode: MHMM1" << endl;
				break;
			case 3:
				SetMatlabEngine(MatlabEngine::MHMM);
				//				SetMatlabEngine(MatlabEngine::DHMM);
				controlledMode = ControlledModes::MHMM2;
				cout << "Controlled mode: MHMM2" << endl;
				break;
			case 4:
				controlledMode = ControlledModes::Maximum;
				cout << "Controlled mode: Maximum" << endl;
				break;
			case 5:
				controlledMode = ControlledModes::Medium;
				cout << "Controlled mode: Medium" << endl;
				break;
			case 6:
				controlledMode = ControlledModes::Minimum;
				cout << "Controlled mode: Minimum" << endl;
				break;
			case 7:
				SetMatlabEngine(MatlabEngine::HCRF);
				controlledMode = ControlledModes::HCRF1;
				cout << "Controlled mode: HCRF" << endl;
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}
}
