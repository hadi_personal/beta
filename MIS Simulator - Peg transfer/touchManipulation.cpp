#include "touchManipulation.h"
#include "common.h"


cColorf red(1, 0, 0);
cColorf green(0, 1, 0);
cColorf blue(0, 0, 1);
const double liftLevel = 0.2;
const double toolProximity = 1;
DeviceInformation deviceInformation[MAX_DEVICES];
int lastPointCounter;
int axisAttach[MAX_OBJECTS];
int objectGraspingDevice[MAX_OBJECTS];
double rootTouchTolerance = 0.2;
cShapeSphere* followPoint[MAX_POINT];
int designatedObject = 0;
double objectAxisProximityXYTolerance = 0.3;
double objectAxisProximityZTolerance = 0.3;
int designatedSide = 1;//right
bool taskCompleted = false;

cMaterial selectObjectMat;
cMaterial objectMat;


void init()
{
#ifdef MOTION_CAPTURE
	ndiMotion = (motion_manager *)malloc(sizeof(motion_manager));
#endif

	lightVector.set(1, 1, 1);
	extraForce.set(0, 0, 0);
	displayW = 0;
	displayH = 0;
	zoomConstant = 0.2;
	simulationRunning = false;
	simulationFinished = false;
	howManyToShow = 5;
	extend = 3;//extend of the pegs

	lastPointCounter = 0;
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		objectGraspingDevice[i] = -1; //none touch
	}

	for (int i = 0; i < MAX_DEVICES; i++)
	{
		deviceInformation[i].graspedObjectIndex = -1; //control none;
		deviceInformation[i].hasTouchedTheFirstPoint = false;
		axisAttach[i] = -1; //no attachment
	}

}

bool lookingForObject(cGenericObject *object, int& objectNum)
{
	if (object == NULL)
		return false;

	cGenericType* externalParent = object->getExternalParent();
	cODEGenericBody *parentObject = dynamic_cast<cODEGenericBody*>(externalParent);

	if (parentObject == NULL)
		return false;
	if ((int)parentObject->m_userData == -2)
		return false; // make sure it is not pegs

	objectNum = (int)parentObject->m_userData;

	return true;
}

void SetupObjects(cVector3d *position)
{
	cColorf objectNormalColor(0.7, 0.1, 0.2);

	selectObjectMat.m_ambient = green;
	selectObjectMat.m_diffuse.set(0.8, 0.15, 0.2);
	selectObjectMat.m_specular.set(1.0, 0.2, 0.2);
	selectObjectMat.setStiffness(0.5 *  stiffnessMax[0]);
	selectObjectMat.setDynamicFriction(0.4);
	selectObjectMat.setStaticFriction(0.9);

	objectMat.m_ambient = objectNormalColor;
	objectMat.m_diffuse.set(0.8, 0.15, 0.2);
	objectMat.m_specular.set(1.0, 0.2, 0.2);
	objectMat.setStiffness(0.5 *  stiffnessMax[0]);
	objectMat.setDynamicFriction(0.4);
	objectMat.setStaticFriction(0.9);

	cMatrix3d rot;
	rot.identity();
	for (int i = 0; i < numberOfObjects; i++)
	{
		cMesh * objectM = new cMesh(world);
		objects[i] = new cODEGenericBody(ODEWorld);
		objectM->loadFromFile(RESOURCE_PATH("resources/peg_transfer/object.3ds"));
		cVector3d scale(0.3, 0.3, 0.15);
		objectM->scale(scale);
		objectM->createAABBCollisionDetector(proxyRadius, true, false);
		objectM->setMaterial(objectMat, true);

		objects[i]->setImageModel(objectM);
		objects[i]->createDynamicMesh();
		objects[i]->setMass(0.06);
		objects[i]->setUseCulling(false, true);

		objects[i]->setPosition(position[i]);
		objects[i]->setRotation(rot);
		/*char aa[10];
		sprintf(aa, "objects%d", i);
		objectM->setUserData(aa, true);*/
		objects[i]->setUserData((void*)i, false);
		objects[i]->setObjectType(CHAI_OBJECT_TYPE_IGNORE_COLLISION_WHEN_PICKED_UP);
		objects[i]->m_imageModel->setHapticEnabled(true, true);
	}
}

void SetupPegs(cVector3d position[])
{
	cMaterial pegMat;

	pegMat.m_ambient.set(0.2, 0.2, 0.2);
	pegMat.m_diffuse.set(0.8, 0.8, 0.8);
	pegMat.m_specular.set(1.0, 1.0, 1.0);
	pegMat.setStiffness(0.5 * stiffnessMax[0]);
	pegMat.setDynamicFriction(0.4);
	pegMat.setStaticFriction(0.9);
	for (int i = 0; i < numberOfPegs; i++)
	{
		if (settings.axisPartition[i] == -1)//left
			pegMat.m_ambient = red;
		else pegMat.m_ambient = blue;
		pegs[i] = new cODEGenericBody(ODEWorld);
		cMesh * axisM = new cMesh(world);
		axisM->loadFromFile(RESOURCE_PATH("resources/peg_transfer/peg.3ds"));
		axisM->scale(0.0015);
		axisM->createAABBCollisionDetector(proxyRadius, true, false);
		axisM->setMaterial(pegMat, true);
		pegs[i]->setImageModel(axisM);
		pegs[i]->createDynamicCapsule(0.050, 0.47, true);
		pegs[i]->setUseCulling(false, true);
		position[i].z = -0.7;
		pegs[i]->setPosition(position[i]);
		pegs[i]->m_imageModel->setHapticEnabled(true, true);
		pegs[i]->setUserData((void *)-2, false);
	}
}

bool testObjectAxisAttach(int object_index, int peg_index)
{
	cVector3d point0 = objects[object_index]->getGlobalPos();
	cVector3d point1 = pegs[peg_index]->getGlobalPos();
	double z0 = point0.z;
	double z1 = point1.z;
	point0.z = 0; point1.z = 0;
	return (point0.distance(point1) < objectAxisProximityXYTolerance &&
		abs(z0 - z1)<objectAxisProximityZTolerance);
}

//void saveLastHapobjectInfo(structLastPosRotInfo &hapticInfo,
//	cGeneric3dofPointer * tool, cODEGenericBody * ODEobject)
//
//{
//	//always save the last configuration
//	hapticInfo.lastPosDevice = tool->getProxyGlobalPos();
//	hapticInfo.lastRotDevice = tool->getProxyGlobalRot();
//	hapticInfo.lastPosObject = ODEobject->getPos();
//	hapticInfo.lastRotObject = ODEobject->getRot();
//	hapticInfo.lastDeviceObjectPos =
//		cTrans(hapticInfo.lastRotDevice) *
//		((hapticInfo.lastPosObject - hapticInfo.lastPosDevice)
//		+ 0.01*cNormalize(hapticInfo.lastPosObject -
//		hapticInfo.lastPosDevice));
//	hapticInfo.lastDeviceObjectRot =
//		cMul(cTrans(hapticInfo.lastRotDevice),
//		hapticInfo.lastRotObject);
//}
