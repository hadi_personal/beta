#include "redraw.h"
#include "common.h"

#define USE_SPHERE

void mainDrawPath()
{
	glDisable(GL_LIGHTING);

	rootCoord = pegs[0]->getGlobalPos() + cVector3d(0, 0, 0.2);

	glPushMatrix();

#ifdef DRAW_COORDINATE_SYSTEM
	glLineWidth(2.5);
	//glTranslated(rootCoord.x, rootCoord.y, rootCoord.z);
	glBegin(GL_LINES);
	glColor3d(1, 0, 0);
	glVertex3d(0, 0, 0);
	glVertex3d(extend, 0, 0);
	glColor3d(0, 1, 0);
	glVertex3d(0, 0, 0);
	glVertex3d(0, extend, 0);
	glColor3d(0, 0, 1);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 0, extend);
	glEnd();
#endif // DRAW_COORDINATE_SYSTEM

	glPointSize(5.0);
	glBegin(GL_POINTS);
	glColor3f(1.0, 1.0, 0.0);

	if (IsPathVisible())
	{
		for (int deviceIndex = 0; deviceIndex < numberOfHapticDevices; deviceIndex++)
		{
			int start = cMax(theClosestPointIndices[deviceIndex], 0);
			int end = cMin(start + pointShownAhead, settings.numberOfReferencePoints[deviceIndex]);
			for (int i = start; i < end; i++)
			{

#ifdef USE_SPHERE
				deviceInformation[deviceIndex].referencePointSpheres[i - start]->setShowEnabled(true);
				deviceInformation[deviceIndex].referencePointSpheres[i - start]->setPos(settings.referencePoints[deviceIndex][i]);
				deviceInformation[deviceIndex].referencePointSpheres[i - start]->m_material.m_diffuse = deviceIndex == 0 ? red : green;
				if (settings.switchClick[deviceIndex][i])
					deviceInformation[deviceIndex].referencePointSpheres[i - start]->m_material.m_diffuse = blue;
#else
				if (deviceIndex == 0) red.render();
				else green.render();
				if (theClosestPointIndices[deviceIndex] == i)
					glColor3d(0, 1, 0);

				if (settings.switchClick[deviceIndex][i])
					blue.render();
				glVertex3f(settings.referencePoints[deviceIndex][i].x, settings.referencePoints[deviceIndex][i].y,
					settings.referencePoints[deviceIndex][i].z);
				/*glVertex3f(settings.point[i].x, settings.point[i].y,
				settings.point[i].z);*/
#endif


			}
		}
		glColor3f(1, 1, 1);

		for (int i = 0; i < numLastPoint; i++)
			glVertex3f(lastPoint[i].x, lastPoint[i].y, lastPoint[i].z);
	}
	else
	{
		for (int deviceIndex = 0; deviceIndex < numberOfHapticDevices; deviceIndex++)
		{
			for (int i = 0; i < pointShownAhead; i++)
			{
#ifdef USE_SPHERE
				deviceInformation[deviceIndex].referencePointSpheres[i]->setShowEnabled(false);
#endif
			}
		}
		glColor3f(1, 1, 1);

		for (int i = 0; i < numLastPoint; i++)
			glVertex3f(lastPoint[i].x, lastPoint[i].y, lastPoint[i].z);
	}
	glEnd();

	glPopMatrix();
	glEnable(GL_LIGHTING);
}


void postDraw()
{
	glutSwapBuffers();
	GLenum err;
	err = glGetError();
	if (err != GL_NO_ERROR) printf("Error:  %s\n", gluErrorString(err));
	if (simulationRunning)
	{
		glutPostRedisplay();
	}
}

void updateGraphicsTopView(void)
{
	rootLabels->setShowEnabled(false, true);
	cameraTopView->renderView(displayW, displayH);
	rootLabels->setShowEnabled(true, true);

	mainDrawPath();
	postDraw();
}

void updateGraphicsSideView(void)
{
	rootLabels->setShowEnabled(false, true);
	cameraSideView->renderView(displayW, displayH);
	rootLabels->setShowEnabled(true, true);

	mainDrawPath();
	postDraw();
}

string classLabels[] = { "Left", "Right", "Down", "Up" };
string controlledModeLabels[] = { "DHMM1", "MHMM1", "MHMM2", "Maximum", "Medium", "Minimum" };

void labelling()
{
	// render world
	string strLabel10;
	strLabel10 = "Operating mode: ";
	strLabel10 += (settings.operaringMode == OperatingModes::CONTROLLED) ? "CONTROLLED" : "FREE";
	//strLabel10 += ", Kp Mode: " + controlledModeLabels[controlledMode];
	//for (int i = 0; i < numberOfHapticDevices; i++)
	//{
	//	// read position of device an convert into millimeters
	//	cVector3d pos = generic3dofPointers[i]->getProxyGlobalPos();
	//	//pos.mul(1000);
	//	strLabel10 += "#";
	//	cStr(strLabel10, i);
	//	strLabel10 += " x: ";
	//	cStr(strLabel10, pos.x, settings.precisionOfFloatingPoints);
	//	strLabel10 += " y: ";
	//	cStr(strLabel10, pos.y, settings.precisionOfFloatingPoints);
	//	strLabel10 += " z: ";
	//	cStr(strLabel10, pos.z, settings.precisionOfFloatingPoints);
	//}
	labels[0]->m_string = strLabel10;
	//// Added by Hadi
	//strLabel += "\n(R)";// " + generic3dofPointers[0]->getProxyGlobalPos().str(settings.precisionOfFloatingPoints) +"\t";
	//cVector3d jointAngles;
	//cVector3d gimbalAngles;
	//genericHapticDevices[0]->getJointAngles(jointAngles, gimbalAngles);
	//strLabel = strLabel + "  1: ";
	//cStr(strLabel, jointAngles[0], 2);
	//strLabel = strLabel + "  2: ";
	//cStr(strLabel, jointAngles[1], 2);
	//strLabel = strLabel + "  3: ";
	//cStr(strLabel, jointAngles[2], 2);
	//strLabel = strLabel + "  4: ";
	//cStr(strLabel, gimbalAngles[0], 2);
	//strLabel = strLabel + "  5: ";
	//cStr(strLabel, gimbalAngles[1], 2);
	//strLabel = strLabel + "  6: ";
	//cStr(strLabel, gimbalAngles[2], 2);

	//// Added by Hadi
	//strLabel += "\n(L)";// " + generic3dofPointers[1]->getProxyGlobalPos().str(settings.precisionOfFloatingPoints);
	//genericHapticDevices[1]->getJointAngles(jointAngles, gimbalAngles);
	//strLabel = strLabel + "  1: ";
	//cStr(strLabel, jointAngles[0], 2);
	//strLabel = strLabel + "  2: ";
	//cStr(strLabel, jointAngles[1], 2);
	//strLabel = strLabel + "  3: ";
	//cStr(strLabel, jointAngles[2], 2);
	//strLabel = strLabel + "  4: ";
	//cStr(strLabel, gimbalAngles[0], 2);
	//strLabel = strLabel + "  5: ";
	//cStr(strLabel, gimbalAngles[1], 2);
	//strLabel = strLabel + "  6: ";
	//cStr(strLabel, gimbalAngles[2], 2);

	string strLabel1;
	for (int i = 0; i < numberOfObjects; i++)
	{
		strLabel1 += "Object ";
		cStr(strLabel1, i);
		strLabel1 += ": ";
		cStr(strLabel1, objects[i]->dynamicEnable);

	}
	labels[1]->m_string = strLabel1;

	string strLabel2;
	for (int i = 0; i < numberOfHapticDevices; i++)
	{
		strLabel2 += "Hap ";
		cStr(strLabel2, i);
		strLabel2 += ": ";
		cStr(strLabel2, deviceInformation[i].graspedObjectIndex);

	}
	labels[2]->m_string = strLabel2;

	string strLabel3 = "Axis Attach: ";
	for (int i = 0; i < numberOfObjects; i++)
	{
		cStr(strLabel3, i);
		strLabel3 += ": ";
		cStr(strLabel3, axisAttach[i]); strLabel3 += " ";
	}
	labels[3]->m_string = strLabel3;

	string strLabel4 = "Clicking button: ";
	cStr(strLabel4, isPedalPressed);
	labels[4]->m_string = strLabel4;

	if (similaritiesAreValid[0])
	{
		string strLabel5 = "HMM=> Tool[0]: ";
		strLabel5 += classLabels[classID[0]];
		strLabel5.append(", ");
		cStr(strLabel5, similarity[0]);
		labels[5]->m_string = strLabel5;
	}

	if (similaritiesAreValid[1])
	{
		string strLabel6 = "HMM=> Tool[1]: ";
		strLabel6 += classLabels[classID[1]];
		strLabel6.append(", ");
		cStr(strLabel6, similarity[1]);
		labels[6]->m_string = strLabel6;
	}

	string strLabel7 = "Path Length: ";
	cStr(strLabel7, taskPathLength[0]);
	labels[7]->m_string = strLabel7;

	string strLabel8 = "Motion Smoothness: ";
	cStr(strLabel8, taskMotionSmoothness[0]);
	labels[8]->m_string = strLabel8;

	string strLabel9 = "Economy of Motion: ";
	cStr(strLabel9, taskEconomyOfMotion[0]);
	labels[9]->m_string = strLabel9;
}

void updateGraphics(void)
{
	labelling();
	camera->renderView(displayW, displayH);
	if (settings.operaringMode != OperatingModes::FREE)
		mainDrawPath();
	postDraw();
}
