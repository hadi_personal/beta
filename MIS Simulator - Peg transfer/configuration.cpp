#include "configuration.h"
#include "virtualEnvironment.h"
#include "common.h"

int ConfigurationLoad(const char *configFilename, motion_manager *ndiMotion)
{
#ifdef TIXML_USE_STL
	TiXmlDocument configDocument(configFilename);

	if (!configDocument.LoadFile())
	{
		printf("Could not load 'configuration.xml'. Error='%s'. Exiting.\n", configDocument.ErrorDesc());
#ifdef MOTION_CAPTURE
		ndiMotion->MotionCapture_ActiveOutputs = ndiMotion->MotionCapture_None;

		ndiMotion->active3DMarkersCount = 1;
		ndiMotion->active3DMarkerIDs = new int[ndiMotion->active3DMarkersCount];
		ndiMotion->active3DMarkerIDs[0] = 0;
		ndiMotion->active3DMarkerNames = new string[ndiMotion->active3DMarkersCount];
		ndiMotion->active3DMarkerNames[0] = "Marker";
#endif // MOTION_CAPTURE
		return -1;
	}

	TiXmlHandle configXMLHandle(&configDocument);
	TiXmlElement* configXMLElement;
	TiXmlHandle configRootHandle(0);

	// block: root
	{
		configXMLElement = configXMLHandle.FirstChildElement().Element();
		// should always have a valid root but handle gracefully if it does
		if (!configXMLElement) return -1;
		//m_name = pElem->Value();

		// save this for later
		configRootHandle = TiXmlHandle(configXMLElement);
	}

	// block: Window attributes
	{
		TiXmlElement* configWindowNode = configRootHandle.FirstChild("Windows").FirstChild().Element();
		for (configWindowNode; configWindowNode; configWindowNode = configWindowNode->NextSiblingElement())
		{
			const char *windowName = configWindowNode->Attribute("name");
			if (strcmp(windowName, "mainWindow") == 0)
			{
				configWindowNode->QueryIntAttribute("x", &settings.mainWindowX);
				configWindowNode->QueryIntAttribute("y", &settings.mainWindowY);
				configWindowNode->QueryIntAttribute("w", &settings.mainWindowWidth);
				configWindowNode->QueryIntAttribute("h", &settings.mainWindowHeight);
			}
			else if (strcmp(windowName, "mainWindow_TopView") == 0)
			{
				configWindowNode->QueryIntAttribute("x", &settings.mainWindowTopViewX);
				configWindowNode->QueryIntAttribute("y", &settings.mainWindowTopViewY);
				configWindowNode->QueryIntAttribute("w", &settings.mainWindowTopViewWidth);
				configWindowNode->QueryIntAttribute("h", &settings.mainWindowTopViewHeight);
			}
			else if (strcmp(windowName, "mainWindow_SideView") == 0)
			{
				configWindowNode->QueryIntAttribute("x", &settings.mainWindowSideViewX);
				configWindowNode->QueryIntAttribute("y", &settings.mainWindowSideViewY);
				configWindowNode->QueryIntAttribute("w", &settings.mainWindowSideViewWidth);
				configWindowNode->QueryIntAttribute("h", &settings.mainWindowSideViewHeight);
			}
#ifdef MOTION_CAPTURE
			else if (strcmp(windowName, "motionCaptureWindow") == 0)
			{
				/*configWindowNode->QueryIntAttribute("x", &motionCaptureWindowPosX);
				configWindowNode->QueryIntAttribute("y", &motionCaptureWindowPosY);
				configWindowNode->QueryIntAttribute("w", &motionCaptureWindowWidth);
				configWindowNode->QueryIntAttribute("h", &motionCaptureWindowHeight);*/
			}
#endif // MOTION_CAPTURE
		}
	}

#ifdef MOTION_CAPTURE
	// block: Points
	{
		configXMLElement = configRootHandle.FirstChild("Points").Element();
		if (configXMLElement)
		{
			ndiMotion->active3DMarkersCount = atoi(configXMLElement->Attribute("count"));
			ndiMotion->active3DMarkerIDs = new int[ndiMotion->active3DMarkersCount];
			ndiMotion->active3DMarkerNames = new string[ndiMotion->active3DMarkersCount];

			int i = 0;
			for (configXMLElement = configRootHandle.FirstChild("Points").
				FirstChild().Element(); configXMLElement; configXMLElement = configXMLElement->NextSiblingElement())
			{
				const char *pointName = configXMLElement->Attribute("name");
				ndiMotion->active3DMarkerNames[i].assign(pointName);
				configXMLElement->QueryIntAttribute("id", &ndiMotion->active3DMarkerIDs[i]);
				ndiMotion->active3DMarkerIDs[i]--;
				i++;
			}
			//pElem->QueryDoubleAttribute("timeout", &m_connection.timeout);
		}
	}
#endif // MOTION_CAPTURE

	// block: Peg Board Configuration
	{
		configXMLElement = configRootHandle.FirstChild("BoardConfigurations").Element();
		if (configXMLElement)
		{
			int activePegBoardConfiguration = atoi(configXMLElement->Attribute("active_id"));
			int id;

			for (TiXmlElement* configurationRoot = configXMLElement->FirstChild("Configuration")->ToElement(); configurationRoot; configurationRoot = configurationRoot->NextSiblingElement())
			{
				id = atoi(configurationRoot->Attribute("id"));
				if (id == activePegBoardConfiguration)
				{

					//configuration->QueryBoolAttribute("ODE_enabled", &isODEEnabled);
					settings.numberOfObjects = atoi(configurationRoot->Attribute("object_count"));
					settings.numberOfPegs = atoi(configurationRoot->Attribute("peg_count"));
					// create a virtual mesh  that will be used for the geometry
					// representation of the dynamic body
					settings.objectPosition = (cVector3d *)malloc(sizeof(cVector3d)* settings.numberOfObjects);
					settings.pegPosition = (cVector3d *)malloc(sizeof(cVector3d)* settings.numberOfPegs);
					settings.axisPartition = (int *)malloc(sizeof(int)* settings.numberOfPegs);

					settings.objectRotation = (cMatrix3d *)malloc(sizeof(cMatrix3d)* settings.numberOfObjects);

					int object_index = 0, peg_index = 0;
					for (TiXmlElement* configurationItem = configurationRoot->FirstChild("Item")->ToElement(); configurationItem; configurationItem = configurationItem->NextSiblingElement())
					{
						const char *itemType = configurationItem->Attribute("type");
						if (strcmp(itemType, "Object") == 0 && object_index < settings.numberOfObjects)
						{
							double x, y, z;
							int rotaion_vector_x, rotaion_vector_y, rotaion_vector_z, rotation_degree;

							// position and oriente ODEPeg
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							configurationItem->QueryIntAttribute("rotaion_vector_x", &rotaion_vector_x);
							configurationItem->QueryIntAttribute("rotaion_vector_y", &rotaion_vector_y);
							configurationItem->QueryIntAttribute("rotaion_vector_z", &rotaion_vector_z);
							configurationItem->QueryIntAttribute("rotation_degree", &rotation_degree);

							settings.objectPosition[object_index] = cVector3d(x, y, z);
							settings.objectRotation[object_index].identity();
							settings.objectRotation[object_index].rotate(cVector3d(rotaion_vector_x, rotaion_vector_y, rotaion_vector_z), cDegToRad(rotation_degree));
							object_index++;
						}
						else if (strcmp(itemType, "Peg") == 0 && peg_index < settings.numberOfPegs)
						{
							double x, y, z;
							int rotaion_vector_x, rotaion_vector_y, rotaion_vector_z, rotation_degree;

							// position ODEAxis
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							settings.pegPosition[peg_index] = cVector3d(x, y, z);
							const char *partition = configurationItem->Attribute("partition");
							if (strcmp(partition, "left") == 0)
								settings.axisPartition[peg_index] = -1;
							else settings.axisPartition[peg_index] = 1;
							peg_index++;
						}
					}
				}
			}
		}

	}

	{ // block: Camera
		configXMLElement = configRootHandle.FirstChild("Cameras").Element();
		if (configXMLElement)
		{
			for (TiXmlElement* configurationRoot = configXMLElement->FirstChild("Camera")->ToElement(); configurationRoot; configurationRoot = configurationRoot->NextSiblingElement())
			{
				const char *cameraName = configurationRoot->Attribute("name");

				if (strcmp(cameraName, "mainWindow") == 0)
				{
					for (TiXmlElement* configurationItem = configurationRoot->FirstChild("Config")->ToElement(); configurationItem; configurationItem = configurationItem->NextSiblingElement())
					{
						const char *cameraPropertyName = configurationItem->Attribute("name");
						if (strcmp(cameraPropertyName, "cameraLookAt") == 0)
						{
							double x, y, z;
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							settings.cameraLookAt = cVector3d(x, y, z);
						}
						else if (strcmp(cameraPropertyName, "cameraPostion") == 0)
						{
							double x, y, z;
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							settings.cameraPostion = cVector3d(x, y, z);
						}
						else if (strcmp(cameraPropertyName, "cameraUpVector") == 0)
						{
							double x, y, z;
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							settings.cameraUpVector = cVector3d(x, y, z);
						}
					}
				}
				else if (strcmp(cameraName, "mainWindowTopView") == 0)
				{
					for (TiXmlElement* configurationItem = configurationRoot->FirstChild("Config")->ToElement(); configurationItem; configurationItem = configurationItem->NextSiblingElement())
					{
						const char *cameraPropertyName = configurationItem->Attribute("name");
						if (strcmp(cameraPropertyName, "cameraLookAt") == 0)
						{
							double x, y, z;
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							settings.cameraTopViewLookAt = cVector3d(x, y, z);
						}
						else if (strcmp(cameraPropertyName, "cameraPostion") == 0)
						{
							double x, y, z;
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							settings.cameraTopViewPostion = cVector3d(x, y, z);
						}
						else if (strcmp(cameraPropertyName, "cameraUpVector") == 0)
						{
							double x, y, z;
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							settings.cameraTopViewUpVector = cVector3d(x, y, z);
						}
					}
				}
				else if (strcmp(cameraName, "mainWindowSideView") == 0)
				{
					for (TiXmlElement* configurationItem = configurationRoot->FirstChild("Config")->ToElement(); configurationItem; configurationItem = configurationItem->NextSiblingElement())
					{
						const char *cameraPropertyName = configurationItem->Attribute("name");
						if (strcmp(cameraPropertyName, "cameraLookAt") == 0)
						{
							double x, y, z;
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							settings.cameraSideViewLookAt = cVector3d(x, y, z);
						}
						else if (strcmp(cameraPropertyName, "cameraPostion") == 0)
						{
							double x, y, z;
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							settings.cameraSideViewPostion = cVector3d(x, y, z);
						}
						else if (strcmp(cameraPropertyName, "cameraUpVector") == 0)
						{
							double x, y, z;
							configurationItem->QueryDoubleAttribute("x", &x);
							configurationItem->QueryDoubleAttribute("y", &y);
							configurationItem->QueryDoubleAttribute("z", &z);
							settings.cameraSideViewUpVector = cVector3d(x, y, z);
						}
					}
				}
			}
		}
	}

#ifdef MOTION_CAPTURE
	// block: Motion Capture
	{
		TiXmlElement* configOutputNode = configRootHandle.FirstChild("MotionCapture").FirstChild().Element();
		for (configOutputNode; configOutputNode; configOutputNode = configOutputNode->NextSiblingElement())
		{
			const char *outputType = configOutputNode->Attribute("type");
			bool isActive;
			configOutputNode->QueryBoolAttribute("enable", &isActive);
			if (strcmp(outputType, "3D Vector") == 0)
			{
				ndiMotion->MotionCapture_ActiveOutputs |= isActive ?
					ndiMotion->MotionCapture_3D_Vector : ndiMotion->MotionCapture_None;
			}
			else if (strcmp(outputType, "6D Vector") == 0)
			{
				ndiMotion->MotionCapture_ActiveOutputs |= isActive ? ndiMotion->MotionCapture_6D_Vector :
					ndiMotion->MotionCapture_None;
			}
			else if (strcmp(outputType, "Analog") == 0)
			{
				ndiMotion->MotionCapture_ActiveOutputs |= isActive ? ndiMotion->MotionCapture_Analog :
					ndiMotion->MotionCapture_None;
			}
		}
	}
#endif // MOTION_CAPTURE

#ifdef SERIAL_INTERFACE
	// block: Serial
	{
		configXMLElement = configRootHandle.FirstChild("Serial").Element();
		if (configXMLElement)
		{
			settings.serialPortName.assign(configXMLElement->Attribute("port"));
		}
	}
#endif // SERIAL_INTERFACE

#ifdef DATA_STORAGE
	// block: Serial
	{
		configXMLElement = configRootHandle.FirstChild("DataStorage").Element();
		if (configXMLElement)
		{
			float aTime;
			configXMLElement->QueryFloatAttribute("hapticSamplingTime_ms", &aTime);
			settings.hapticSamplingTime = aTime / 1000.0;
#ifdef MOTION_CAPTURE
			configXMLElement->QueryFloatAttribute("motionCaptureSamplingTime_ms", &aTime);
			ndiMotion->motionCaptureSamplingTime = aTime / 1000.0;
#endif
			settings.outputDirectory.assign(configXMLElement->Attribute("outputDirectory"));
		}
	}
#endif // DATA_STORAGE

	// block: Reference Model
	{
		configXMLElement = configRootHandle.FirstChild("Reference").Element();
		if (configXMLElement)
		{
			settings.referenceModelFilename.assign(configXMLElement->Attribute("modelFilename"));
			settings.minimumDistanceBetweenReferencePoints = atof(configXMLElement->Attribute("minimumDistanceBetweenReferencePoints"));
			settings.precisionOfFloatingPoints = atoi(configXMLElement->Attribute("precisionOfFloatingPoints"));
			bool isActive;
			configXMLElement->QueryBoolAttribute("enable", &isActive);
			if (isActive)
			{
				settings.operaringMode = OperatingModes::CONTROLLED;
				LoadReferencePoints();
			}
			else
			{
				settings.operaringMode = OperatingModes::FREE;
			}
		}
	}

#ifdef VIDEO_CAPTURE
	// block: Serial
	{
		configXMLElement = configRootHandle.FirstChild("Video").Element();
		if (configXMLElement)
		{
			videoStreamAddress.assign(configXMLElement->Attribute("streamAddress"));
			configXMLElement->QueryBoolAttribute("displayOnScreen", &displayVideoOnScreen);
		}
	}
#endif // VIDEO_CAPTURE

#else // TIXML_USE_STL

	{ // Load peg board configuration
		numberOfPegs = 1;
		numberOfAxises = 4;

		// ODE object
		ODEPeg = new cODEGenericBody*[numberOfPegs];
		ODEAxis = new cODEGenericBody*[numberOfAxises];

		// define postion and roation variables for peg object
		objectPosition = new cVector3d[numberOfPegs];
		objectRotation = new cMatrix3d[numberOfPegs];

		// define postion and roation variables for pegs object
		pegPosition = new cVector3d[numberOfAxises];

		// default distance between both objects when demo starts
		double DISTANCE = 0.4;

		if (numberOfPegs == 1)
		{
			// position and oriente ODEPeg 1
			objectPosition[0] = cVector3d(0.2, -DISTANCE, -0.3);
			objectRotation[0].identity();
			objectRotation[0].rotate(cVector3d(0, 0, 1), cDegToRad(90));
		}
		else if (numberOfPegs == 2)
		{
			// position and oriente ODEPeg 1
			objectPosition[0] = cVector3d(0.2, -DISTANCE, -0.3);
			objectRotation[0].identity();
			objectRotation[0].rotate(cVector3d(0, 0, 1), cDegToRad(90));

			// position and oriente ODEPeg 2
			objectPosition[1] = cVector3d(-0.2, DISTANCE, -0.3);
			objectRotation[1].rotate(cVector3d(0, 0, 1), cDegToRad(14));
		}

		if (numberOfAxises == 1)
		{
			// position ODEAxis 1
			pegPosition[0] = cVector3d(0.0, -DISTANCE, -0.3);
		}
		else if (numberOfAxises == 2)
		{
			// position ODEAxis 1
			pegPosition[0] = cVector3d(0.0, -DISTANCE, -0.3);

			// position ODEAxis 2
			pegPosition[1] = cVector3d(0.0, DISTANCE, -0.3);
		}
		else if (numberOfAxises == 4)
		{
			// position ODEAxis 1
			pegPosition[0] = cVector3d(0.2, -DISTANCE, -0.1);

			// position ODEAxis 2
			pegPosition[1] = cVector3d(0.2, DISTANCE, -0.1);

			// position ODEAxis 3
			pegPosition[2] = cVector3d(-0.2, -DISTANCE, -0.1);

			// position ODEAxis 4
			pegPosition[3] = cVector3d(-0.2, DISTANCE, -0.1);
		}

		// create a virtual mesh  that will be used for the geometry
		// representation of the dynamic body
		peg = new cMesh*[numberOfPegs];
		pegs = new cMesh*[numberOfAxises];
	}

	{ // Camera
		cameraLookAt = cVector3d(1.2, 0.0, 2.5);
		cameraPostion = cVector3d(0.0, 0.0, -0.5);
		cameraUpVector = cVector3d(0.0, 0.0, 1.0);
	}
#ifdef MOTION_CAPTURE
	MotionCapture_ActiveOutputs = MotionCapture_3D_Vector;

	active3DMarkersCount = 1;
	active3DMarkerIDs = new int[active3DMarkersCount];
	active3DMarkerIDs[0] = 0;
	active3DMarkerNames = new string[active3DMarkersCount];
	active3DMarkerNames[0] = "Marker";

	mainWindowPosX = (mainWindowWidth - WINDOW_SIZE_W) / 4;
	mainWindowPosY = (mainWindowHeight - WINDOW_SIZE_H) / 4;
	motionCaptureWindowPosX = 3 * (mainWindowWidth - WINDOW_SIZE_W) / 4;
	motionCaptureWindowPosY = (mainWindowHeight - WINDOW_SIZE_H) / 4;
	mainWindowWidth = 600;
	mainWindowHeight = 600;
	motionCaptureWindowWidth = 600;
	motionCaptureWindowHeight = 600;
#else
	mainWindowPosX = (mainWindowWidth - WINDOW_SIZE_W) / 2;
	mainWindowPosY = (mainWindowHeight - WINDOW_SIZE_H) / 2;
	mainWindowWidth = 600;
	mainWindowHeight = 600;
#endif // MOTION_CAPTURE
#ifdef SERIAL_INTERFACE
	serialPortName = "COM5";
#endif // SERIAL_INTERFACE
#ifdef VIDEO_CAPTURE
	videoStreamAddress = "rtsp://camera/axis-media/media.amp?videocodec=h264&streamprofile=Quality";
#endif // VIDEO_CAPTURE

#endif // TIXML_USE_STL
}

//---------------------------------------------------------------------------

int ConfigurationSave(const char *filename)
{
#ifdef TIXML_USE_STL
	TiXmlDocument configDocument(filename);

	if (!configDocument.LoadFile())
	{
		printf("Could not save 'configuration.xml'. Error='%s'. Exiting.\n", configDocument.ErrorDesc());
		return -1;
	}

	TiXmlHandle configXMLHandle(&configDocument);
	TiXmlElement* configXMLElement;
	TiXmlHandle configRootHandle(0);

	// block: root
	{
		configXMLElement = configXMLHandle.FirstChildElement().Element();
		// should always have a valid root but handle gracefully if it does
		if (!configXMLElement) return -1;
		//m_name = pElem->Value();

		// save this for later
		configRootHandle = TiXmlHandle(configXMLElement);
	}

	// block: Window attributes
	{
		TiXmlElement* configWindowNode = configRootHandle.FirstChild("Windows").FirstChild().Element();
		for (configWindowNode; configWindowNode; configWindowNode = configWindowNode->NextSiblingElement())
		{
			const char *windowName = configWindowNode->Attribute("name");
			if (strcmp(windowName, "mainWindow") == 0)
			{
				glutSetWindow(mainWindow);
				configWindowNode->SetAttribute("x", glutGet((GLenum)GLUT_WINDOW_X));
				configWindowNode->SetAttribute("y", glutGet((GLenum)GLUT_WINDOW_Y));
				configWindowNode->SetAttribute("w", glutGet((GLenum)GLUT_WINDOW_WIDTH));
				configWindowNode->SetAttribute("h", glutGet((GLenum)GLUT_WINDOW_HEIGHT));
			}
#ifdef MOTION_CAPTURE
			else if (strcmp(windowName, "motionCaptureWindow") == 0)
			{
				glutSetWindow(motionCaptureWindowReference);
				configWindowNode->SetAttribute("x", glutGet((GLenum)GLUT_WINDOW_X));
				configWindowNode->SetAttribute("y", glutGet((GLenum)GLUT_WINDOW_Y));
				configWindowNode->SetAttribute("w", glutGet((GLenum)GLUT_WINDOW_WIDTH));
				configWindowNode->SetAttribute("h", glutGet((GLenum)GLUT_WINDOW_HEIGHT));
			}
#endif // MOTION_CAPTURE
		}
	}
	configDocument.SaveFile();
#else // TIXML_USE_STL
	// Do nothing!	
#endif // TIXML_USE_STL
	return 0;
}

//---------------------------------------------------------------------------

const std::string currentDateTime() {
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);

	// for more information about date/time format
	strftime(buf, sizeof(buf), "%Y%m%d_%H%M%S", &tstruct);

	return buf;
}

//---------------------------------------------------------------------------

void LoadReferencePoints()
{
	//if (settings.referenceModelFilename.compare("") == 0) return;
	char fileName[100];
	sprintf(fileName, "%s/%s", settings.outputDirectory.c_str(), settings.referenceModelFilename.c_str());
	FILE * file = fopen(fileName, "r");
	const int size = 100;
	vector<cVector3d> hap0;
	vector<bool> switch0;
	vector<cVector3d> hap1;
	vector<bool> switch1;
	if (file)
	{
		//int device;
		char buff[size];

		int switchh[4];
		float x1, y1, z1, x2, y2, z2;
		float time;
		while (fgets(buff, sizeof(buff), file))
		{
			int res = sscanf(buff, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%d\t%d\t%d\t%d\n",
				&time, &x1, &y1, &z1, &x2, &y2, &z2,
				&switchh[0], &switchh[1],
				&switchh[2], &switchh[3]);
			if (res != 11) break;
			cVector3d hap00(x1, y1, z1);
			cVector3d hap11(x2, y2, z2);
			if (hap0.size() > 0)
			{
				cVector3d tmp = hap0.back();
				if (tmp.distance(hap00) > settings.minimumDistanceBetweenReferencePoints)
				{
					hap0.push_back(hap00);
					switch0.push_back(switchh[1]);
				}
			}
			else
			{
				hap0.push_back(hap00);
				switch0.push_back(switchh[1]);
			}
			if (hap1.size() > 0)
			{
				cVector3d tmp = hap1.back();
				if (tmp.distance(hap11) > CHAI_SMALL)// settings.minimumDistanceBetweenReferencePoints)
				{
					hap1.push_back(hap11);
					switch1.push_back(switchh[3]);
				}
			}
			else
			{
				hap1.push_back(hap11);
				switch1.push_back(switchh[3]);
			}
		}
		settings.numberOfReferencePoints[0] = hap0.size();
		settings.numberOfReferencePoints[1] = hap1.size();
		settings.referencePoints[0] = (cVector3d *)malloc(sizeof(cVector3d)* settings.numberOfReferencePoints[0]);
		settings.referencePoints[1] = (cVector3d *)malloc(sizeof(cVector3d)* settings.numberOfReferencePoints[1]);
		settings.switchClick[0] = (bool *)malloc(sizeof(bool)* settings.numberOfReferencePoints[0]);
		settings.switchClick[1] = (bool *)malloc(sizeof(bool)* settings.numberOfReferencePoints[1]);

		std::copy(hap0.begin(), hap0.end(), settings.referencePoints[0]);
		std::copy(hap1.begin(), hap1.end(), settings.referencePoints[1]);
		std::copy(switch0.begin(), switch0.end(), settings.switchClick[0]);
		std::copy(switch1.begin(), switch1.end(), settings.switchClick[1]);

		settings.point = hap0;
	}
}

//---------------------------------------------------------------------------
