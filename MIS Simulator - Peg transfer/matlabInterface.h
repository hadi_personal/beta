#ifndef MATLABINTERFACE_H
#define MATLABINTERFACE_H

#include "engine.h"
//#include "extra.h"

#define	SPATIAL_DATA_WINDOW_SIZE		10
#define	SIMILARITY_WINDOW_SIZE			5
#define	END					9  // WINDOW_SIZE - 1
#define	FEATURES_DIMENSION	9

enum MatlabEngine
{
	DHMM,
	MHMM,
	HCRF
};

#define		Px		0
#define		Py		1
#define		Pz		2
#define		Vx		3
#define		Vy		4
#define		Vz		5
#define		Ax		6
#define		Ay		7
#define		Az		8
#define		Jx		0
#define		Jy		1
#define		Jz		2

struct TooltipSpatialDatatype {
	double filtered[FEATURES_DIMENSION][SPATIAL_DATA_WINDOW_SIZE];
	double raw[FEATURES_DIMENSION][SPATIAL_DATA_WINDOW_SIZE];
	double time[SPATIAL_DATA_WINDOW_SIZE];
	double jerk[3][SPATIAL_DATA_WINDOW_SIZE];
	double filteredJerk[3][SPATIAL_DATA_WINDOW_SIZE];
};

int InitMatlab();
int CloseMatlab();
bool FindSimilarities(TooltipSpatialDatatype *spatialData, int toolID, int &classID, double &similarity);
void SetMatlabEngine(MatlabEngine id);
MatlabEngine GetMatlabEngine();

#endif // MATLABINTERFACE_H