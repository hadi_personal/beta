#ifndef THREADS_H
#define THREADS_H

//#include "variable.h"
//#include "touchManipulation.h"
//#include <iostream>
//#include <fstream>
//#include "timer.h"

void SetupPedal();
void SetupNDI();
void runningPedalThread(void);
void dataRecorderThreadHandler(void);
void interactiveCommandCenterThreadHandler(void);

#endif // THREADS_H
