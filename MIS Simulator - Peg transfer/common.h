#ifndef COMMON_H
#define COMMON_H

#include "configuration.h"
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "chai3d.h"
#include "virtualEnvironment.h"
#include "matlabInterface.h"
#include "timer.h"

extern string		resourceRoot;
#define RESOURCE_PATH(p)    (char*)((resourceRoot + string(p)).c_str())

extern CTimer applicationTimer;

extern double taskPathLength[];
extern double taskMotionSmoothness[];
extern double taskEconomyOfMotion[];

enum ControlledModes
{
	DHMM1,
	MHMM1,
	MHMM2,
	Maximum,
	Medium,
	Minimum,
	HCRF1
};

extern ControlledModes controlledMode;

const int pointShownAhead = 150;

#define MAX_DEVICES				5
#define MAX_OBJECTS				20

#define WINDOW_SIZE_W			1500
#define WINDOW_SIZE_H			1000
#define OPTION_FULLSCREEN		1
#define OPTION_WINDOWDISPLAY	2

#define THRESHOLD1				0.02
#define SPHERE_RADIUS			0.001
#define MAX_POINT				200


extern double		Kp[];
extern Engine*		ep;

extern int			classID[];
extern double		similarity[];
extern bool			similaritiesAreValid[];


extern int			numberOfObjects;
extern int			numberOfPegs;

extern int			displayW;
extern int			displayH;
extern double		zoomConstant;
extern bool			simulationRunning;
extern bool			simulationFinished;
extern int			howManyToShow;
extern double		extend;//extend of the pegs
extern double		stiffnessMax[MAX_DEVICES];

extern cMatrix3d	leftToolRotNormal;
extern cMatrix3d	leftToolRotIn;
extern cMatrix3d	rightToolRotNormal;
extern cMatrix3d	rightToolRotIn;

extern cWorld*		world;
extern cCamera*		camera;
extern cCamera*		cameraTopView;
extern cCamera*		cameraSideView;

extern cLight*		light;
extern cBitmap*		logo;

extern cGenericHapticDevice*	genericHapticDevices[];
extern cHapticDeviceHandler*	hapticDeviceHandler;
extern cGeneric3dofPointer*		generic3dofPointers[];
extern int						theClosestPointIndices[];

extern double proxyRadius;

extern cODEWorld*			ODEWorld;

extern cODEGenericBody*		objects[];
extern cODEGenericBody*		pegs[];

extern bool					pathVisible;
bool						IsPathVisible();

extern cVector3d			rootCoord;
extern int					mainWindow, mainWindowTopView, mainWindowSideView;

extern double				subWin1[];
extern double				subWin2[];


extern cVector3d			lightVector;
extern cVector3d			extraForce;


extern int					numberOfHapticDevices;
extern cGenericObject*		rootLabels;
extern cLabel**				labels;
#define				numLastPoint 1000
extern cVector3d			lastPoint[];

extern CSerial			serial;

extern bool				serialIsConnected;
extern bool				serialInterfaceThreadFinished;
extern bool				isPedalPressed;


void keySelect(unsigned char key, int x, int y);
void menuSelect(int value);
void close();
void updateGraphics();
void updateHaptics();
void runningPedalThread();
void updateGraphicsTopView();
void updateGraphicsSideView();
void SetupPedal();
void SetupNDI();
void postDraw();
void mainDrawPath();
void init();

void mouseWheel(int button, int direction, int x, int y);

void SetupVirtualEnvironment();

int main(int argc, char* argv[]);
int findTheClosestPointIndex(int deviceIndex, cVector3d * devicePosition);

#endif // COMMON_H
