#ifndef TOUCHMANIPULATION_H
#define TOUCHMANIPULATION_H

#include "common.h"


extern cColorf red;
extern cColorf green;
extern cColorf blue;

extern const double liftLevel;
extern const double toolProximity;

struct DeviceInformation
{
	bool			hasTouchedTheFirstPoint;
	bool			hasReachedTheLastPoint;

	bool			hasGraspedAnObject;
	int				graspedObjectIndex;

	bool			button;

	cVector3d		graspedObjectPosition;
	cShapeLine*		graspLine;

	cShapeSphere*	referenceStartingPointSphere;
	cShapeSphere**	referencePointSpheres;

	cVector3d		lastPosDevice;
	cMatrix3d		lastRotDevice;
	cVector3d		lastPosObject;
	cMatrix3d		lastRotObject;
	cVector3d		lastDeviceObjectPos;
	cMatrix3d		lastDeviceObjectRot;
};

extern DeviceInformation deviceInformation[];

extern int lastPointCounter;

extern int axisAttach[];
extern int objectGraspingDevice[];

extern double rootTouchTolerance;

extern cShapeSphere* followPoint[];



extern int designatedObject;
extern double objectAxisProximityXYTolerance;
extern double objectAxisProximityZTolerance;

extern int designatedSide;//right
//value of -1 indicate there is no rotation
//value else: indicate which haptic

extern bool taskCompleted;
extern cMaterial selectObjectMat;
extern cMaterial objectMat;

void	init();
void	SetupObjects(cVector3d *position);
void	SetupPegs(cVector3d position[]);
bool	lookingForObject(cGenericObject *object, int& objectNum);
bool	testObjectAxisAttach(int object_index, int peg_index);
//void	saveLastHapObjectInfo(structLastPosRotInfo &hapticInfo, cGeneric3dofPointer *tool, cODEGenericBody *ODEobject);

#endif // TOUCHMANIPULATION_H
