#include "virtualEnvironment.h"
#include "common.h"

void printMenu()
{
	printf("Instructions:\n\n");
	printf("- Use haptic device and user switch to manipulate object parts \n");
	printf("\n\n");
	printf("Keyboard Options:\n\n");
	printf("[x] - Exit application\n");
	printf("\n\n");
}

unsigned long long choose(unsigned long long n, unsigned long long k) {
	if (k > n) {
		return 0;
	}
	unsigned long long r = 1;
	for (unsigned long long d = 1; d <= k; ++d) {
		r *= n--;
		r /= d;
	}
	return r;
}

void SetupCameras(cWorld* world, cCamera* camera, cCamera* cameraTopView, cCamera* cameraSideView, char* path, cGenericObject* rootLabels)
{
	world->addChild(camera);
	world->addChild(cameraTopView);
	world->addChild(cameraSideView);

	camera->setClippingPlanes(0.01, 10.0);
	cameraTopView->setClippingPlanes(0.01, 10.0);
	cameraSideView->setClippingPlanes(0.01, 10.0);

	// create a light source and attach it to the camera
	cLight* light = new cLight(world);
	camera->addChild(light);                   // attach light to camera

	light->setEnabled(true);                   // enable light source
	light->setPos(cVector3d(2.0, 0.5, 1.0));   // position the light source
	light->setDir(cVector3d(-2.0, 0.5, 1.0));  // define the direction of the light beam
	light->m_ambient.set(0.6, 0.6, 0.6);
	light->m_diffuse.set(0.8, 0.8, 0.8);
	light->m_specular.set(0.8, 0.8, 0.8);

	cBitmap* logo = new cBitmap();
	//camera->m_front_2Dscene.addChild(logo);
	bool fileload;
	fileload = logo->m_image.loadFromFile(path);
	logo->setPos(10, 10, 0);
	logo->setZoomHV(0.25, 0.25);
	logo->m_image.replace(
		cColorb(0, 0, 0),      // original RGB color
		cColorb(0, 0, 0, 0)    // new RGBA color
		);
	logo->enableTransparency(true);

	camera->m_front_2Dscene.addChild(rootLabels);
}

void SetupLaparoscopicTool(cGeneric3dofPointer* tool, cWorld * world)
{
	// create a new mesh.
	cMesh* laparoscopicTool = new cMesh(world);
	bool cong = laparoscopicTool->loadFromFile(RESOURCE_PATH("resources/peg_transfer/forceps_handle.obj"));
	laparoscopicTool->scale(0.7);
	laparoscopicTool->deleteCollisionDetector(true);
	
	const double both = 0.1;

	cMesh* leftJaw = new cMesh(world);
	leftJaw->loadFromFile(RESOURCE_PATH("resources/peg_transfer/forceps_left_jaw.obj"));
	leftJaw->scale(both);
	leftJaw->deleteCollisionDetector(true);

	cMesh* rightJaw = new cMesh(world);
	rightJaw->loadFromFile(RESOURCE_PATH("resources/peg_transfer/forceps_right_jaw.obj"));
	rightJaw->scale(both);
	rightJaw->deleteCollisionDetector(true);

	cMaterial mat;
	mat.m_ambient.set(0.5, 0.5, 0.5);
	mat.m_diffuse.set(0.8, 0.15, 0.2);
	mat.m_specular.set(1.0, 0.2, 0.2);
	laparoscopicTool->setMaterial(mat, true);
	// attach jwas to the laparoscopic tool
	laparoscopicTool->setObjectType(CHAI_OBJECT_TYPE_DONT_IGNORE_COLLISION);
	tool->m_proxyMesh->setObjectType(CHAI_OBJECT_TYPE_DONT_IGNORE_COLLISION);
	tool->m_proxyMesh->addChild(laparoscopicTool);

	// add the left jaw as the first child to m_proxyMesh
	tool->m_proxyMesh->addChild(leftJaw);
	// add the right jaw as the second child to m_proxyMesh
	tool->m_proxyMesh->addChild(rightJaw);
}


void CreateWallsAndGround(cWorld * world, cODEWorld * ODEWorld, double bottomLevel, double size, cGeneric3dofPointer * tool[], double stiffness)
{
	// we now create 6 static walls to bound the workspace of our simulation
	cODEGenericBody * ODEGPlane0 = new cODEGenericBody(ODEWorld);
	cODEGenericBody * ODEGPlane1 = new cODEGenericBody(ODEWorld);
	cODEGenericBody * ODEGPlane2 = new cODEGenericBody(ODEWorld);
	cODEGenericBody * ODEGPlane3 = new cODEGenericBody(ODEWorld);
	cODEGenericBody * ODEGPlane4 = new cODEGenericBody(ODEWorld);
	cODEGenericBody * ODEGPlane5 = new cODEGenericBody(ODEWorld);

	ODEGPlane0->createStaticPlane(cVector3d(0.0, 0.0, size),		cVector3d(0.0, 0.0, -1.0));
	ODEGPlane1->createStaticPlane(cVector3d(0.0, 0.0, bottomLevel), cVector3d(0.0, 0.0, 1.0));
	ODEGPlane2->createStaticPlane(cVector3d(0.0, size, 0.0),		cVector3d(0.0, -1.0, 0.0));
	ODEGPlane3->createStaticPlane(cVector3d(0.0, -size, 0.0),		cVector3d(0.0, 1.0, 0.0));
	ODEGPlane4->createStaticPlane(cVector3d(size, 0.0, 0.0),		cVector3d(-1.0, 0.0, 0.0));
	ODEGPlane5->createStaticPlane(cVector3d(-size, 0.0, 0.0),		cVector3d(1.0, 0.0, 0.0));
	//#define USE_REFLECTION
#ifdef USE_REFLECTION
	cGenericObject* reflexion = new cGenericObject();
	reflexion->setAsGhost(true);
	world->addChild(reflexion);
	cMatrix3d rotRefexion;
	rotRefexion.set(1.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, -1.0);
	reflexion->setRot(rotRefexion);
	reflexion->setPos(0.0, 0.0, -2.005);
	reflexion->addChild(ODEWorld);
	for (int i = 0; i < numberOfHapticDevices; i++)
		reflexion->addChild(tool[i]);
#endif

	// create mesh to model ground surface
	cMesh* ground = new cMesh(world);
	world->addChild(ground);
	double groundSize = size;
	int vertices0 = ground->newVertex(-groundSize, -groundSize, 0.0);
	int vertices1 = ground->newVertex(groundSize, -groundSize, 0.0);
	int vertices2 = ground->newVertex(groundSize, groundSize, 0.0);
	int vertices3 = ground->newVertex(-groundSize, groundSize, 0.0);

	ground->newTriangle(vertices0, vertices1, vertices2);
	ground->newTriangle(vertices0, vertices2, vertices3);
	ground->computeAllNormals();
	ground->setPos(0.0, 0.0, bottomLevel);

	cMaterial matGround;
	matGround.setStiffness(stiffness);
	matGround.setDynamicFriction(0.7);
	matGround.setStaticFriction(1.0);
	matGround.m_ambient.set(0, 0, 0.0);
	matGround.m_diffuse.set(0.5, 0.5, 0.0);
	matGround.m_specular.set(0.0, 0.0, 0.0);

	ground->setMaterial(matGround);
	ground->setTransparencyLevel(0.75);
	ground->setUseTransparency(true);
	ground->setName("congdoan", true);
}

void SetupHapticDevices(int index, cGeneric3dofPointer* tool, cShapeLine* graspLine, cWorld* world, double proxyRadius, cGenericHapticDevice* hapticDevice)
{
	world->addChild(tool);
	tool->setHapticDevice(hapticDevice);
	// initialize tool by connecting to haptic device
	tool->start();
	// map the physical workspace of the haptic device to a larger virtual workspace.
	tool->setWorkspaceRadius(3);
	// define a radius for the tool (graphical display)
	tool->setRadius(0.015);
	// hide the device sphere. only show proxy.
	tool->m_deviceSphere->setShowEnabled(false);

	// set the physical readius of the proxy.
	tool->m_proxyPointForceModel->setProxyRadius(proxyRadius);
	tool->m_proxyPointForceModel->m_collisionSettings.m_checkBothSidesOfTriangles = false;
	// enable if objects in the scene are going to rotate of translate
	// or possibly collide against the tool. If the environment
	// is entirely static, you can set this parameter to "false"
	tool->m_proxyPointForceModel->m_useDynamicProxy = true;
	// ajust the color of the tool
	tool->m_materialProxy = tool->m_materialProxyButtonPressed;

	// create a small label to indicate the position of the device
	
	world->addChild(graspLine);
	graspLine->m_ColorPointA.set(1.0, 1.0, 1.0);
	graspLine->m_ColorPointB.set(1.0, 1.0, 1.0);
	graspLine->setShowEnabled(false);
}

HANDLE console_handle;

BOOL WINAPI ConsoleHandler(DWORD ctrl_type)
{
	if (ctrl_type == CTRL_C_EVENT || ctrl_type == CTRL_BREAK_EVENT)
	{
		ExitProcess(0);
	}
	return FALSE;
}

void ConsoleInit()
{
	COORD dwSize;
	CONSOLE_CURSOR_INFO cursor_info;

	console_handle =
		CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE,
		0, NULL,
		CONSOLE_TEXTMODE_BUFFER,
		NULL);
	SetConsoleActiveScreenBuffer(console_handle);
	SetConsoleCtrlHandler(ConsoleHandler, TRUE);
	dwSize.X = 80;
	dwSize.Y = 24;
	SetConsoleScreenBufferSize(console_handle, dwSize);
	cursor_info.dwSize = 1;
	cursor_info.bVisible = FALSE;
	SetConsoleCursorInfo(console_handle, &cursor_info);
	SetConsoleTextAttribute(console_handle,
		FOREGROUND_GREEN | FOREGROUND_INTENSITY);
}

void Display(int position_y, int position_x, char *message)
{
	COORD dwPosition;
	DWORD written;

	dwPosition.X = position_x;
	dwPosition.Y = position_y;
	SetConsoleCursorPosition(console_handle, dwPosition);
	WriteFile(console_handle, message, strlen(message), &written, NULL);
}
