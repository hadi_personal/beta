#include "matlabInterface.h"
#include <stdlib.h>
#include <iostream>
#include <deque>
#include "common.h"

using namespace std;

#define	BUFSIZE				2256

Engine *ep;

char matlabBuffer[BUFSIZE + 1];
//double window[FEATURES_DIMENSION][WINDOW_SIZE];

using namespace std;

MatlabEngine engineID = MatlabEngine::MHMM;
string matlabCode;

void SetMatlabEngine(MatlabEngine id)
{
	engineID = id;

	ifstream inFile;
	if (engineID == MatlabEngine::DHMM)
	{
		inFile.open(RESOURCE_PATH("resources/models/findSimilarities_dhmm.m"));//open the input file
	}
	else if (engineID == MatlabEngine::MHMM)
	{
		inFile.open(RESOURCE_PATH("resources/models/findSimilarities_mhmm.m"));//open the input file
	}
	else if (engineID == MatlabEngine::HCRF)
	{
		inFile.open(RESOURCE_PATH("resources/models/findSimilarities_hcrf.m"));//open the input file
	}
	stringstream strStream;
	strStream << inFile.rdbuf();//read the file
	matlabCode = strStream.str();//str holds the content of the file
}

MatlabEngine GetMatlabEngine()
{
	return engineID;
}

int InitMatlab()
{
	if (!(ep = engOpen(""))) {
		fprintf(stderr, "\nCan't start MATLAB engine\n");
		return EXIT_FAILURE;
	}
	engEvalString(ep, (string("cd \'") + RESOURCE_PATH("resources/models") + string("\'")).c_str());
	engEvalString(ep, "load hmmModels;");
	engEvalString(ep, "load hcrfModel;");
	engEvalString(ep, "load allCentroids;");

	engEvalString(ep, (string("addpath(genpath(\'") + RESOURCE_PATH("resources/murphy") + string("\'));")).c_str());
	engEvalString(ep, (string("addpath(genpath(\'") + RESOURCE_PATH("resources/resources\\HCRF - UGM_Konstantinos Bousmalis") + string("\'));")).c_str());

	return EXIT_SUCCESS;
}

int classIDs[MAX_DEVICES][SIMILARITY_WINDOW_SIZE];
double similarities[MAX_DEVICES][SIMILARITY_WINDOW_SIZE];
int numberOfInfinities = 0;

bool FindSimilarities(TooltipSpatialDatatype *spatialData, int toolID, int &classID, double &similarity)
{
	mxArray* selectedData = mxCreateDoubleMatrix(SPATIAL_DATA_WINDOW_SIZE, FEATURES_DIMENSION, mxREAL);

	memcpy((void*)mxGetPr(selectedData), (void*)spatialData[toolID].filtered, sizeof(spatialData[toolID].filtered));
	engPutVariable(ep, "selectedData", selectedData);

	matlabBuffer[BUFSIZE] = '\0';
	engOutputBuffer(ep, matlabBuffer, BUFSIZE);
	engEvalString(ep, matlabCode.c_str());

	//Display(10, 1, matlabBuffer);
	
	double *newr;
	mxArray *result;
	result = engGetVariable(ep, "results");
	newr = mxGetPr(result);
	//cout << "START \t" << *(double *)mxGetPr(engGetVariable(ep, "maxPosition")) << "\t" << similarities[toolID][SIMILARITY_WINDOW_SIZE - 1] << "\t" << newr[0] << "\t" << newr[1] << "\t" << newr[2] << "\t" << newr[3] << "\t" << endl;

	for (int i = 1; i < SIMILARITY_WINDOW_SIZE; i++)
	{
		classIDs[toolID][i - 1] = classIDs[toolID][i];
		similarities[toolID][i - 1] = similarities[toolID][i];
	}

	int detectedClassID = *(double *)mxGetPr(engGetVariable(ep, "maxPosition")) - 1; 
	classIDs[toolID][SIMILARITY_WINDOW_SIZE - 1] = detectedClassID;
	similarities[toolID][SIMILARITY_WINDOW_SIZE - 1] = newr[detectedClassID];

	int detectedGesture[] = { 0, 0, 0, 0 };
	for (int i = 0; i < SIMILARITY_WINDOW_SIZE; i++)
	{
		detectedGesture[classIDs[toolID][i]]++;
	}
	classID = -1;
	for (int i = 0; i < 4; i++)
	{
		if (detectedGesture[i] > classID)
			classID = i;
	}
	similarity = 0.0;
	int noc = 0;
	for (int i = 0; i < SIMILARITY_WINDOW_SIZE; i++)
	{
		if (classIDs[toolID][i] == classID)
		{
			similarity += similarities[toolID][i];
			noc++;
		}
	}
	similarity = similarity / noc;

	//std::string delims = "\n";
	//std::string matlabResults = matlabBuffer;
	//std::vector<std::string> results;
	//size_t lastOffset = 0;
	//while (true)
	//{
	//	size_t offset = matlabResults.find_first_of(delims, lastOffset);
	//	results.push_back(matlabResults.substr(lastOffset, offset - lastOffset));
	//	if (offset == std::string::npos)
	//		break;
	//	else
	//		lastOffset = offset + 1; // add one to skip the delimiter
	//}

	//if (results.size() == 7)
	//{
	//	for (int i = 1; i < SIMILARITY_WINDOW_SIZE; i++)
	//	{
	//		classIDs[toolID][i - 1] = classIDs[toolID][i];
	//		similarities[toolID][i - 1] = similarities[toolID][i];
	//	}

	//	if (results[4].find("Inf") != -1)
	//	{
	//		numberOfInfinities++;
	//		if (numberOfInfinities > 3)
	//		{
	//			similarities[toolID][SIMILARITY_WINDOW_SIZE - 1] = 0.0;
	//		}
	//	}
	//	else
	//	{
	//		classIDs[toolID][SIMILARITY_WINDOW_SIZE - 1] = atoi(results[3].c_str()) - 1;
	//		similarities[toolID][SIMILARITY_WINDOW_SIZE - 1] = atof(results[4].c_str());
	//		numberOfInfinities = 0;
	//	}

	//	classID = classIDs[toolID][SIMILARITY_WINDOW_SIZE - 1];
	//	similarity = 0.0;
	//	for (int i = 0; i < SIMILARITY_WINDOW_SIZE; i++)
	//	{
	//		similarity += similarities[toolID][i];
	//	}
	//	similarity = similarity / SIMILARITY_WINDOW_SIZE;
	//}
	//else
	//{
	//	return false;
	//}

	mxDestroyArray(selectedData);

	return true;
}

int CloseMatlab()
{
	return engClose(ep);
}
