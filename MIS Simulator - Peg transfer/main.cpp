#include "common.h"

#include "redraw.h"
#include "touchManipulation.h"
#include "threads.h"
#include "RestartAPI.h"

CTimer			applicationTimer;
ControlledModes	controlledMode;
string			resourceRoot;
bool			isGravityEnabled = true;
cVector3d		gravity(0.0, 0.0, -1);

void testForCompletion();
void mouseWheel(int button, int direction, int x, int y);
void SetupVirtualEnvironment();
void resizeWindow(int w, int h);
void resizeWindowTopView(int w, int h);
void resizeWindowSideView(int w, int h);

int main(int argc, char* argv[])
{
	if (RA_CheckForRestartProcessStart())
		RA_WaitForPreviousProcessFinish();

	resourceRoot = string(argv[0]).substr(0, string(argv[0]).find_last_of("/\\") + 1);
	ConfigurationLoad(RESOURCE_PATH("resources/configuration.xml"), ndiMotion);

	init();
	//ConsoleInit();
	//printMenu();
	SetMatlabEngine(MatlabEngine::MHMM);

	SetupVirtualEnvironment();

	glutInit(&argc, argv);

	glutInitWindowPosition(settings.mainWindowX, settings.mainWindowY);
	glutInitWindowSize(settings.mainWindowWidth, settings.mainWindowHeight);

	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	mainWindow = glutCreateWindow(argv[0]);

	glutDisplayFunc(updateGraphics);
	glutKeyboardFunc(keySelect);
	glutReshapeFunc(resizeWindow);

	// Commented by Hadi
	//		glutMouseWheelFunc(mouseWheel);

	glutCreateMenu(menuSelect);

	//glutSetWindowTitle("PEG TRANSFER");
	//glutAddMenuEntry("full screen", OPTION_FULLSCREEN);
	//glutAddMenuEntry("window display", OPTION_WINDOWDISPLAY);
	//glutAttachMenu(GLUT_RIGHT_BUTTON);

	mainWindowTopView = glutCreateSubWindow(mainWindow, settings.mainWindowTopViewX, settings.mainWindowTopViewY, settings.mainWindowTopViewWidth, settings.mainWindowTopViewHeight);
	glutDisplayFunc(updateGraphicsTopView);
	glutReshapeFunc(resizeWindowTopView);

	mainWindowSideView = glutCreateSubWindow(mainWindow, settings.mainWindowSideViewX, settings.mainWindowSideViewY, settings.mainWindowSideViewWidth, settings.mainWindowSideViewHeight);
	glutDisplayFunc(updateGraphicsSideView);
	glutReshapeFunc(resizeWindowSideView);

	simulationRunning = true;

	cThread* hapticsThread = new cThread();
	hapticsThread->set(updateHaptics, CHAI_THREAD_PRIORITY_HAPTICS);

	cThread* pedalThread = new cThread();
	pedalThread->set(runningPedalThread, CHAI_THREAD_PRIORITY_GRAPHICS);

	cThread* recordHapPosThread = new cThread();
	recordHapPosThread->set(dataRecorderThreadHandler, CHAI_THREAD_PRIORITY_GRAPHICS);

	cThread* interactiveCommandCenterThread = new cThread();
	interactiveCommandCenterThread->set(interactiveCommandCenterThreadHandler, CHAI_THREAD_PRIORITY_GRAPHICS);

	applicationTimer.Start();

	glutMainLoop();

	close();
	RA_DoRestartProcessFinish();

	return (0);
}

void SetupVirtualEnvironment()
{
	SetupPedal();
	SetupNDI();
	
	world = new cWorld();
	world->setBackgroundColor(0.0, 0.0, 0.0);
	ODEWorld		= new cODEWorld(world);
	camera			= new cCamera(world);
	cameraTopView	= new cCamera(world);
	cameraSideView	= new cCamera(world);

	rootLabels = new cGenericObject();
	SetupCameras(world, camera, cameraTopView, cameraSideView, "resources/images/chai3d.bmp", rootLabels);

	// create a haptic device handler
	hapticDeviceHandler = new cHapticDeviceHandler();
	numberOfHapticDevices = hapticDeviceHandler->getNumDevices();

	cHapticDeviceInfo info[MAX_DEVICES];

	for (int i = 0; i < numberOfHapticDevices; i++)
	{
		deviceInformation[i].hasGraspedAnObject = false;
		hapticDeviceHandler->getDevice(genericHapticDevices[i], i);
		// retrieve information about the current haptic device
		if (genericHapticDevices[i])
		{
			info[i] = genericHapticDevices[i]->getSpecifications();
		}
		// create a 3D tool and add it to the world
		generic3dofPointers[i] = new cGeneric3dofPointer(world);

		deviceInformation[i].graspLine = new cShapeLine(cVector3d(0, 0, 0), cVector3d(0, 0, 0));
		SetupHapticDevices(i, generic3dofPointers[i], deviceInformation[i].graspLine, world, proxyRadius, genericHapticDevices[i]);

		stiffnessMax[i] = info[i].m_maxForceStiffness / generic3dofPointers[i]->getWorkspaceScaleFactor();
		SetupLaparoscopicTool(generic3dofPointers[i], world);
	}

	// get the first child of m_proxyMesh which is the left jaw
	cGenericObject* leftJaw = generic3dofPointers[0]->m_proxyMesh->getChild(1);
	// get the second child of m_proxyMesh which is the right jaw
	cGenericObject* rightJaw = generic3dofPointers[0]->m_proxyMesh->getChild(2);

	leftToolRotNormal.copyfrom(leftJaw->getRot());
	leftToolRotNormal.rotate(cVector3d(0, 1, 0), cDegToRad(90));
	leftToolRotIn.copyfrom(leftToolRotNormal);
	leftToolRotNormal.rotate(cVector3d(0, 0, 1), cDegToRad(-30));
	rightToolRotNormal.copyfrom(rightJaw->getRot());
	rightToolRotNormal.rotate(cVector3d(0, 1, 0), cDegToRad(90));
	rightToolRotNormal.rotate(cVector3d(1, 0, 0), cDegToRad(180));
	rightToolRotIn.copyfrom(rightToolRotNormal);
	rightToolRotNormal.rotate(cVector3d(0, 0, 1), cDegToRad(30));

	const int numLabel = 15;
	labels = (cLabel**)malloc(numLabel * sizeof(cLabel*));
	for (int i = 0; i < numLabel; i++)
	{
		labels[i] = new cLabel();
		labels[i]->setPos(0, 2 + 5 * i, 0);
		labels[i]->m_fontColor.set(1, 1, 1);
		rootLabels->addChild(labels[i]);
		labels[i]->m_font->setFontFace("courier50");
	}

	world->addChild(ODEWorld);
	ODEWorld->setGravity(gravity);
	ODEWorld->translate(-0.5, 0, 0);

	numberOfObjects = settings.numberOfObjects;
	numberOfPegs	= settings.numberOfPegs;

	SetupObjects(settings.objectPosition);
	SetupPegs(settings.pegPosition);

	objects[designatedObject]->getImageModel()->setMaterial(selectObjectMat, true);

	cODEGenericBody* root = new cODEGenericBody(ODEWorld);
	root->setPos(cVector3d(0, 0, 0));

	camera->set(settings.cameraPostion, settings.cameraLookAt, settings.cameraUpVector);
	cameraTopView->set(settings.cameraTopViewPostion, settings.cameraTopViewLookAt, settings.cameraTopViewUpVector);
	cameraSideView->set(settings.cameraSideViewPostion, settings.cameraSideViewLookAt, settings.cameraSideViewUpVector);

	CreateWallsAndGround(world, ODEWorld, -1, 4, generic3dofPointers, stiffnessMax[0]);
	world->computeGlobalPositions();

	if (settings.operaringMode == OperatingModes::CONTROLLED)
	{
#define USE_SPHERE
#define SHOW_ROOT_INDICATOR
		for (int deviceIndex = 0; deviceIndex < numberOfHapticDevices; deviceIndex++)
		{
			if (settings.numberOfReferencePoints[deviceIndex] != 0)
			{
#ifdef SHOW_ROOT_INDICATOR
				deviceInformation[deviceIndex].referenceStartingPointSphere = new cShapeSphere(rootTouchTolerance);
				world->addChild(deviceInformation[deviceIndex].referenceStartingPointSphere);
				deviceInformation[deviceIndex].referenceStartingPointSphere->setPos(settings.referencePoints[deviceIndex][0]);
#endif
#ifdef USE_SPHERE
				deviceInformation[deviceIndex].referencePointSpheres = (cShapeSphere**)malloc(sizeof(cShapeSphere*)* pointShownAhead);
				for (int i = 0; i < pointShownAhead; i++)
				{
					deviceInformation[deviceIndex].referencePointSpheres[i] = new cShapeSphere(0.1);
					world->addChild(deviceInformation[deviceIndex].referencePointSpheres[i]);
				}
			}
#endif
		}
	}
}

bool IsPathVisible()
{
	return pathVisible;
}

void keySelect(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'g':
		if (isGravityEnabled)
		{
			// disable gravity
			ODEWorld->setGravity(cVector3d(0.0, 0.0, 0.0));
			isGravityEnabled = false;
		}
		else
		{
			// enable gravity
			ODEWorld->setGravity(gravity);
			isGravityEnabled = true;
		}
		break;
	case 'c':
		camera->set(lightVector * 2.5,    // camera position (eye)
			cVector3d(0.0, 0.0, 0.0),    // lookat position (target)
			cVector3d(0.0, 0.0, 1.0));   // direction of the "up" vector
		break;
	case 'a':
	case 'A':
		glutPositionWindow(200, 140);
		glutReshapeWindow(887, 703);
		break;
	case 'z':
	case 'Z':
		glutPositionWindow(1920, 23);
		glutReshapeWindow(1280, 961);
		break;
	case 'p':
	case 'P':
		pathVisible = !pathVisible;
		break;
	case 27: // escape key
	case 'x':
	case 'X':
		close();
		exit(0);
		break;
	case 'r':
	case 'R':
		if (!RA_ActivateRestartProcess())
		{
			cout << "Something went wrong";
		}
		// Terminate application.
		exit(0);
	default:
		break;
	}
}

//---------------------------------------------------------------------------

void menuSelect(int value)
{
	switch (value)
	{
		// enable full screen display
	case OPTION_FULLSCREEN:
		glutFullScreen();
		break;

		// reshape window to original size
	case OPTION_WINDOWDISPLAY:
		glutReshapeWindow(WINDOW_SIZE_W, WINDOW_SIZE_H);
		break;
	}
}

//---------------------------------------------------------------------------

void close()
{
	// stop the simulation
	simulationRunning = false;

	// wait for graphics and haptics loops to terminate
	while (!simulationFinished || !serialInterfaceThreadFinished)
	{
		cSleepMs(100);
	}
	for (int deviceIndex = 0; deviceIndex < numberOfHapticDevices; deviceIndex++)
	{
		// close haptic device
		generic3dofPointers[deviceIndex]->stop();
	}
	ConfigurationSave(RESOURCE_PATH("resources/configuration.xml"));
}


int findTheClosestPointIndex(int deviceIndex, cVector3d* devicePosition)
{
	double	minimumDistance		= 99999999;
	int		closestPointIndex	= -1;
	int		start				= cMax(theClosestPointIndices[deviceIndex], 0);
	int		end					= cMin(start + pointShownAhead, settings.numberOfReferencePoints[deviceIndex]);

	if (start == end - 1)
	{
		return -2;
	}

	for (int i = start; i < end; i++)
	{
		double distance = devicePosition->distance(settings.referencePoints[deviceIndex][i]);
		if (distance < minimumDistance)
		{
			minimumDistance = distance;
			closestPointIndex = i;
		}
	}

	return closestPointIndex;
}

//enable this to draw the last point of haptic on screen
//#define DRAW_LAST_POINT_MOUSE

void updateHaptics(void)
{
	// simulation clock
	cPrecisionClock simClock;

	// a pointer to the current haptic device
	cGeneric3dofPointer* tool;

	simClock.start(true);
	pathVisible = true;

	// main haptic simulation loop
	while (simulationRunning)
	{
		world->computeGlobalPositions(true);

		for (int deviceIndex = 0; deviceIndex < numberOfHapticDevices; deviceIndex++)
		{
			extraForce.set(0, 0, 0);

			tool = generic3dofPointers[deviceIndex];
			tool->updatePose();
#ifdef DRAW_LAST_POINT_MOUSE
			lastpointcounter = (lastpointcounter + 1) % numlastpoint;
			lastpoint[lastpointcounter] = thistool->getproxyglobalpos();
#endif
			//TESTING IF HAVE TO TOUCH THE FIRST POINT
			if (!deviceInformation[deviceIndex].hasTouchedTheFirstPoint && settings.numberOfReferencePoints[deviceIndex] != 0)
			{
				if (tool->getProxyGlobalPos().distance(settings.referencePoints[deviceIndex][0]) < rootTouchTolerance && !deviceInformation[deviceIndex].hasReachedTheLastPoint)
				{
					deviceInformation[deviceIndex].hasTouchedTheFirstPoint = true;
					deviceInformation[deviceIndex].referenceStartingPointSphere->setShowEnabled(false);
				}
			}

			deviceInformation[deviceIndex].button = tool->getUserSwitch(1);

			tool->m_proxyMesh->getChild(1)->setRot(!deviceInformation[deviceIndex].button ? leftToolRotNormal : leftToolRotIn);
			tool->m_proxyMesh->getChild(2)->setRot(!deviceInformation[deviceIndex].button ? rightToolRotNormal : rightToolRotIn);

			//IF THE MODE IS NOT FREE, APPLYING FORCE BASED ON PATH
			if (settings.operaringMode == OperatingModes::CONTROLLED && deviceInformation[deviceIndex].hasTouchedTheFirstPoint)
			{
				cVector3d devicePosition = tool->getProxyGlobalPos();

//#define USE_FORCE_CONTROL
#ifdef USE_FORCE_CONTROL
				if (
					(theClosestPointIndices[deviceIndex] >= 0 && settings.switchClick[deviceIndex][theClosestPointIndices[deviceIndex]] && userSwitch) ||
					theClosestPointIndices[deviceIndex] < 0 ||
					theClosestPointIndices[deviceIndex] >= 0 && !settings.switchClick[deviceIndex][theClosestPointIndices[deviceIndex]])
#else
				if (true)
#endif
				{
					int theClosestPointIndex = findTheClosestPointIndex(deviceIndex, &devicePosition);
					if (theClosestPointIndex == -2) // it has reached the end point
					{
						deviceInformation[deviceIndex].hasTouchedTheFirstPoint = false;
						deviceInformation[deviceIndex].hasReachedTheLastPoint = true;
						pathVisible = false;
						settings.operaringMode = OperatingModes::FREE;
					}
					else if (theClosestPointIndex != -1)
					{
						theClosestPointIndices[deviceIndex] = theClosestPointIndex;
					}
				}

				//IF THERE IS A POINT TO APPLY FORCE AT 
				if (theClosestPointIndices[deviceIndex] >= 0)
				{
					//apply force
					cVector3d displacement = cSub(devicePosition, settings.referencePoints[deviceIndex][theClosestPointIndices[deviceIndex]]);

					switch (controlledMode)
					{
					case ControlledModes::DHMM1:
						double maxValue, minValue;
						maxValue = 0.0;
						minValue = -600.0;
						Kp[deviceIndex] = similarity[deviceIndex] > 0 ? 0.1 : (similarity[deviceIndex] < minValue ? 10 : ((-1 * similarity[deviceIndex] / (maxValue - minValue)) * 10));
						break;
					case ControlledModes::MHMM1:
						maxValue = 0.0;
						minValue = -3000.0;
						Kp[deviceIndex] = similarity[deviceIndex] > 0 ? 0.1 : (similarity[deviceIndex] < minValue ? 10 : ((-1 * similarity[deviceIndex] / (maxValue - minValue)) * 10));
						break;
					case ControlledModes::MHMM2:
						maxValue = 0.0;
						minValue = -4000.0;
						Kp[deviceIndex] = similarity[deviceIndex] > 0 ? 0.1 : (similarity[deviceIndex] < minValue ? 10 : ((-1 * similarity[deviceIndex] / (maxValue - minValue)) * 10));
						break;
					case ControlledModes::Maximum:
						Kp[deviceIndex] = 5;
						break;
					case ControlledModes::Medium:
						Kp[deviceIndex] = 2;
						break;
					case ControlledModes::Minimum:
						Kp[deviceIndex] = 0;
						break;
					case ControlledModes::HCRF1:
						maxValue = 0.0;
						minValue = 4.0;
						Kp[deviceIndex] = (similarity[deviceIndex] > maxValue ? 10 : ((similarity[deviceIndex] / (maxValue - minValue)) * 10));
						break;
					default:
						break;
					}
					extraForce = -Kp[deviceIndex] * displacement;
					//double xx = displacement.length() - THRESHOLD1;
					//extraForce.mul((1 / (1 + exp(-8 * xx))));
				}
			}

			tool->computeInteractionForces();
			cGenericObject* currentObject = tool->m_proxyPointForceModel->m_contactPoint0->m_object;

			cVector3d devicePosition = tool->getProxyGlobalPos();
			cMatrix3d deviceRotation = tool->m_deviceGlobalRot;

			//IF CURRENT GRASPING SOMETHING
			if (deviceInformation[deviceIndex].hasGraspedAnObject && deviceInformation[deviceIndex].button)
			{
				cODEGenericBody* graspedObject = objects[deviceInformation[deviceIndex].graspedObjectIndex];
				cVector3d graspedObjectPosition = graspedObject->getGlobalPos();
				cMatrix3d graspedObjectRotation = graspedObject->getGlobalRot();

				//cVector3d otherTool = generic3dofPointers[1 - deviceIndex]->getProxyGlobalPos();
				int objectIndex = (int)graspedObject->m_userData;
				if (objectGraspingDevice[objectIndex] == deviceIndex)
				{ //DISREGARD ROTATING
					// compute the position of the grasp point on object in global coordinates
					cVector3d globalGraspedObjectPosition = graspedObjectPosition + cMul(graspedObjectRotation, deviceInformation[deviceIndex].graspedObjectPosition);
					cVector3d offset = devicePosition - globalGraspedObjectPosition;

					//APPLYING THE SPRING-DAMPER INTO THE SPRING
					double STIFFNESS = 10;
					cVector3d force = STIFFNESS * offset;
					cVector3d pointApply = globalGraspedObjectPosition;//  graspedObjectPosition;
#define SCHEME_SPRING_DAMPER	1
#define SCHEME_NO_SPRING		1-SCHEME_SPRING_DAMPER
#define SCHEME_ROTATION			1
					//TELL ME WHETHER YOU WANT THE SPRING AND THE DAMPER
					if (SCHEME_SPRING_DAMPER == 1)
					{
						graspedObject->addGlobalForceAtGlobalPos(force, pointApply);//  globalGraspedObjectPosition);
						tool->m_lastComputedGlobalForce.add(cMul(-1.0, force));
					}
					else
					{
						//graspedObject->setPosition(devicePosition);
						cVector3d newPos = cAdd(devicePosition, cMul(deviceRotation, deviceInformation[deviceIndex].lastDeviceObjectPos));
						graspedObject->setPosition(newPos);
					}


					// update both end points of the line which is used for display purposes only
					deviceInformation[deviceIndex].graspLine->m_pointA = pointApply;
					deviceInformation[deviceIndex].graspLine->m_pointB = devicePosition;//  pointApply + force;
					if (SCHEME_ROTATION == 0 || SCHEME_SPRING_DAMPER)
					{
						graspedObject->disableRotation(); //NEVER LET OBJECT ROTATE A BIT
					}

					deviceInformation[deviceIndex].graspLine->m_pointA = graspedObjectPosition;
					deviceInformation[deviceIndex].graspLine->m_pointB = devicePosition;

					if (SCHEME_ROTATION && !SCHEME_SPRING_DAMPER)
					{
						cMatrix3d newRot = cMul(deviceRotation, deviceInformation[deviceIndex].lastDeviceObjectRot);
						graspedObject->setRotation(newRot);
					}
				}
			}
			else //IF NOT GRASPING ANYTHING
			{
				if (currentObject != NULL)
				{
					int objectIndex;
					if (lookingForObject(currentObject, objectIndex))
					{
						cODEGenericBody* ODEobject = objects[objectIndex];
						cVector3d devicePosition = tool->m_proxyPointForceModel->m_contactPoint0->m_globalPos;
						if (deviceInformation[deviceIndex].button) //IF CLICKED
						{
							deviceInformation[deviceIndex].graspedObjectIndex = objectIndex;
							tool->m_proxyPointForceModel->m_collisionSettings.m_hasBeenPicked = true;

							deviceInformation[deviceIndex].graspedObjectPosition = tool->m_proxyPointForceModel->m_contactPoint0->m_localPos;
							deviceInformation[deviceIndex].hasGraspedAnObject = true;
							objectGraspingDevice[objectIndex] = deviceIndex;
							if (SCHEME_ROTATION)
							{
								deviceInformation[deviceIndex].lastDeviceObjectRot = cMul(cTrans(deviceRotation), ODEobject->getRot());
							}
							deviceInformation[deviceIndex].lastDeviceObjectPos =
								cTrans(deviceRotation) * ((ODEobject->getPos() - devicePosition) +
								0.01*cNormalize(ODEobject->getPos() - devicePosition));
						}
						//ONLY APPLY THIS LAST FORCE IF IT IS IN FREE MODE, OR HAS TOUCHED THE FIRST SPHERE.
						cVector3d force = tool->m_lastComputedGlobalForce;
						if (deviceInformation[deviceIndex].hasTouchedTheFirstPoint || settings.operaringMode == OperatingModes::FREE)
						{
							ODEobject->addGlobalForceAtGlobalPos(cNegate(force), devicePosition);
						}
					}
				}
				else
				{
					//IF THERE IS NO OBJECT
					deviceInformation[deviceIndex].graspedObjectIndex = -1;
					tool->m_proxyPointForceModel->m_collisionSettings.m_hasBeenPicked = false;
					deviceInformation[deviceIndex].hasGraspedAnObject = false;
					deviceInformation[deviceIndex].graspLine->setShowEnabled(false);
				}
			}
			// apply generated extra force for haptic guidance
			tool->applyForces(extraForce);
		} //FOR EACH HAPTIC

		//UPDATE WHAT CONTROL WHAT
		for (int objectIndex = 0; objectIndex < numberOfObjects; objectIndex++)
		{
			if (objectGraspingDevice[objectIndex] != -1) //IF THE PEG HAS SOMETHING TOUCH IT
			{
				int deviceIndex = objectGraspingDevice[objectIndex];
				if (deviceInformation[deviceIndex].graspedObjectIndex != objectIndex) //if it no longer control 
					//this device
					objectGraspingDevice[objectIndex] = -1;
			}
			if (objectGraspingDevice[objectIndex] == -1) //if has touch, but now not have
			{
				//objects[objectIndex]->m_imageModel->setHapticEnabled(true, true);
				if (SCHEME_NO_SPRING == 1)
					objects[objectIndex]->enableDynamics();
			}
			if (objectGraspingDevice[objectIndex] != -1) // if object has not been touched, but now it has
			{
				//objects[objectIndex]->m_imageModel->setHapticEnabled(false, true);
				if (SCHEME_ROTATION == 0)
				{
					cMatrix3d tmp;
					tmp.identity();
					objects[objectIndex]->setRotation(tmp);
				}
				if (SCHEME_NO_SPRING == 1)
					objects[objectIndex]->disableDynamics();

			}
			////IMPLEMENET PASSING THE PEG FROM ONE TO ANOTHER HAPTIC
			//if (objectGraspingDevice[objectIndex] != -1) //IF CURRENTLY TOUCH
			//{
			//	int otherTool = 1 - objectGraspingDevice[objectIndex];
			//	bool userSwitch = getSwitch[otherTool];
			//	if (deviceInformation[otherTool].hasGraspedAnObject == false)
			//	if (objects[objectIndex]->getGlobalPos().distance(
			//		generic3dofPointers[otherTool]->getProxyGlobalPos()) < toolProximity)
			//	{
			//		objects[objectIndex]->m_imageModel->m_material.m_diffuse.set(0.5, 0.5, 0.5);
			//		//only need to pass to the second 
			//		if (userSwitch)
			//		{
			//			deviceInformation[otherTool].graspedObjectIndex = objectIndex;
			//			deviceInformation[otherTool].graspedObjectPosition = deviceInformation[objectGraspingDevice[objectIndex]].graspedObjectPosition;
			//			deviceInformation[otherTool].hasGraspedAnObject = true;
			//			deviceInformation[otherTool].graspedObjectPosition = cMul(objects[objectIndex]->getGlobalRot().inv(), generic3dofPointers[otherTool]->getProxyGlobalPos() -
			//				objects[objectIndex]->getGlobalPos());
			//			objectGraspingDevice[objectIndex] = otherTool;

			//		}

			//	}
			//}
		}

		//TESTING PEG TRANSFER COMPLETION
		testForCompletion();

		double time = simClock.getCurrentTimeSeconds();
		double nextSimInterval = cClamp(time, 0.001, 0.004);
		simClock.reset();
		simClock.start();
		ODEWorld->updateDynamics(nextSimInterval);
	}

	// exit haptics thread
	simulationFinished = true;
}

void testForCompletion()
{
	if (taskCompleted) return;

	for (int i = 0; i < numberOfObjects; i++)
	{
		bool found = false;
		for (int j = 0; j < numberOfPegs; j++)
		if (testObjectAxisAttach(i, j)) //TESTING IF OBJECT I ATTACH TO J AXIS
		{
			axisAttach[i] = j;
			found = true; break;
		}
		if (!found) axisAttach[i] = -1;
	}


	bool completeCurrentObject = false;
	if (axisAttach[designatedObject] != -1)
		completeCurrentObject = settings.axisPartition[axisAttach[designatedObject]] == designatedSide;

	//looking for next object
	if (completeCurrentObject)
	{
		objects[designatedObject]->m_imageModel->setMaterial(objectMat, true);
#define NO_MORE_OBJECT -1			
		designatedObject = NO_MORE_OBJECT;

		for (int i = 0; i < numberOfObjects; i++)
		if (axisAttach[i] == -1)
			designatedObject = i;
		else if (settings.axisPartition[axisAttach[i]] != designatedSide) //different side
			designatedObject = i;

		if (designatedObject == NO_MORE_OBJECT)
		{
			taskCompleted = true;
			puts("Completed");
		}
		else
		{
			objects[designatedObject]->m_imageModel->setMaterial(selectObjectMat, true);
		}
	}
}
//---------------------------------------------------------------------------
void mouseWheel(int button, int direction, int x, int y)
{
	double add = 0.1;
	if (direction != 1)
		zoomConstant += add;
	else zoomConstant -= add;
	if (zoomConstant < 0) zoomConstant = 0;

	// position and oriente the camera
	camera->set(lightVector * zoomConstant,    // camera position (eye)
		cVector3d(0.0, 0.0, 0.0),    // lookat position (target)
		cVector3d(0.0, 0.0, 1.0));   // direction of the "up" vector
}


void resizeWindow(int w, int h)
{
	// update the size of the viewport
	displayW = w;
	displayH = h;
	glViewport(0, 0, displayW, displayH);

	glutSetWindow(mainWindowTopView);
	//resize and reposition the sub window
	glutPositionWindow(subWin1[0] * w, subWin1[1] * h);
	glutReshapeWindow(w*subWin1[2], h*subWin1[2]);
	glutSetWindow(mainWindowSideView);
	//resize and reposition the sub window
	glutPositionWindow(subWin2[0] * w, subWin2[1] * h);
	glutReshapeWindow(w*subWin2[2], h*subWin2[2]);

}

void resizeWindowTopView(int w, int h)
{
	// update the size of the viewport
	displayW = w;
	displayH = h;
	glViewport(0, 0, displayW, displayH);
}

void resizeWindowSideView(int w, int h)
{
	// update the size of the viewport
	displayW = w;
	displayH = h;
	glViewport(0, 0, displayW, displayH);
}

