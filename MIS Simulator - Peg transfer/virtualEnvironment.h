#ifndef VIRTUAL_ENVIRONMENT_H
#define VIRTUAL_ENVIRONMENT_H

#define TIXML_USE_STL
#define SERIAL_INTERFACE
#define DATA_STORAGE

#include "stdio.h"
#include "chai3d.h"
#include "CODE.h"
#include "tinyxml.h"
#include "Serial.h"
#include <string>
#include <time.h>
//#include "configuration.h"
#include "RTC3Dclient.h"


//#define MOTION_CAPTURE
void printMenu();


void MotionCaptureUpdate3dList(vector<Position3d> & v3d);


unsigned long long choose(unsigned long long n, unsigned long long k);


void SetupHapticDevices(int index, cGeneric3dofPointer * tool, cShapeLine* graspLine, cWorld * world, double proxyRadius, cGenericHapticDevice* genericHapticDevices);
void SetupLaparoscopicTool(cGeneric3dofPointer* tool, cWorld * world);
void SetupCameras(cWorld * world, cCamera * camera, cCamera * cameraTopView, cCamera* cameraSideView, char * path, cGenericObject * rootLabels);
void CreateWallsAndGround(cWorld * world, cODEWorld * ODEWorld, double bottomLevel, double size, cGeneric3dofPointer * tool[], double stiffness);
const std::string currentDateTime();


void ConsoleInit();
void Display(int position_y, int position_x, char *message);

extern int subjectNumber, workingMode;


#endif // VIRTUAL_ENVIRONMENT_H
