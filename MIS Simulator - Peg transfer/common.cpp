#include "common.h"

double Kp[MAX_DEVICES];

int			numberOfObjects;
int			numberOfPegs;

int			displayW;
int			displayH;
double		zoomConstant;
bool		simulationRunning;
bool		simulationFinished;
int			howManyToShow;
double		extend;//extend of the pegs
double		stiffnessMax[MAX_DEVICES];

cMatrix3d	leftToolRotNormal;
cMatrix3d	leftToolRotIn;
cMatrix3d	rightToolRotNormal;
cMatrix3d	rightToolRotIn;
	
bool		pathVisible;

Settings	settings;
motion_manager* ndiMotion;

cWorld*		world;
cCamera*	camera;
cCamera*	cameraTopView;
cCamera*	cameraSideView;

cLight*		light;
cBitmap*	logo;

cGenericHapticDevice*	genericHapticDevices[MAX_DEVICES];
cHapticDeviceHandler*	hapticDeviceHandler;
cGeneric3dofPointer *	generic3dofPointers[MAX_DEVICES];
int						theClosestPointIndices[MAX_DEVICES];

double proxyRadius;

cODEWorld*			ODEWorld;

cODEGenericBody*	objects[MAX_OBJECTS];
cODEGenericBody*	pegs[MAX_OBJECTS];

cVector3d			rootCoord;
int					mainWindow, mainWindowTopView, mainWindowSideView;

double subWin1[3] = { 0.0, 0.0, 0.25 };
double subWin2[3] = { 0.8, 0.0, 0.25 }; 


cVector3d			lightVector;
cVector3d			extraForce;

int					numberOfHapticDevices;
cGenericObject*		rootLabels;
cLabel**				labels;

cVector3d			lastPoint[numLastPoint];

CSerial				serial;

bool				serialIsConnected = false;
bool				serialInterfaceThreadFinished = false;
bool				isPedalPressed = false;
