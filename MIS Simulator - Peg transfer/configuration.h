#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include "chai3d.h"
#include "RTC3Dclient.h"

enum OperatingModes
{
	FREE,
	CONTROLLED
};

struct Settings
{
	int				mainWindowX, mainWindowY, mainWindowWidth, mainWindowHeight;
	int				mainWindowTopViewX, mainWindowTopViewY, mainWindowTopViewWidth, mainWindowTopViewHeight;
	int				mainWindowSideViewX, mainWindowSideViewY, mainWindowSideViewWidth, mainWindowSideViewHeight;

	int				numberOfObjects, numberOfPegs;
	cVector3d		*objectPosition;
	cMatrix3d		*objectRotation;

	cVector3d		*pegPosition;
	int				*axisPartition;

	cVector3d		cameraLookAt;
	cVector3d		cameraPostion;
	cVector3d		cameraUpVector;

	cVector3d		cameraTopViewLookAt;
	cVector3d		cameraTopViewPostion;
	cVector3d		cameraTopViewUpVector;

	cVector3d		cameraSideViewLookAt;
	cVector3d		cameraSideViewPostion;
	cVector3d		cameraSideViewUpVector;

	string			referenceModelFilename;
	string			outputDirectory;
	string			serialPortName;
	OperatingModes			operaringMode;
	cVector3d		*referencePoints[2];
	vector<cVector3d> point;
	bool *			switchClick[2];
	int				numberOfReferencePoints[2];

	double			minimumDistanceBetweenReferencePoints;
	int				precisionOfFloatingPoints;
	float			hapticSamplingTime;
};

struct motion_manager
{
	const int MotionCapture_None = 0x00;
	const int MotionCapture_3D_Vector = 0x01;
	const int MotionCapture_6D_Vector = 0x02;
	const int MotionCapture_Analog = 0x04;
	const int MotionCapture_All = 0x07;

	int MotionCapture_ActiveOutputs = MotionCapture_None;

	int active3DMarkersCount;
	int* active3DMarkerIDs;
	string* active3DMarkerNames;

	// initial size (width/height) in pixels of the display window
	int motionCaptureWindowWidth;
	int motionCaptureWindowHeight;

	int motionCaptureWindowReference;

	// has exited haptics simulation thread
	bool motionCaptureSimulationFinished = false;

	Client m_Client;
	char* m_sHostname;
	DWORD m_dwIP;
	char szHost[100];
	bool rtc3dClientIsConnected = false;
	vector<QuatErrorTransformation> m_v6d;
	vector<Position3d> m_v3d;
	vector<float> m_vAnalog;
	unsigned int m_uFrame3d;
	unsigned int m_uFrame6d;
	unsigned int m_uFrameAnalog;

	int motionCaptureWindowPosX;
	int motionCaptureWindowPosY;
	double motionCaptureSamplingTime;
};

int		ConfigurationLoad(const char *configFilename, motion_manager *motion);
int		ConfigurationSave(const char *filename);
void	LoadReferencePoints();

// Global variables
extern Settings settings;
extern motion_manager* ndiMotion;

#endif // CONFIGURATION_H
